#include "HS_Utility.h"

string TimestampToString(unsigned long timeValue,bool isGMT)
{
	struct tm FTime;
	char strTmp[128] = "";
	time_t time_tmp = timeValue;
	gmtime_s(&FTime,&time_tmp);


	if (!isGMT)
	{
		gmtime_s(&FTime,&time_tmp);
		time_t UTCTime = mktime(&FTime);
		gmtime_s(&FTime,&UTCTime);
	}

	sprintf_s(strTmp,128, "%04d/%02d/%02d %02d:%02d:%02d", (FTime.tm_year) + 1900, (FTime.tm_mon + 1), FTime.tm_mday, FTime.tm_hour, FTime.tm_min, FTime.tm_sec);
	return string(strTmp);

}

unsigned long StringtoTime(char *str,bool isGMT)
{
	struct tm t;
	struct tm * ptm;
	int year,month,day, hour,minite,second;
	sscanf(str,"%d-%d-%d %d:%d:%d",&year,&month,&day,&hour,&minite,&second);
	t.tm_year = year-1900;
	t.tm_mon = month-1;
	t.tm_mday = day;
	t.tm_hour = hour;
	t.tm_min = minite;
	t.tm_sec = second;
	t.tm_isdst = 0;

	time_t t_of_day;
	time_t t_of_gmt;

	////cout<<ctime(&t_of_day)<<endl;
	//cout <<t_of_day<<endl;
	long int time;
	const int buff_size = 20;
	char buff[buff_size] = {0};
	if(isGMT)
	{
		t_of_day=mktime(&t);//會扣掉timezone時間
		if(t_of_day==-1)
			return 0;
		ptm = gmtime ( &t_of_day );
		t_of_gmt=mktime(ptm);  
		int diff=t_of_day-t_of_gmt;
		t_of_day=diff+t_of_day;
	}
	else
	{
		t_of_day= mktime(&t);  // timestamp in current timezone	
	}
	if(t_of_day!=-1)
		time = t_of_day;
	else 
		time=0;
	return time;

}

unsigned int UTC2Local(long timeValue)
{
	struct tm FTime;
	int diff=0;

	time_t time_tmp = timeValue;

	struct tm *ptm;
	time_t t_of_local;
	time_t t_of_utc;

	gmtime_s(&FTime,&time_tmp);

	t_of_local=mktime(&FTime);//會扣掉timezone時間

	//當時間異常時(ex 1970/01/01)
	if(t_of_local==-1)
		return 0;

	ptm = gmtime ( &t_of_local );
	t_of_utc=mktime(ptm);  
	diff=t_of_local-t_of_utc;

	return diff+timeValue;
}
unsigned int Local2UTC(long timeValue)
{
	struct tm FTime;
	char strTmp[128] = "";
	time_t time_tmp = timeValue;

	gmtime_s(&FTime,&time_tmp);
	time_t UTCTime = mktime(&FTime);

	return UTCTime;
}

//目錄及檔案皆可用
bool DirIsExist(std::wstring Path)
{
	DWORD d;
	d=::GetFileAttributesW(Path.c_str());

	if(d==INVALID_FILE_ATTRIBUTES)
		return false;
	else
		return true;
}


