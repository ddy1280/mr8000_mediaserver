#ifndef GLOBAL_H
#define GLOBAL_H

#include <string>

class CGlobal
{
public:
	static std::string ServerIPAddress;
	static int ServerPort;
	
};
#endif