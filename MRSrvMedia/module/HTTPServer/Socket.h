/*
   Socket.h

   Copyright (C) 2002-2004 Rene Nyffenegger

   This source code is provided 'as-is', without any express or implied warranty. In no event will the author be held liable for any damages
   arising from the use of this software.

   Permission is granted to anyone to use this software for any purpose,
   including commercial applications, and to alter it and redistribute it
   freely, subject to the following restrictions:

   1. The origin of this source code must not be misrepresented; you must not
      claim that you wrote the original source code. If you use this source code
      in a product, an acknowledgment in the product documentation would be
      appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
      misrepresented as being the original source code.

   3. This notice may not be removed or altered from any source distribution.

   Rene Nyffenegger rene.nyffenegger@adp-gmbh.ch
*/

#ifndef SOCKET_H
#define SOCKET_H

#include <stdio.h>
#include <string>
#include <vector>
#include <WinSock.h>

namespace HS
{
namespace Net
{


#ifndef EINPROGRESS
#define EINPROGRESS	WSAEINPROGRESS
#endif

#ifndef EWOULDBLOCK
#define EWOULDBLOCK	WSAEWOULDBLOCK
#endif

#ifndef ETIMEDOUT
#define ETIMEDOUT	  WSAETIMEDOUT
#endif
enum TypeSocket {BlockingSocket=0, NonBlockingSocket};
enum TypeConnectModet {cmTCP=0, cmUDP};
typedef int socklen_t ;




//=========================================================================================================
// Socket Class
//=========================================================================================================

class Socket
{
public:

  virtual ~Socket();
  Socket(const Socket&);
  Socket& operator=(Socket&);
  TypeConnectModet Mode;      //tcp or udp
  std::string ReceiveLine();
  std::string ReceiveBytes();
  std::string ReceiveBytes(int size);
  std::vector<char> ReceiveBuffer(int size);
  
  int Send(SOCKET s,char *buf,int len,int flags);
  int Recv(SOCKET s,char *buf,int len,int flags);

  int   Close();

  SOCKET GetSocket(){return s_;};
  void   SendLine (std::string);
  void   SendBytes(const std::string&);
  int   SendBytes(const char *buffer,int size);
  void GetPeerIPAddress(std::string &str);
  void GetPeerIPAddressPort(std::string &str);
  void GetLocalIPAddress(std::string &str);
  void GetLocalDefaultGateway(std::string &str);
  void SetSocketBlockType(TypeSocket type=BlockingSocket);
  BOOL CheckSocketReadable(int timeout_ms=1000) ;
  BOOL CheckSocketWriteable(int timeout_ms=1000);
protected:
  friend class SocketServer;
  friend class SocketSelect;

  Socket(SOCKET s);
  Socket();


  SOCKET s_;
  int* refCounter_;
    TypeSocket m_Type;

private:
  void Start();
  void End();
  int  nofSockets_;
};




//=========================================================================================================
// SocketClient Class
//=========================================================================================================

class SocketClient : public Socket
{
private:
    std::string mHost;
    int mPort;

public:
    SocketClient(const std::string& host, int port, TypeSocket type=NonBlockingSocket);
    int Connect(SOCKET s,const struct sockaddr *name,int namelen,int timeout_ms);

};



//=========================================================================================================
// SocketServer Class
//=========================================================================================================

class SocketServer : public Socket
{
private:
    int m_connections;

public:
  SocketServer(int port, int connections, TypeSocket type=NonBlockingSocket);

  Socket* Accept();
  int Listen();
};



//=========================================================================================================
// HttpSocket Server Class
//=========================================================================================================

class HttpSocketServer : public SocketServer
{
public:
    HttpSocketServer(int port, int connections, TypeSocket type=BlockingSocket):SocketServer(port,connections,type)
	{
		try
		{
		}
		catch (std::exception& e)
		{
			throw "INVALID_SOCKET";
		}

	};

    std::string ReceiveHeader();

};


//=========================================================================================================
// HttpSocket Client Class
//=========================================================================================================

class HttpSocketClient: public SocketClient
{
public:
    HttpSocketClient(const std::string& host, int port, TypeSocket type=NonBlockingSocket):SocketClient(host,port,type){;};
    std::string StringBetween(std::string s,std::string startstr,std::string endstr);
    std::string ReceiveHeader();
    std::string GetHeaderInfo(std::string HeaderString,std::string HeaderName);
};




//=========================================================================================================
// SocketSelect Class
//=========================================================================================================

// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/winsock/wsapiref_2tiq.asp
class SocketSelect
{
  public:
    SocketSelect(Socket const * const s1, Socket const * const s2=NULL, TypeSocket type=BlockingSocket);

    bool Readable(Socket const * const s);

  private:
    fd_set fds_;
};




}
}
#endif
