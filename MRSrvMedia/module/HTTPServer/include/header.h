#ifndef HTTP_HEADER_H
#define HTTP_HEADER_H

#include <string>
namespace HS
{
namespace Net
{
	 struct header
	 {
		 std::string name;
		 std::string value;
	 };

}//end of namespace Net
}//end of namespace HS


#endif 


