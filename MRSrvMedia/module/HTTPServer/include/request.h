
#ifndef HTTP_REQUEST_H
#define HTTP_REQUEST_H

#include <string>
#include <vector>
#include "header.h"

namespace HS
{
namespace Net
{
	struct request
	{
		//void* connection_;	
		std::string method;
		std::string uri;
		std::string cmd;
		std::string remote_ipaddress;
		int http_version_major;
		int http_version_minor;
		std::vector<header> headers;
		std::string raw_header;	 //add by David on 2014/4/15
		std::string post_data;   //add by David on 2013/2/22
		SOCKET hSocket;
	};
}//end of namespace Net
}//end of namespace HS
#endif 
