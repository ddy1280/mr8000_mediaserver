

#ifndef HTTP_REPLY_H
#define HTTP_REPLY_H

#include <string>
#include <vector>

#include "header.h"

namespace HS
{
namespace Net
{
	 
	 struct reply
	 {
		 public:
		 /// The status of the reply.
		 enum status_type
		 {
			 ok = 200,
			 created = 201,
			 accepted = 202,
			 no_content = 204,
			 multiple_choices = 300,
			 moved_permanently = 301,
			 moved_temporarily = 302,
			 not_modified = 304,
			 bad_request = 400,
			 unauthorized = 401,
			 forbidden = 403,
			 not_found = 404,
			 internal_server_error = 500,
			 not_implemented = 501,
			 bad_gateway = 502,
			 service_unavailable = 503
		 } status;
		 
		 /// The headers to be included in the reply.
		 std::vector<header> headers;

		 /// The content to be sent in the reply.
		 std::string content;

		 //std::vector <unsigned char> content;
		 std::string to_string(reply::status_type status);
		 std::string to_strings();
		 std::vector<unsigned char> reply::to_array();
		 /// Get a stock reply.
		 static reply stock_reply(status_type status);
		 static void stock_reply(reply &rep,std::string replyStr,std::string content_type="text/html;charset=utf-8");
		 static std::string replyString(status_type status);
		 
	 };
	 


}//end of namespace Net
}//end of namespace HS


#endif 
