//---------------------------------------------------------------------------
#include "stdafx.h"
#include "CThread.h"

//---------------------------------------------------------------------------

CCriticalSection CThread::cs_static;
CThread::CThread()
{
    run = false;
    suspend=false;
    looping = true;
    hThread = NULL;
    m_runStatus = CTHREAD_STATUS_STOP;
    // Create events
    m_WaitThread = CreateEvent(0, TRUE, FALSE, 0);    //不自動重置，所以要使用 ResetEvent 手動重置

    ThreadFunction = NULL;
    //取得CPU頻率，用以計時
    QueryPerformanceFrequency(&m_liPerfFreq);
}
                

CThread::~CThread()
{
    if (run) Stop();
    // Close handles
    ::CloseHandle(m_WaitThread);
    if (hThread) ::CloseHandle(hThread);
    ThreadFunction = NULL;
}

void CThread::Start()
{
    hThread = (HANDLE) _beginthreadex(NULL, 0, CThread::ThreadFunctionInternal, (LPVOID) this, 0, &ThreadID);
    run = true;
    mActive = true;
    ::ResetEvent(m_WaitThread);

    ResetCounter();
}

void CThread::Stop()
{
    if (run)
    {
        ::PostThreadMessage(ThreadID,WM_CTHREAD_STOP,0,0);
        run = false;
        mActive = false;
        //如果是 suspend 狀態中停止執行緒，將會造成 Thread 無法完全釋放，所以要在結束前使狀態resume
        if (m_runStatus == CTHREAD_STATUS_SUSPEND)
            Resume();

        if (looping)
        {
            // Trigger thread to stop
            ::WaitForSingleObject(m_WaitThread, INFINITE);//5000 );
            if (hThread) ::CloseHandle(hThread);
            hThread = NULL;

        }
        m_runStatus = CTHREAD_STATUS_STOP;
    }
}

void CThread::Suspend()
{
    if(m_runStatus != CTHREAD_STATUS_SUSPEND)
    {
        ::SuspendThread(hThread);
        m_runStatus = CTHREAD_STATUS_SUSPEND;
        suspend=true;
    }
}

void CThread::Resume()
{
    if(m_runStatus != CTHREAD_STATUS_RESUME)
    {
        ::ResumeThread(hThread);
        m_runStatus = CTHREAD_STATUS_RESUME;
        suspend=false;
    }
    ResetCounter();
}

void CThread::ResetCounter()
{
    LARGE_INTEGER utime;
    QueryPerformanceCounter(&utime);
    mElapsedTime = utime.QuadPart;
}

void CThread::Execute(void)
{

}

UINT __stdcall CThread::ThreadFunctionInternal(LPVOID pvParam)
{
    CThread *pParent = (CThread *)pvParam;

    while(pParent->run)
    {

        //==================================================================================================
        //接受外部訊息
        //==================================================================================================
        if (PeekMessage(&pParent->Msg, NULL, 0, 0, PM_REMOVE))
        {
            pParent->OnEvent(&pParent->Msg);
            if (pParent->Msg.message == WM_CTHREAD_STOP)
            {
                pParent->run = false;
                break;
            }

            TranslateMessage(&pParent->Msg);
            DispatchMessage(&pParent->Msg);
        }

        // Check event for stop thread
        if (pParent->ThreadFunction)
        {//call 外部指定的 function pointer
            if (pParent->mActive)
            {
                pParent->ThreadFunction(pParent->ThreadParam);
            }
            else
                Sleep(10);
        }
        else
        {//使用 c++ 繼承方式
            if (pParent->mActive)
            {
                pParent->Execute();
            }
            else
                Sleep(10);
        }
        if (!pParent->looping) break;
        if (pParent->m_runStatus == CTHREAD_STATUS_STOP)
            pParent->m_runStatus = CTHREAD_STATUS_START;


        //==================================================================================================


    }

    // Set event
    ::SetEvent(pParent->m_WaitThread);
    return 0;
}


unsigned long CThread::GetElapsedTime()
{
    LARGE_INTEGER utime;
    QueryPerformanceCounter(&utime);

    mLastTime = utime.QuadPart - mElapsedTime;
    return mLastTime*1000/m_liPerfFreq.QuadPart; // 微秒轉毫秒
}

//std::string CThread::GetElapsedTimeStr()
//{
//    unsigned long mTime = GetElapsedTime();
//    struct tm *FTime;
//    char strTmp[128] = "";
//    time_t time_tmp = mTime/1000;
//    FTime = gmtime(&time_tmp);
//    sprintf(strTmp, "%02d:%02d:%02d.%03d", FTime->tm_hour, FTime->tm_min, FTime->tm_sec,mTime%1000);
//    return std::string(strTmp);
//}







