//---------------------------------------------------------------------------

#ifndef CThreadH
#define CThreadH

#include <windows.h>
#include <process.h>
#include <string>
#include "CLock.h"
//---------------------------------------------------------------------------

#define WM_CTHREAD_BASE         WM_USER + 1300
#define WM_CTHREAD_STOP         WM_CTHREAD_BASE + 1



typedef enum {CTHREAD_STATUS_START=0,CTHREAD_STATUS_STOP,CTHREAD_STATUS_SUSPEND,CTHREAD_STATUS_RESUME}TThreadRunningStatus;
class CThread
{
    private://variable
        volatile bool run;
        volatile bool suspend;
        volatile bool mActive;
        bool looping;
        LARGE_INTEGER m_liPerfFreq;// 取得目前 CPU frequency
        __int64 mElapsedTime,mLastTime;

        TThreadRunningStatus m_runStatus;
        int toggleCount;
        HANDLE m_WaitThread;
        HANDLE hThread;
        unsigned int ThreadID;


    private://function
        static UINT WINAPI ThreadFunctionInternal(LPVOID pvParam);

    protected:

    public://variable
        void *ThreadParam;
        MSG Msg;
        CCriticalSection cs_local;   //critical session 的作用範圍只在存取此Thread的變數保護
        static CCriticalSection cs_static; //所有繼承自CThread 的Thread 物件共同受cs_static 影響

    public://function
        CThread();
        ~CThread();
        virtual void Start();
        virtual void Stop();
        virtual void Suspend();
        virtual void Resume();
        void SetLooping(bool value){looping = value;};
        HANDLE GetThreadHandle(){return hThread;};
        unsigned int GetThreadID(){return ThreadID;};
        virtual void OnEvent(MSG *msg){};
        TThreadRunningStatus GetThreadRunningStatus(){return m_runStatus;};
        bool IsRunning(){return run;};
        bool IsSuspend(){return suspend;};
        int  (*ThreadFunction)(void *param);
        unsigned long GetElapsedTime();
//        std::string GetElapsedTimeStr();
        virtual void Execute(void);
        void ResetCounter();
       // __property bool Active={read=mActive,write=mActive};
};

#endif
