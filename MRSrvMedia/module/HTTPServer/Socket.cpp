/*
   Socket.cpp

   Copyright (C) 2002-2004 Rene Nyffenegger

   This source code is provided 'as-is', without any express or implied warranty. In no event will the author be held liable for any damages
   arising from the use of this software.

   Permission is granted to anyone to use this software for any purpose,
   including commercial applications, and to alter it and redistribute it
   freely, subject to the following restrictions:

   1. The origin of this source code must not be misrepresented; you must not
      claim that you wrote the original source code. If you use this source code
      in a product, an acknowledgment in the product documentation would be
      appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
      misrepresented as being the original source code.

   3. This notice may not be removed or altered from any source distribution.

   Rene Nyffenegger rene.nyffenegger@adp-gmbh.ch
*/

#include "stdafx.h"
#include "Socket.h"


#include <iostream>

#pragma comment(lib, "ws2_32.lib")

using namespace std;
namespace HS
{
namespace Net
{


//int Socket::nofSockets_= 0;

void Socket::Start()
{
    if (!nofSockets_)
    {
        WSADATA info;
        if (WSAStartup(MAKEWORD(2,0), &info))
        {
            throw "Could not start WSA";
        }
    }
    ++nofSockets_;
}

void Socket::End()
{
    WSACleanup();
}

Socket::Socket() : s_(0),Mode(cmTCP)//Mode(cmTCP)
{
	nofSockets_ = 0;
    Start();
    // UDP: use SOCK_DGRAM instead of SOCK_STREAM
    if (Mode == cmTCP)
        s_ = socket(AF_INET,SOCK_STREAM,0);
    else if (Mode == cmUDP)
        s_ = socket(AF_INET,SOCK_DGRAM,0);
    bool bDontLinger=true;

    setsockopt(s_,SOL_SOCKET,SO_DONTLINGER,(const char*)&bDontLinger,sizeof(bool));
    if (s_ == INVALID_SOCKET)
    {
        throw "INVALID_SOCKET";
    }

    refCounter_ = new int(1);
}

Socket::Socket(SOCKET s) : s_(s)
{
    Start();
    refCounter_ = new int(1);
};

Socket::~Socket()
{
    if (! --(*refCounter_))
    {
        Close();
        delete refCounter_;
    }

    --nofSockets_;
    if (!nofSockets_) End();
}

Socket::Socket(const Socket& o)
{
    refCounter_=o.refCounter_;
    (*refCounter_)++;
    s_         =o.s_;

    nofSockets_++;
}

Socket& Socket::operator=(Socket& o)
{
    (*o.refCounter_)++;

    refCounter_=o.refCounter_;
    s_         =o.s_;

    nofSockets_++;

    return *this;
}

int Socket::Close()
{
    shutdown(s_,0x02);
    int ret=closesocket(s_);
    s_ = 0;
    return ret;
}

int Socket::Send(SOCKET s,char *buf,int len,int flags)
{
    int iR=-1;
    if(m_Type==BlockingSocket)
    {
        iR = send (s, buf, len, flags);
    }
    else
    {
        int errcnt=0;
        while (iR<0)
        {
            char r;
            iR = send (s, buf, len, flags);
            if (iR == SOCKET_ERROR)
            {
                if (WSAGetLastError()==WSAEWOULDBLOCK)
                {
                    Sleep(30);
                    errcnt++;
                    if (errcnt>5) break;
                    continue;
                }
                else
                {
                    shutdown(s,0x02);
                    closesocket(s);
                    break;
                }
                break ;
            }
        }
    }
    return iR;    
}

int Socket::Recv(SOCKET s,char *buf,int len,int flags)
{
    if(m_Type==BlockingSocket)
        return recv (s, buf, len, flags);
    else
    {

        int iR=-1;
        int errcnt=0;
        while (iR<0)
        {
            char r;
            iR = recv (s, buf, len, flags);
            if (iR == SOCKET_ERROR)
            {
                if (WSAGetLastError()==WSAEWOULDBLOCK)
                {
                    Sleep(1);
                    errcnt++;
                    if (errcnt>30) break;
                    continue;
                }
                else
                {
                    shutdown(s,0x02);
                    closesocket(s);
                    break;
                }

            }
        }
        return iR;
    }
}

std::string Socket::ReceiveBytes()
{
    std::string ret;
    char buf[1024];

    while (1)
    {
        u_long arg = 0;
        if (ioctlsocket(s_, FIONREAD, &arg) != 0)
            break;

        if (arg == 0)
            break;

        if (arg > 1024) arg = 1024;

        int rv = Recv (s_, buf, arg, 0);
        if (rv <= 0) break;

        std::string t;

        t.assign (buf, rv);
        ret += t;

    }

    return ret;
}

std::string Socket::ReceiveBytes(int size)
{
    int iR,cnt=0;
    std::string ret;
	u_long arg = 0;

	if (size>1024) 
		arg = 1024;
	else
		arg = size;


    while (cnt<size)
    {
        char r[1024];
        iR = Recv(s_, r, arg, 0);
        if (iR == SOCKET_ERROR)
        {
            if (WSAGetLastError()==WSAEWOULDBLOCK) continue;
        }
        else if (iR == 0) break;
        else if (iR == -1)
        {
            break;
        }
        cnt+=iR;

		ret.append(r,iR);
		//std::string t;
		//t.assign (r, iR);
  //      ret += t;
    }
	if (ret.length() != size)
	{
		cout << "Receive not complete!" <<"\r\n";
		return "";
	}
    return ret;
}

std::vector<char> Socket::ReceiveBuffer(int size)
{
	std::vector<char> rbuf;
	int iR,cnt=0;
	u_long arg = 0;

	if (size>1024) 
		arg = 1024;
	else
		arg = size;

	FILE *fp = fopen("R:\\mpp.recv.bin","wb");
	

	while (cnt<size)
	{
		char r[1024];

		iR = Recv(s_, r, arg, 0);
		if (iR == SOCKET_ERROR)
		{
			if (WSAGetLastError()==WSAEWOULDBLOCK) continue;
		}
		else if (iR == 0) break;
		else if (iR == -1)
		{
			break;
		}
		cnt+=iR;

		fwrite(r,iR,1,fp);

		std::copy(r,r+iR,std::back_inserter(rbuf));
	}
		fclose(fp);
	return rbuf;
}

std::string Socket::ReceiveLine()
{
    std::string ret;
    DWORD dwStart;
    dwStart = GetTickCount();

    while (1)
    {
        char r;

        switch(Recv(s_, &r, 1, 0))
        {
			case 0: // not connected anymore;
				// ... but last line sent
				// might not end in \n,
				// so return ret anyway.
				return ret;
			case -1:
			{

				return "";
			}

        }

        ret += r;
        if (r == '\n')  return ret;

		//If without '\n' then check timeout
		if (GetTickCount()- dwStart >5000) return ret;  //add by David on 2014/7/2

    }
}


void Socket::SendLine(std::string s)
{
    s += "\r\n";
    int ret=Send(s_,(char*)s.c_str(),s.length(),0);
}

void Socket::SendBytes(const std::string& s)
{
    char *p;
    int ret,len=s.length();
    p = (char*)s.c_str();
    while(len>0)
    {
        if (len>=1024)
            ret = Send(s_,p,1024,0);
        else
            ret = Send(s_,p,len,0);


        if (ret<0) continue;
        p+=ret;
        len-=ret;
    }
}


int Socket::SendBytes(const char *buffer,int size)
{
    char *p;
    int errcnt=0;
    int ret=0,len=size;
    p = (char*)buffer;

    //printf("strlen(p)=%d\n",strlen(p));

    while(len>0)
    {

        fd_set fds;
        FD_ZERO(&fds);
        FD_SET(s_,&fds);
        timeval tv;
        tv.tv_sec = 5000 /1000;
        tv.tv_usec = (5000%1000)*1000;

        int rc = select(s_+1, NULL,&fds, NULL, &tv);

        if (FD_ISSET(s_,&fds) && rc>0)
        {//Writable

            if (len>=1024)
                ret = Send(s_,p,1024,0);
            else
                ret = Send(s_,p,len,0);
    //        ret = Send(s_,p,len,0);
    //
    //        printf("socket send:%d\n",ret);
        }
        else
        {
            printf("Write fail!\n");
        }

        if (ret<0)
        {
            errcnt++;
            unsigned int xs = ::GetLastError();
            if (errcnt>10) break;
            printf("Send error!\n");
            continue;
        }
        errcnt = 0;
        p+=ret;
        len-=ret;
    }
    return ret;
}

void Socket::SetSocketBlockType(TypeSocket type)
{
    u_long arg = type;
    ioctlsocket(s_, FIONBIO, &arg);
}

void Socket::GetPeerIPAddress(std::string &str)
{
    //取得遠端 IPAddreee
    char strIP[32]="";
    unsigned long nIP;
    sockaddr dev;
    SOCKADDR_IN *in = (SOCKADDR_IN*)&dev;
    int devlen = sizeof(sockaddr);
    getpeername(s_,&dev,&devlen);
    nIP = in->sin_addr.S_un.S_addr;
			
    sprintf(strIP,"%d.%d.%d.%d",(nIP&0x000000FF),(nIP&0x0000FF00)>>8,(nIP&0x00FF0000)>>16,(nIP&0xFF000000)>>24);
    str = strIP;
}

void Socket::GetPeerIPAddressPort(std::string &str)
{
	//取得遠端 IPAddreee
	char strIP[32]="";
	unsigned long nIP;
	sockaddr dev;
	SOCKADDR_IN *in = (SOCKADDR_IN*)&dev;
	int devlen = sizeof(sockaddr);
	getpeername(s_,&dev,&devlen);
	nIP = in->sin_addr.S_un.S_addr;
			
	sprintf(strIP,"%d.%d.%d.%d:%d",(nIP&0x000000FF),(nIP&0x0000FF00)>>8,(nIP&0x00FF0000)>>16,(nIP&0xFF000000)>>24,in->sin_port);
	str = strIP;
}

void Socket::GetLocalIPAddress(std::string &str)
{
    //取得本地 IPAddreee
    char strIP[32]="";
    unsigned long nIP;
    sockaddr dev;
    SOCKADDR_IN *in = (SOCKADDR_IN*)&dev;
    int devlen = sizeof(sockaddr);
    getsockname(s_,&dev,&devlen);
    nIP = in->sin_addr.S_un.S_addr;

    sprintf(strIP,"%d.%d.%d.%d",(nIP&0x000000FF),(nIP&0x0000FF00)>>8,(nIP&0x00FF0000)>>16,(nIP&0xFF000000)>>24);
    str = strIP;
}

void Socket::GetLocalDefaultGateway(std::string &str)
{
        // Get local host name
        char szHostName[128] = "";
        std::string localIP;
        GetLocalIPAddress(localIP);


        //------------------------------------
        //------------------------------------
        if(gethostname(szHostName, sizeof(szHostName)))
        {
                // Error handling -> call 'WSAGetLastError()'
        }

        SOCKADDR_IN socketAddress;
        hostent *pHost        = 0;

        // Try to get the host ent

        pHost = gethostbyname(szHostName);
        if(!pHost)
        {
                // Error handling -> call 'WSAGetLastError()'
        }


        char ppszIPAddresses[10][16]; // maximum of ten IP addresses
        for(int iCnt = 0; (pHost->h_addr_list[iCnt]) && (iCnt < 10); ++iCnt)
        {
                memcpy(&socketAddress.sin_addr, pHost->h_addr_list[iCnt], pHost->h_length);
                strcpy(ppszIPAddresses[iCnt], inet_ntoa(socketAddress.sin_addr));
                str = std::string(ppszIPAddresses[iCnt]);
        }
}

BOOL Socket::CheckSocketReadable(int timeout_ms)
{
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(s_,&fds);
    timeval tv;
    tv.tv_sec = timeout_ms /1000;
    tv.tv_usec = (timeout_ms%1000)*1000;

    int rc = select(s_+1, &fds, NULL, NULL, &tv);
    if (FD_ISSET(s_,&fds)) return true;
    return false;

}

BOOL Socket::CheckSocketWriteable(int timeout_ms)
{
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(s_,&fds);
    timeval tv;
    tv.tv_sec = timeout_ms /1000;
    tv.tv_usec = (timeout_ms%1000)*1000;

    int rc = select(s_+1, NULL,&fds, NULL, &tv);
    if (FD_ISSET(s_,&fds)) return true;
    return false;

}


SocketServer::SocketServer(int port, int connections, TypeSocket type)
{
    //connections 是指在完成真正連線前(即 Server 未 accept() Client 要求前)，所能接受的要求次數，不是指該 Server Socket 可以服務的 Client 數量
    sockaddr_in sa;

    memset(&sa, 0, sizeof(sa));

    sa.sin_family = PF_INET;
    sa.sin_port = htons(port);
//    s_ = socket(AF_INET, SOCK_DGRAM, 0);
//    if (s_ == INVALID_SOCKET)
//    {
//        throw "INVALID_SOCKET";
//    }
    m_Type = type;
    if(type==NonBlockingSocket)
    {
        u_long arg = 1;
        ioctlsocket(s_, FIONBIO, &arg);
    }

    /* bind the socket to the internet address */
    if (bind(s_, (sockaddr *)&sa, sizeof(sockaddr_in)) == SOCKET_ERROR)
    {
        closesocket(s_);
        throw "INVALID_SOCKET";
    }
    m_connections = connections;

}

int SocketServer::Listen()
{
    if (this && s_ && s_ > 0)
        return listen(s_, m_connections);
    else
        return -1;

}

Socket* SocketServer::Accept()
{
    if (s_==NULL) return NULL;
    SOCKET new_sock = accept(s_, 0, 0);

    if (new_sock == INVALID_SOCKET)
    {
        int rc = WSAGetLastError();
        if(rc==WSAEWOULDBLOCK)
        {
            return 0; // non-blocking call, no request pending
        }
        else
        {
            return 0;												                      
        }
    }

    Socket* r = new Socket(new_sock);
    return r;
}

SocketClient::SocketClient(const std::string& host, int port, TypeSocket type) : Socket()
{
    std::string error;
    mHost = host;
    mPort = port;
    hostent *he;
    if ((he = gethostbyname(host.c_str())) == 0)
    {
        error = strerror(errno);
        throw error;
    }

    m_Type = type;
    if(type==NonBlockingSocket)
    {
        u_long arg = 1;
        ioctlsocket(s_, FIONBIO, &arg);
    }


    sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr = *((in_addr *)he->h_addr);
    memset(&(addr.sin_zero), 0, 8);

    int err = Connect(s_, (sockaddr *) &addr, sizeof(sockaddr),3000);
}

int SocketClient::Connect(SOCKET s,const struct sockaddr *name,int namelen,int timeout_ms)
{

    int err = ::connect(s_, name, sizeof(sockaddr));

    if(m_Type==NonBlockingSocket && err == SOCKET_ERROR)
    {
        struct timeval oTV;
        oTV.tv_sec = timeout_ms / 1000;
        oTV.tv_usec = (timeout_ms%1000)*1000;

        fd_set oRead, oWrite;
        FD_ZERO(&oRead);
        FD_ZERO(&oWrite);
        int nResult, nError;

        if (WSAGetLastError() != WSAEWOULDBLOCK)
        {
            printf("Connection failed, ec:%d\n", WSAGetLastError());
            closesocket(s_);
            return -1;
        }
        else // need select
        {
            FD_SET(s_, &oRead);
            oWrite = oRead;
            nResult = select(s_+1, &oRead, &oWrite, 0, &oTV);
            if (nResult == 0)
            {
                printf("Connection timeout\n");
                closesocket(s_);
                return -1;
            }
            if (FD_ISSET(s_, &oRead) || FD_ISSET(s_, &oWrite))
            {
                int nLen = sizeof(nError);
                if (getsockopt(s_, SOL_SOCKET, SO_ERROR, (char*)&nError, &nLen) < 0)
                {
                    printf("Connect error %d\n", nError);
                    closesocket(s_);
                    return -1;
                }
            }
            else
            {
                printf("Unknown err in connect\n");
                closesocket(s_);
                return -1;
            }
        }
    } // else connected immediately
    return err;
}

SocketSelect::SocketSelect(Socket const * const s1, Socket const * const s2, TypeSocket type)
{
    FD_ZERO(&fds_);
    FD_SET(const_cast<Socket*>(s1)->s_,&fds_);
    if(s2)
    {
        FD_SET(const_cast<Socket*>(s2)->s_,&fds_);
    }

    TIMEVAL tval;
    tval.tv_sec  = 0;
    tval.tv_usec = 1;

    TIMEVAL *ptval;
    if(type==NonBlockingSocket)
    {
        ptval = &tval;
    }
    else
    {
        ptval = 0;
    }

    if (select (0, &fds_, (fd_set*) 0, (fd_set*) 0, ptval) == SOCKET_ERROR)
        throw "Error in select";

}

bool SocketSelect::Readable(Socket const* const s)
{
    if (FD_ISSET(s->s_,&fds_)) return true;
    return false;
}

std::string HttpSocketServer::ReceiveHeader()
{
    std::string ret;
    unsigned long dwStart=0;

    while (1)
    {
        if (!CheckSocketReadable()) return "";
        std::string r=ReceiveLine();
        ret += r;
        if (ret =="")  return "";
   //     {
			//dwStart++;
			//if (dwStart>10) return ""; //Timeout
   //     }
        if (r=="\n" || r=="\r\n") break;
    }
    return ret;
}

std::string HttpSocketClient::ReceiveHeader()
{
    std::string ret="";
    unsigned long dwStart=0;
    while (1)
    {
        if (!CheckSocketReadable()) return "";
        std::string r=ReceiveLine();
        ret += r;
        if (ret =="" && GetTickCount() - dwStart >=5000)
        {
            return ""; //Timeout
        }
        if (r=="\n" || r=="\r\n") break;
        Sleep(1);
    }
    return ret;
}


std::string HttpSocketClient::GetHeaderInfo(std::string HeaderString,std::string HeaderName)
{
    return StringBetween(HeaderString,HeaderName,"\n");
}

std::string HttpSocketClient::StringBetween(std::string s,std::string startstr,std::string endstr)
{
    int start = s.find(startstr,0)+startstr.length();
    int end = s.find(endstr,start);

    if (end==-1)
    {
        return "";
    }
    return s.substr(start,end-start);
}


}
}