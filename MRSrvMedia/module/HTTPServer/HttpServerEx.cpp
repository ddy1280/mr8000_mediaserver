//---------------------------------------------------------------------------
#include "stdafx.h"
#include <winsock2.h> 
#include <vector>

#include <iostream>
#include <istream>
#include <ostream>
#include <fstream>
#include <ctime>
#include <process.h>
#include "Socket.h"
#include "HttpServerEx.h"			   
#include "String/StringUtility.h"
#include "File/cfileutility.h"
#include <mstcpip.h> 

//#include <Ws2tcpip.h>


// Link with ws2_32.lib
//#pragma comment(lib, "Ws2_32.lib")


//---------------------------------------------------------------------------
using namespace std;
#pragma comment (lib,"String.lib")
#pragma comment (lib,"File.lib")
using namespace HS::String;

namespace HS
{
namespace Net
{



//=============================================================================================================================

//=============================================================================================================================
HttpServer::HttpServer(int port, int connections, SOCKET_MODE type)
{
    Listen(port,connections,type);
    mUniID = 0;
    mCount = 0;
    m_status = false;
    ExitFlag = true;

	handleHttpCallback = NULL;
	handleHttpRequest = NULL;
	handleHttpCallback_Param = NULL;
	handleHttpRequest_Param = NULL;
	evWaitThread = new CEvent(TRUE, FALSE, NULL);
	evWaitcallback= new CEvent(TRUE, TRUE, NULL);


}

HttpServer::HttpServer()
{

	mUniID = 0;
	mCount = 0;
	m_status = false;
	ExitFlag = true;

	handleHttpCallback = NULL;
	handleHttpRequest = NULL;
	handleHttpCallback_Param = NULL;
	handleHttpRequest_Param = NULL;
	evWaitThread = new CEvent(TRUE, FALSE, NULL);
	evWaitcallback= new CEvent(TRUE, TRUE, NULL);
	evWaitWorkingThread=new CEvent(TRUE, TRUE, NULL);

}

void HttpServer::Listen(int port,int connections, SOCKET_MODE type)
{
	mPort = port;
	mConnections = connections;
    mType = type;
}

HttpServer::~HttpServer()
{
    if (httpHandle)
        Stop();			  


    delete evWaitThread;
	delete evWaitcallback;
	delete evWaitWorkingThread;

}

void HttpServer::Start()
{
	std::ifstream in("DEBUG.INI");
	isDebug = in.good();
	if (isDebug)
	{
		in.seekg(0, std::ios::end);
		DebugOptionContents.resize(in.tellg());
		in.seekg(0, std::ios::beg);
		in.read(&DebugOptionContents[0], DebugOptionContents.size());
		in.close();

		DebugOptionContents = BetweenReplaceString(DebugOptionContents,";","\n","",true);

	}


    if (ExitFlag == true)
    {
		try
		{

			HttpSocketServer *in = new HttpSocketServer(mPort,mConnections,(TypeSocket) mType);
			httpHandle = (HTTP_SERVER_HANDLE) in;
			ExitFlag = false;

			listenInfo.exitflag = false;
			listenInfo.Parent = this;
			listenInfo.hThreadListen = (HANDLE) _beginthreadex(NULL, 0, HttpServer::HttpListenFunc,(LPVOID) &listenInfo,0,&listenInfo.hListenThreadID);
			m_status = true;
		}
		catch (std::exception& e)
		{
			throw "INVALID_SOCKET";
		}

    }
}

void HttpServer::Stop()
{
    if (ExitFlag == false)																				
    {


		//避免call back繼續呼叫
		//if(handleHttpRequest_Param)
		//CloseHandle(handleHttpRequest_Param);
		//if(handleHttpRequest)
		//	CloseHandle(handleHttpRequest);
		//handleHttpRequest_Param = NULL;
		//handleHttpRequest = NULL;

        listenInfo.exitflag = true;
        evWaitThread->WaitFor(3000); //等待 Thread 結束
		evWaitWorkingThread->WaitFor(3000);

		//立即斷線
		linger LingerOption;
		LingerOption.l_onoff = FALSE;
		LingerOption.l_linger = 0; //設定 TimeOut 秒數

		HttpSocketServer *in = (HttpSocketServer*)httpHandle;
		setsockopt( in->GetSocket() ,SOL_SOCKET,SO_LINGER, (char*)&LingerOption,sizeof(linger));

		//BOOL bDontLinger = FALSE;
		//setsockopt(in->GetSocket(),SOL_SOCKET,SO_DONTLINGER,(const char*)&bDontLinger,sizeof(BOOL));

		in->Close();

		ExitFlag = true;
        CloseHandle(listenInfo.hThreadListen);
        m_status = false;


		
        delete in;
        in = NULL;
    }
}

UINT WINAPI HttpServer::HttpListenFunc(void* param)
{
	//std::cerr << "HttpListenFunc thread start \r\n";
		TListenInfo *listinfo = (TListenInfo*) param;
		HttpServer *obj = (HttpServer*) listinfo->Parent;
		HttpSocketServer *serv = (HttpSocketServer*) obj->httpHandle;			   
	
    while (!listinfo->exitflag)
    {
				Sleep(10); //Sleep 放 accept()前面，用意是即使沒有連線一樣會先 sleep而不會佔cpu時間
				 
        if (serv->Listen()!=0)
        {
            closesocket(serv->GetSocket());

            obj->cs.Enter();
            listinfo->exitflag = true;
            obj->cs.Leave();
            break;
        }

				if (obj->handleHttpCallback)
					obj->handleHttpCallback();


				Socket* s=serv->Accept();
				if (s==NULL)
				{
					continue;
				}
				
				int keep_alive=0;
				int nRet = setsockopt(s->GetSocket(), SOL_SOCKET, SO_KEEPALIVE,	(char*)&keep_alive, sizeof(keep_alive));
				if (nRet == SOCKET_ERROR)
				{
					continue;
				}
				// set KeepAlive parameter
				tcp_keepalive alive_in;
				tcp_keepalive alive_out;
				alive_in.keepalivetime      = 500;  //0.5s
				alive_in.keepaliveinterval  = 1000; //1s
				alive_in.onoff              = TRUE;
				unsigned long ulBytesReturn = 0;
				nRet = WSAIoctl(s->GetSocket(), SIO_KEEPALIVE_VALS, &alive_in, sizeof(alive_in),&alive_out, sizeof(alive_out), &ulBytesReturn, NULL, NULL);
				if (nRet == SOCKET_ERROR)
				{
					continue;
				}


				std::cerr << "==>ACCEPT\r\n";

				TConnectInfo *newClient = new TConnectInfo;
				s->GetPeerIPAddressPort(newClient->keyID);
				newClient->Parent = obj;
				newClient->ThreadIndex =obj->mUniID++;
				newClient->exitflag = false;
				newClient->socket = (SOCKET_HANDLE)s;
	 			newClient->hThreadAccept = (HANDLE) _beginthreadex(NULL, 0, HttpServer::HttpWorkingThreadFunc,(LPVOID) newClient,0,&newClient->hAcceptThreadID);
				CloseHandle(newClient->hThreadAccept);

				obj->m_ClientConnections.push_back(newClient);
		

    }
	
	closesocket(serv->GetSocket());

	


	std::vector<TConnectInfo*>::iterator iter = obj->m_ClientConnections.begin();
	for (;iter !=  obj->m_ClientConnections.end();)  
	{
		TConnectInfo *connInfo=(TConnectInfo*)(*iter);
		connInfo->exitflag=true;
		iter = obj->m_ClientConnections.erase(iter);

	}	
	
	//std::cerr << "==>HttpListenFunc thread end\r\n";
	obj->evWaitThread->SetEvent();
    return 0;
}


UINT WINAPI HttpServer::HttpWorkingThreadFunc(void* param)
{
	//std::cerr << "HttpWorkingThreadFunc thread start \r\n";
	TConnectInfo *connInfo = (TConnectInfo*) param;
	HttpServer *obj = (HttpServer*) connInfo->Parent;
	HttpSocketServer *in = (HttpSocketServer*) obj->httpHandle;
	Socket *s = (Socket*)connInfo->socket;
  MSG msg;
					  
	bool bKeep_alive=false;
	obj->evWaitWorkingThread->ResetEvent();
	while (!connInfo->exitflag)
	{
		/*try
		{*/

			//====================================================================
			std::string header;
			//Get http header information
			if (!obj->GetHeaderMember(connInfo))
			{//通常會進入這裡的，表示不是正常的HTTP header, 有可能是Check port 的封包
				break;
				//continue;
			}


			/*if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				if (msg.message == WM_THREAD_EXIT)
				{
					playlist->DecodeThreadWDG.Active = false;
					dtinfo->bKillThread = true;
					break;
				}
			}*/
			//////cout <<"Http Server Thread Alive...\n";
			request req;
			reply rep;


			//取得遠端 IPAddreee
			std::string strIP;
			s->GetPeerIPAddress(strIP);   //---->這個方式取得的IP 若裝置在內網時能找到Gateway IP Address
			req.remote_ipaddress = strIP;
			req.raw_header = obj->HeaderString;
			req.headers.resize(4);
			req.headers[0].name = "Host";
			req.headers[0].value = connInfo->Header.Host;

			req.headers[1].name = "Content-Length";
			req.headers[1].value = ToString(connInfo->Header.ContentLength);

			req.headers[2].name = "Content-Type";
			req.headers[2].value = connInfo->Header.ContentType;

			req.headers[3].name = "Connection";
			req.headers[3].value = connInfo->Header.ConnectionType;

			req.hSocket=s->GetSocket();


			int find=connInfo->Header.ConnectionType.find("Keep-Alive");
			if(find>=0)
			{
					bKeep_alive=true;
					printf("====>Socket=%d\n",s->GetSocket());
			}
			/*int keep_alive=0;

			if(connInfo->Header.ConnectionType=="Keep-Alive")
			{
   				int nRet = setsockopt(s->GetSocket(), SOL_SOCKET, SO_KEEPALIVE,	(char*)&keep_alive, sizeof(keep_alive));
					if (nRet == SOCKET_ERROR)
					{
							break;
					}
					// set KeepAlive parameter
					tcp_keepalive alive_in;
					tcp_keepalive alive_out;
					alive_in.keepalivetime      = 500;  //0.5s
					alive_in.keepaliveinterval  = 1000; //1s
					alive_in.onoff              = TRUE;
					unsigned long ulBytesReturn = 0;
					nRet = WSAIoctl(s->GetSocket(), SIO_KEEPALIVE_VALS, &alive_in, sizeof(alive_in),&alive_out, sizeof(alive_out), &ulBytesReturn, NULL, NULL);
					if (nRet == SOCKET_ERROR)
					{
							break;
					}
			}		*/	

			req.method = GetToken(connInfo->Header.RequestLine,0," ");
			if (req.method == "GET")
			{
				req.cmd = GetToken(connInfo->Header.RequestLine,1," ");	
				if (req.cmd.find("?")!=std::string::npos)
				{
					req.post_data = GetToken(req.cmd,1,"?");
					req.cmd = GetToken(req.cmd,0,"?");
				}
			}
			if (req.method == "POST")
			{
				req.cmd = GetToken(connInfo->Header.RequestLine,1," ");	
				if (connInfo->Header.ContentLength>0)
				{
					req.post_data = s->ReceiveBytes(connInfo->Header.ContentLength);
				}
			}

			if (obj->handleHttpRequest)
				obj->handleHttpRequest(req,rep,obj->handleHttpRequest_Param);

			//Ethan:之後要加上去
			//if(!bKeep_alive)
				s->SendLine(rep.to_strings());

			if (obj->isDebug)
			{
				int timestmp = time(0);
				std::string SrvName = ToUpper(obj->mInformation.substr(0,obj->mInformation.find("_")));
				std::string cmdName = ToUpper(req.cmd.substr(1));
				std::wstring path = HS::File::CreateFolder(L"ProtocolMessages");


				if (obj->DebugOptionContents.length()>0)
				{
					//; ";" 表示disable
					//; Register command ==> REG.XXXX
					//; Log command ==>LOG.XXXX
					//;
					//; Register Server command
					//REG.SERVER_JOIN
					//REG.SERVER_LEAVE
					//REG.SERVER_ALIVE
					//REG.SERVER_QUERY
					//REG.JOIN
					//REG.ALIVE
					//REG.LEAVE
					//REG.QUERY
					//REG.READ_CONFIG
					//REG.WRITE_CONFIG
					//REG.AUTHINFO
					//; Log Server command
					//LOG.PUSHDATA
					//LOG.LOG
					//LOG.PANIC
					//LOG.QUERY
					//LOG.READ_CONFIG
					//LOG.WRITE_CONFIG
					std::string cmdkey=SrvName+"."+cmdName;
					if (ToUpper(obj->DebugOptionContents).find(cmdkey)!=std::string::npos)
					{
						std::wstring reqFile = path + UTF8Decode("\\"+SrvName+"_["+ cmdName +"]_REQ_"+ToString(timestmp)+".TXT");
						std::wstring repFile = path + UTF8Decode("\\"+SrvName+"_["+ cmdName +"]_REP_"+ToString(timestmp)+".TXT");


						ofstream ofile;
						ofile.open(reqFile.c_str(),ios::binary);
						ofile.write(req.raw_header.data(),req.raw_header.length());
						//ofile.write(obj->HeaderString.data(),obj->HeaderString.length());
						if (connInfo->Header.ContentLength>0)
						{
							ofile.write(req.post_data.data(),req.post_data.length());
						}
						ofile.close();

						ofstream orfile;
						orfile.open(repFile.c_str(),ios::binary);
						orfile.write(rep.to_strings().data(),rep.to_strings().length());
						orfile.close();
					}
				}
			}
			if(!bKeep_alive)
				break;
			else
			{
				//printf("");
				/*
				if(obj->handleHttpStatus)
					if(obj->handleHttpStatus(s->GetSocket())==-1);
					{
						break;
					}*/
			}



		/*}
		catch(std::exception& e)
		{
			cout <<"Http Server Exception!\n";
		}*/
		Sleep(30);
	}

	if(bKeep_alive)
		printf("Stream closed\n");

	s->Close();
	delete s;
	//s=NULL;
	delete connInfo;
	connInfo=NULL;
	obj->evWaitWorkingThread->SetEvent();
	//std::cerr << "==>HttpWorkingThreadFunc thread end\r\n";
	return 0;

}


void HttpServer::SetHttpRequestHandler(EXT_HTTP_HANDLER handler,void *param)
{
	handleHttpRequest_Param = param;
	handleHttpRequest = handler;	 //只有收到Request 才會呼叫的 callback

}
void HttpServer::DeleteHttpRequestHandler()
{
	handleHttpRequest_Param = NULL;
	handleHttpRequest = NULL;	
}
void HttpServer::SetHttpUpdateHandler(EXT_HTTP_CALLBACK handler,void *param)
{
	handleHttpCallback_Param = param;
	handleHttpCallback = handler;	 //在Server thread 不管有沒有Request 都會呼叫的callback   
}

void HttpServer::SetHttpStatusHandler(CALLBACK_HTTP_STATUS handler,void *param)
{
	handleHttpStatus_Param = param;
	handleHttpStatus = handler;	 //在Server thread 不管有沒有Request 都會呼叫的callback   
}

//std::string HttpServer::ReceiveHeader(Socket *s)
//{
//    char ret[1024];
//    int i=0;
//
//    memset(ret,0,1024);
//    char r;
//    while (1)
//    {
//        switch(recv(s->GetSocket(), &r, 1, 0))
//        {
//        case 0:
//            return std::string(ret);
//        case -1:
//            return "";
//        }
//        ret[i]=r;
//        if (ret[i-3]=='\r' && ret[i-2]=='\n' && ret[i-1]=='\r' && ret[i]=='\n') break;
//        if (ret[i-1]=='\n' && ret[i]=='\n') break;
//        i++;
//    }
//    return std::string(ret);
//}


bool HttpServer::GetHeaderMember(TConnectInfo *info)
{
	std::string crlf;
	HS::Net::Socket *s = (HS::Net::Socket*)info->socket;
	THeaderMember &header = info->Header;
	int start=0,end=0;

	std::string headerStr = std::string(((HttpSocketServer*)s)->ReceiveHeader().c_str());
	HeaderString = headerStr;
	if (headerStr.length() == 0) return false;

	//////cout <<"[HTTP HEADER]" <<headerStr <<std::endl;

	if (headerStr.find("\r")!=std::string::npos)
		crlf="\r\n";
	else
		crlf="\n";

	std::string Method = headerStr.substr(0,headerStr.find(" ")-1);

	if (headerStr.find("GET")!=std::string::npos || headerStr.find("POST")!=std::string::npos ||
		headerStr.find("HEAD")!=std::string::npos || headerStr.find("PUT")!=std::string::npos ||
		headerStr.find("OPTIONS")!=std::string::npos || headerStr.find("DELETE")!=std::string::npos)
	{//Header
		header.RequestLine = headerStr.substr(0,headerStr.find(crlf)+crlf.length()-1);
		//        header.Accept = StringBetween(headerStr,"Accept: ","\n");
		//        header.AcceptLanguage = StringBetween(headerStr,"Accept-Language: ","\n");
		//
		//        header.UserAgent = StringBetween(headerStr,"User-Agent: ","\n");
		//        header.AcceptEncoding = StringBetween(headerStr,"Accept-Encoding: ","\n");
		header.Host = StringBetween(headerStr,"Host: ",crlf);
		if (headerStr.find("Content-Length: ")!=std::string::npos)
			header.ContentLength = atoi(StringBetween(headerStr,"Content-Length: ",crlf).c_str());
		if (headerStr.find("Connection: ")!=std::string::npos)
			header.ConnectionType = StringBetween(headerStr,"Connection: ","\n");
		if (headerStr.find("Content-Type: ")!=std::string::npos)
			header.ContentType = StringBetween(headerStr,"Content-Type: ","\n");


		std::string param_tmp;
		if (Method == "GET")
		{
			if (header.RequestLine.find("?")!=std::string::npos)
			{
				header.Document = StringBetween(header.RequestLine," ","?");
				param_tmp = StringBetween(header.RequestLine,"?"," ");
			}
			else
				header.Document = StringBetween(header.RequestLine," "," ");

			header.Param = param_tmp;
		}
		else if (Method == "POST")
		{
			header.Document = StringBetween(header.RequestLine," "," ");

			if (header.ContentLength>0)
			{
				//                std::string tmpstr = s->ReceiveBytes(header.ContentLength);
				//				param_tmp = std::string(tmpstr.c_str());
				header.Param = s->ReceiveBytes(header.ContentLength);
			}


		}

 		return true;
	}
    return false;
}



std::string HttpServer::StringBetween(std::string s,std::string startstr,std::string endstr)
{
    std::string _s = std::string(s.c_str());
    std::string _startstr = std::string(startstr.c_str());
    std::string _endstr = std::string(endstr.c_str());
    int start = _s.find(_startstr,0)+_startstr.length();
    int end = _s.find(_endstr,start);

    if (end==-1)
    {
        cout << "ERR:Parsing error!\n";
        return "";
    }
    return std::string(_s.substr(start,end-start).c_str());
}

}//end of namespace Net
}//end of namespace HS