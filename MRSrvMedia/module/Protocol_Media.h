#ifndef MEDIA_PROTOCOL_H
#define MEDIA_PROTOCOL_H

#include "HTTPServerEx/HttpServerEx.h"
#include "HttpClient/CMultipart.h"

#include "Protocol.h"
//#include "ReceiveQueueMgr.h"
#include "ShmMgr.h"
#include <iostream>
#include <direct.h>

#include <map>
using namespace std;
using namespace HS;
typedef const char* (*EXT_PROCESS_HANDLER)(const char *Command, const char *Param);
enum StreamSource {HTTP=0,JPEG};
class CStreamAbility;


enum ConnectStatus
{
	CS_NONE=0,
	CS_SUCCESS,
	CS_CONNECT_FAIL,
	CS_OVER_CONNECTION_LIMIT,	
};

enum VideoFrameType
{
	V_PFREAM=0,
	V_IFREAM,
};
enum StreamType
{
	VIDEO=0,
	AUDIO,
	TEXT,
};


typedef struct SThreadInfo
{
	HANDLE hThreadListen;
	unsigned int hListenThreadID;
	void *Parent;
	bool exitflag;
	int toggleCount;
}S_ThreadInfo;

typedef struct SRequestCount
{
		int iPlayback;
		int iLive;
}S_RequestCount;
typedef std::vector<unsigned int> VectorTime;



typedef struct SRequestInfo
{
	SRequestCount s_ReqCnt;
	unsigned int iRequestID;
	string strRID;
	string strCmd;								//LIVE, PLAYBACK, TEARDOWN, SEARCH, SEARCH, SET, GET
	string strGetParam;								//取回設定資料(HS-ForceI|HS-Mute|HS-Delay) ??
	string strRequestSN;						//Request的識別碼(hash index)
	string strStreamID;
	wstring wsrtJpegPath;
	unsigned int uiTime;				//要搜尋的時間
	short nStream;							//那一個Streaming的資料 ??
	short nCH;									//那一Channel的資料
	
	unsigned short unForceI;	//是否強制只送I-Frame(Enable: 0/Disable: 1)
	unsigned short unMute;		//是否要送Audio資料(Enable:0/Disable:1)
	unsigned short unDelay;	//是否要延遲送的時間(Enable:0/Disable:1)
	
	bool bIsStartGetStream;
	bool bIsStopGetStream;
	unsigned long long ullCurrentTime;
	int iMaxConnection;
	int iConnectionNum;
	bool bResponsed;
	SOCKET hSocket;

}S_RequestInfo;

typedef struct SStreamAbility
{
		string strModel;					//DVR型號
		string strRID;						//DVR識別碼(MAC/IMEI 碼)
		string strVersion;				//DVR版本
		
		short nStreamNum;					//串流數(main/sub/…)
		short nCHNum;							//有幾個Channel串流
		short nStreamContent;			//串流內容Bit mode(video = 1, audio = 2, Text = 4)
		short nVideoType;					//影像串流格式(H.264: 96 / M.JPG: 1002 / MP4: 230)
		short nVideoBitrate;			//影像串流的。單位(k-bit/s)
		short nVideoFPS;					//影像張數
		short nVideoResolution;		//影像解析度(CIF : 1,half D1 : 3, D1 : 4)
		short nAudioType;					//聲音串流格式(PCMA: 8)
		short nAudioBitDepth;			//聲音的Bits per sample.
		short nAudioChannelNum;		//幾聲道(Mono: 1, Stereo: 2)
		unsigned short unAudioSampleRate;//聲音的Sampling Rate (Hz)
		unsigned int uiLastRequestTime;
}S_StreamAbility;

typedef struct SStreamData
{
	char strRID[34];
	char strType[34];
	char strVideoTitle[34];
	char strCarID[18];
	char strDeviceSN[18];
	char StrCompany[34];
	char strDriver[34];
	char strRequestSN[42];
	char strStreamID[34];
	unsigned long long ulUTC;
	unsigned int uiPTS;
	unsigned int uiTimezone;
	float iG_X;
	float iG_Y;
	float iG_Z;

	unsigned short unEventVloss;
	unsigned short unEventMotion;
	unsigned short unEventAlarm;
	unsigned short unAudioSampleRate;
	short nChannel;
	short nVideoBitrate;
	short nVideoFPS;
	short nVideoResolution;
	short nAudioBitDepth;
	unsigned short unVideoFrame;
	unsigned short unEventPanic;

	float fGPSLatitude;
	float fGPSLognitude;
	float fGPSAltitude;
	float fGPSSpeed;
	short nGPSHeading;
	unsigned short unGPSFix;
}S_StreamData;


class CStreamCmdResult
{
	public:
		//CStreamCmdResult();
		//~CStreamCmdResult();
		
		unsigned int REQUESTSN;
		short RESULT;
		unsigned short HS_ForceI;
		unsigned short HS_Mute;
		unsigned short HS_Delay;
};
//typedef std::map<std::string, S_RequestInfo*> MapRequestInfo;
//typedef std::map<std::string, MapRequestInfo*> MapRequest;

typedef std::deque<S_RequestInfo*> RequestQueue;
typedef std::map<std::string, S_StreamAbility*> MapStreamAbility;
typedef std::map<std::string, S_RequestInfo*> MapRequestInfo;
typedef std::map<std::string, ConnectStatus> MapRequestTable;


class CMediaProtocol : public CProtocol
{
private:

//variables
	int m_Interval;
	bool m_bIsSendRequest;
	std::string mMethod;
	RequestQueue m_qRequest;
	CMultiPart m_multiPart;
	std::wstring m_strSavePath;
	int m_iStreamSource;
	CRITICAL_SECTION m_critSec;

//functions
	time_t StringToTimestamp(string DateTimeString,bool isGMT=false);
	void DelRequestInfoFromTable(string HashIdx);
	unsigned __int64 GetUniqueID();
	map <string, string> ParameterToKeyPair(std::string param);
	map <string, string> DataKeyPair(std::string param);
	string InnerMediaServerProtocol(UNICODE_MODE_ENUM uniMode,string command, string param,SOCKET hSocket=0);
#ifdef _CLIENT_SERVICE
	string InnerMediaServerProtocol2(UNICODE_MODE_ENUM uniMode,string command, string param);
#endif
	bool to_bool(std::string str);
	S_StreamAbility* ParseAbilityToMap(map<string,string> contentMap,string param);

	void ParseStreamData(S_StreamData* pstStreamData,std::string param);

	string ProcessCmdResult(string param);
	string ProcessAbility(string Param);
	string ProcessRequest(string Param);
	string ProcessStream(std::string Param,unsigned int hSocket=0);
	//std::string ProcessGetStream(string Param);

	void ClearAllQueue(string RID);
	static UINT __stdcall HttpWatchFunc(void* param);
	bool IsRequestExist(string QueueSN);

public:

//variables
	MapStreamAbility m_mapStreamAbility;//以Device為單位，以rid為key
	MapRequestInfo	m_mapRequestInfo;   //以ch為單位，以hash index為key
	ShmMgr *m_shmMgr;
	S_ThreadInfo m_sThreadInfo;
	MapRequestTable m_mapRequestTable;

	//MapRequestConnect m_mapRequestConnect;

//functions
	CMediaProtocol();
  ~CMediaProtocol();
	virtual string ProtocolImplement(const request& req, reply& rep); //Command for WEB Interface
	virtual int GetConnectStatus(SOCKET s);
	virtual string ProtocolPostProcessing(string command,string param);

	void InitialShmMgr(ShmMgr *pRcvQueueMgr);
	std::string GetRequestStr(S_RequestInfo *pstRequest);

	int AddRequest(string RID,string StreamType,int StreamID,int CH,unsigned int UTC,int SessionID);
	bool ReleaseRequest(string RID,string StreamType,int StreamID,int CH,unsigned int UTC,int SessionID);
	//std::string GetStreamTime(string RID,int CH,string CMD,unsigned int UTC);
	void SetStreamSource(int Mode);
	void SetJpegPath(wchar_t *Path);
	std::string GetClientSN(string RID,string CMD,int StreamNum,int CH,unsigned int UTC,int SessionID);
	std::string GetStreamSN(string RID,string CMD,int StreamNum,int CH,unsigned int UTC);
#ifdef _CLIENT_SERVICE
	virtual string ProtocolImplement2(const request& req, reply& rep); //Command for WEB Interface
	virtual string ProtocolPostProcessing2(string command,string param);
#endif
};




#endif

