#include "StdAfx.h"
#include "ReceiveQueue.h"

Packet::Packet()
{

		m_iFrameType=FT_UNKNOWN;
		m_pData=NULL;
		m_iDataSize=0;
}

Packet::Packet( BYTE* pData, int nSize)
{
	m_iDataSize=nSize;
	m_pData = new BYTE[nSize + 1];	
	m_pData[nSize] = '\0';
	memcpy(m_pData, pData, m_iDataSize);
}

Packet::~Packet(void)
{
	if(m_pData)
	{
		delete[] m_pData;
		m_pData = NULL;
	}

}
Packet* Packet::CreatePacket( BYTE* pData, int nSize)
{
	return new Packet(pData, nSize);
}

Packet& Packet::operator=( const Packet& packet )
{
	if (this == &packet )
		return *this;

	m_iDataSize = packet.m_iDataSize;
	memcpy(m_pData, packet.m_pData, m_iDataSize);

	return *this;
}

////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

CReceiveQueue::CReceiveQueue()
{
	InitializeCriticalSection(&m_critSec);
}
CReceiveQueue::~CReceiveQueue()
{		
	Clear();
	DeleteCriticalSection(&m_critSec);
}

void CReceiveQueue::AddPacket(unsigned char* data, unsigned dataSize)
{

	Packet* pPacket = Packet::CreatePacket(data, dataSize);


	EnterCriticalSection(&m_critSec);
	pPacket->m_iRefCnt++;
	m_qPacket.push_back(pPacket);
	LeaveCriticalSection(&m_critSec);
}
void CReceiveQueue::AddPacket(Packet* pPacket)
{

	EnterCriticalSection(&m_critSec);
	pPacket->m_iRefCnt++;
	m_qPacket.push_back(pPacket);
	LeaveCriticalSection(&m_critSec);
}

Packet* CReceiveQueue::GetPacket()
{
	Packet* pPacket = NULL;

	if (!m_qPacket.empty())
	{
		EnterCriticalSection(&m_critSec);
		pPacket = m_qPacket.front();
		m_qPacket.pop_front();
		LeaveCriticalSection(&m_critSec);
	}
	return pPacket;
}
bool CReceiveQueue::HasPackets()
{
	EnterCriticalSection(&m_critSec);

	return !m_qPacket.empty();

	LeaveCriticalSection(&m_critSec);
}

int CReceiveQueue::Size()
{
	return m_qPacket.size();
}

void CReceiveQueue::Free(Packet* pPacket)
{
	if(pPacket->m_iRefCnt>0)
		pPacket->m_iRefCnt--;
	if(pPacket->m_iRefCnt==0)
	{
		if(pPacket)
		{
			delete pPacket;
			pPacket=NULL;
		}
	}
}

void CReceiveQueue::Clear()
{
	EnterCriticalSection(&m_critSec);
	while ( !m_qPacket.empty() )
	{
		Packet* pPacket = m_qPacket.front();
		delete pPacket;
		pPacket=NULL;
		m_qPacket.pop_front();
	}
	LeaveCriticalSection(&m_critSec);
}
