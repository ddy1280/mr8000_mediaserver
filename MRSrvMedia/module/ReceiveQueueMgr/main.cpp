// ReceiveQueueMgr.cpp : 定義主控台應用程式的進入點。
//

#include "stdafx.h"
#include "ReceiveQueueMgr.h"
#include <process.h>

#include <iomanip>      // std::setfill, std::setw
#include <iostream>
#include <sstream>
#include "Algorithm\md5.h"
#pragma comment (lib, "Algorithm.lib")
using namespace HS;

typedef struct
{
	HANDLE hThread;
	HANDLE hMsg;
	unsigned int aThreadID;
	void *Parent;
	bool exitflag;
	int toggleCount;
} ThreadInfo;
unsigned char tmpdata[10];

CReceiveQueueMgr *qMgr;

std::string GetStreamHashIndex(int SessionID)
{
	std::stringstream hashSrc;
	hashSrc << "000f3a026333" << "LIVE" << 1 << 1388538061; 

	std::string hashIdx = Algorithm::md5(hashSrc.str());
	std::stringstream ret;
	ret << std::setfill ('0') << std::setw(8) << std::uppercase  << std::hex << SessionID;
	hashIdx = ret.str()+hashIdx;
	return hashIdx;	
}

static UINT WINAPI Thread1(LPVOID pvParam)
{
	int i=0,j=0;

	for(i=0;i<10;i++)
	{
		for(j=0;j<10;j++)
			tmpdata[j]='a'+i;
#if 1
		Packet *pPacket=new Packet();
		pPacket->m_pData=new BYTE[10+1];
		pPacket->m_pData[10] = '\0';
		memcpy(pPacket->m_pData, tmpdata, 10);
		pPacket->m_iDataSize=10;
		pPacket->m_iStreamType=ST_VIDEO;
		pPacket->m_iFrameType=FT_FRAME_I;


		qMgr->GetPacketQueue(GetStreamHashIndex(1))->AddPacket(pPacket);
#else
		qMgr->AddPacket(hashIdx,tmpdata,10);
#endif
	}

	return 0;
}
static UINT WINAPI Thread2(LPVOID pvParam)
{
	while(1)
	{	
		CReceiveQueue *pQueue=qMgr->GetPacketQueue(GetStreamHashIndex(1));
		if(pQueue->HasPackets())
		{
			Packet *pPacket=pQueue->GetPacket();
			if(pPacket!=NULL)
			{
				unsigned char *myData=pPacket->m_pData;
#if 1
				pQueue->Free(pPacket);
#else
				delete pPacket;
				pPacket=NULL;
#endif			

			}
		}
			Sleep(10);
	}

	return 0;
}

int main(int argc, _TCHAR* argv[])
{

	qMgr=new CReceiveQueueMgr();

	//hashIdx=hashGen.Generate("00:0f:3a:02:63:33","1");

	qMgr->InitPacketQueue(GetStreamHashIndex(1));
	qMgr->InitPacketQueue(GetStreamHashIndex(2));

	ThreadInfo *threadInfo=new ThreadInfo;
	threadInfo->exitflag=false;
	threadInfo->toggleCount = 0;

	threadInfo->hThread= (HANDLE) _beginthreadex(0,0,Thread1,(LPVOID)threadInfo,0,  (unsigned *) &threadInfo->aThreadID);
	Sleep(100);
	ThreadInfo *threadInfo2=new ThreadInfo;
	threadInfo2->exitflag=false;
	threadInfo2->toggleCount = 0;

	threadInfo2->hThread= (HANDLE) _beginthreadex(0,0,Thread2,(LPVOID)threadInfo2,0,  (unsigned *) &threadInfo2->aThreadID);




	//使程式不會結束
	while(true);
		return 0;
}

