#include "stdafx.h"
#include "md5.h"
#include "HashGenerator.h"
#include <iostream> 
#include <sstream>
using namespace std;

CHashGenerator::CHashGenerator()
{}
CHashGenerator::~CHashGenerator()
{}

std::string CHashGenerator::Generate(std::string RID, std::string CHID)
{
    return "";
}
std::string CHashGenerator::Generate(std::string RID, int CH,std::string CMD,unsigned int Timestamp)
{
	std::stringstream ch,timestamp;
	ch << CH;
	timestamp<<Timestamp;
	std::string strCH=ch.str();
	std::string strTime=timestamp.str();

	if(RID.size()==0)
	{
		std::string error="Error: Empty RID";
		return  error;
	}
	else if(strCH.size()==0)
	{
		std::string error="Error: Empty Channel ID";
		return  error;
	}
	else if(strTime.size()==0)
	{
		std::string error="Error: Empty Timestamp";
		return  error;
	}
	else if(CMD.size()==0)
	{
		std::string error="Error: Empty CMD";
		return  error;
	}
	else
	{
		RID.append(strCH);
		RID.append(CMD);
		RID.append(strTime);
		return MD5(RID).hexdigest();
	}
}