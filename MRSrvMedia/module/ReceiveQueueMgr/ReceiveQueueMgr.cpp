#include "StdAfx.h"
#include "ReceiveQueueMgr.h"
#include <assert.h>
#include <iomanip>      // std::setfill, std::setw

CReceiveQueueMgr::CReceiveQueueMgr(void)
{
	InitializeCriticalSection(&m_critSec);
}

CReceiveQueueMgr::~CReceiveQueueMgr(void)
{
	DeleteCriticalSection(&m_critSec);
}

CReceiveQueue* CReceiveQueueMgr::InitPacketQueue(string QueueSN)
{

	//不存在則新增
	CReceiveQueue *pRcvQueue;

	EnterCriticalSection(&m_critSec);
	string hashIdx="";
	string sessionID="";

	if(QueueSN.length()==40)
	{
		sessionID=QueueSN.substr(0,8);
		hashIdx=QueueSN.substr(8,32);
	}
	else
		return NULL;

	bool isExist=false;
	multimap_pair_t its;
	its = m_mapQueues.equal_range(hashIdx);
	if(m_mapQueues.count(hashIdx))
	{
		multimap_iterator_t it ;

		for (it=its.first;it != its.second;	++it)
		{
			if(it->second->ID==sessionID)
			{
					pRcvQueue=it->second;
					isExist=true;
			}
		}
	}
	else
	{
		isExist=false;
	}
	if(!isExist)
	{
			pRcvQueue = new CReceiveQueue();
			pRcvQueue->ID=sessionID;
			m_mapQueues.insert(pair<std::string, CReceiveQueue*>(hashIdx, pRcvQueue));			
	}

	LeaveCriticalSection(&m_critSec);
	return pRcvQueue;

}

CReceiveQueue* CReceiveQueueMgr::GetPacketQueue(string HashIdx,int Index)
{
	multimap_pair_t it;
	int i=0;

	it = m_mapQueues.equal_range(HashIdx);
	multimap_iterator_t it2 ;

	for (it2=it.first;it2 != it.second;	++it2)
	{
		if(i==Index)
			return it2->second;
		i++;
	}
}
CReceiveQueue* CReceiveQueueMgr::GetPacketQueue(string QueueSN)
{
	string hashIdx="";
	string sessionID="";

	if(QueueSN.length()==40)
	{
			sessionID=QueueSN.substr(0,8);
			hashIdx=QueueSN.substr(8,32);
	}
	else
		return NULL;

	multimap_pair_t it;
	it = m_mapQueues.equal_range(hashIdx);

	multimap_iterator_t it2 ;

	for (it2=it.first;it2 != it.second;	++it2)
	{
			if(it2->second->ID==sessionID)
				return it2->second;
	}

}
int CReceiveQueueMgr::Count(string HashIdx)
{
		int cnt=0;
		EnterCriticalSection(&m_critSec);
		cnt=m_mapQueues.count(HashIdx);
		LeaveCriticalSection(&m_critSec);

		return cnt;		
}
void CReceiveQueueMgr::ClearAll()
{
	EnterCriticalSection(&m_critSec);

	for (multimap_iterator_t it = m_mapQueues.begin(); it != m_mapQueues.end(); ++it)
	{
		it->second->Clear();
	}
	LeaveCriticalSection(&m_critSec);
}

void CReceiveQueueMgr::Delete(string QueueSN)
{
	EnterCriticalSection(&m_critSec);
	string hashIdx="";
	string sessionID="";

	if(QueueSN.length()==40)
	{
		sessionID=QueueSN.substr(0,8);
		hashIdx=QueueSN.substr(8,32);
	}
	else
		return;

	multimap_pair_t its;
	its = m_mapQueues.equal_range(hashIdx);

	multimap_iterator_t it;
	for (it = its.first;it != its.second;	)
	{
		if(it->second->ID==sessionID)
		{
			it->second->Clear();
			delete it->second;
			it->second=NULL;
			it=m_mapQueues.erase(it);
		}
		else
		{
			++it;
		}
	}
	LeaveCriticalSection(&m_critSec);
}

void CReceiveQueueMgr::DeleteALL()
{
	EnterCriticalSection(&m_critSec);
	for (multimap_iterator_t it = m_mapQueues.begin(); it != m_mapQueues.end(); ++it)
	{
		delete it->second;
		it->second=NULL;
	}
	m_mapQueues.clear();
	LeaveCriticalSection(&m_critSec);
}

