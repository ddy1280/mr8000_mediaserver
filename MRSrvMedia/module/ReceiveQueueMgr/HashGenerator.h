
#ifndef __CHASHGENERATOR__
#define __CHASHGENERATOR__


class CHashGenerator
{
public:
	CHashGenerator();
	~CHashGenerator();
	
	std::string Generate(std::string RID, std::string CHID);
	std::string Generate(std::string RID, int CH,std::string CMD,unsigned int Timestamp);

};

#endif