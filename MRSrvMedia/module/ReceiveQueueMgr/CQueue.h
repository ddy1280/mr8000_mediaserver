#ifndef CQUEUE
#define CQUEUE
#include <windows.h>
#include <queue>
#include <typeinfo>
//Thread-safe Queue

template <class T>
class CQueue
{
    private:
        std::queue <T> q;
        bool isPointer;
        CRITICAL_SECTION m_cs;
        void Lock()
        {
            EnterCriticalSection(&m_cs);
        }

        void Unlock()
        {
            LeaveCriticalSection(&m_cs);
        }
    public:
        CQueue()
        {
            if (strstr(typeid(q).name(),"*")>0)  //判斷是否為指標型態
                isPointer=true;
            else
                isPointer=false;

            InitializeCriticalSection(&m_cs);
        }
        ~CQueue()
        {
            DeleteCriticalSection(&m_cs);
        }
        int Put (T value)
        {
           Lock();
           q.push(value);
           Unlock();
           return 0;
        };
        T Get()
        {
            if (q.empty()) return NULL;
            Lock();
            T ret= q.front();
            q.pop();
            Unlock();
            return ret;
        };
        void Empty()
        {
            Lock();
            q.empty(); 
            Unlock();
        }
        size_t Size()
        {
           return q.size();
        };
        virtual void Clear()
        {
            while(!q.empty())
            {
                T tmp= Get();
                if (isPointer)
                {
                    Lock();
                   if (FreeFunction)
                      FreeFunction(tmp);
                   delete (void*)tmp;
                    Unlock();
                }
            }
            Empty();
        };
        void (*FreeFunction)(T Node);
} ;



/**** sample code  ****
    CQueue <DDD*> a;

    DDD *av= new DDD;
    av->x=3;
    a.Put(av);

    DDD *rr = a.Get();
    Caption = String(rr->x);
    delete rr;
*/

#endif
