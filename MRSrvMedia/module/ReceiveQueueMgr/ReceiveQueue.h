#pragma once

#include <Windows.h>
#include <deque>

class Packet;

typedef unsigned char BYTE;

typedef enum STREAM_TYPE
{
	ST_UNKNOWN = -1,
	ST_VIDEO =0,
	ST_AUDIO,
	ST_TEXT,
}STREAM_TYPE;

typedef enum STREAM_MODE
{
	SM_UNKNOWN = -1,
	SM_LIVE =0,
	SM_PLAYBACK,
}STREAM_MODE;

typedef enum FRAME_TYPE
{
	FT_UNKNOWN = -1,
	FT_FRAME_I =0,
	FT_FRAME_P,
	FT_BITMAP,
	FT_JPEG,
}FRAME_TYPE;

typedef std::deque<Packet*> PacketQueue;

class Packet
{
public:
	Packet();
	virtual ~Packet(void);
	static Packet* CreatePacket(BYTE* pData, int nSize);
	int m_iDataSize;
	int m_iStreamType;  //0:  1:  2:
	int m_iFrameType;
	int m_miStream;
	int m_iStreamMode;
	BYTE* m_pData;


	friend class CReceiveQueue;

	

	Packet& operator=(const Packet& packet);

private:
	
	Packet(BYTE* pData, int nSize);
	int m_iRefCnt;
	
	
};

class CReceiveQueue
{

public:
	CReceiveQueue();
	~CReceiveQueue();

	void AddPacket(unsigned char* data, unsigned dataSize);
	void AddPacket(Packet* pPacket);
	Packet* GetPacket();
	bool HasPackets();
	void Free(Packet* pPacket);
	void Clear();
	int Size();
	std::string ID;

private:

	PacketQueue m_qPacket;
  CRITICAL_SECTION m_critSec;
	CReceiveQueue(const CReceiveQueue&);
	CReceiveQueue operator=(const CReceiveQueue&);

};
