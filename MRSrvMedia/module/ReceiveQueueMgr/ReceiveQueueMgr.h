#pragma once

#include "ReceiveQueue.h"
#include <string>
#include <map>
#include <Windows.h>


class CReceiverQueue;
using namespace std;

typedef pair<multimap<std::string, CReceiveQueue*>::iterator, multimap<std::string, CReceiveQueue*>::iterator> multimap_pair_t;  
typedef multimap<std::string, CReceiveQueue*>::iterator  multimap_iterator_t; 
typedef std::multimap<std::string, CReceiveQueue*> MapQueue;


class CReceiveQueueMgr
{
	//friend class ReceiverQueue;

public:

	CReceiveQueueMgr(void);
	~CReceiveQueueMgr(void);

	CReceiveQueue* InitPacketQueue(string QueueSN);
	CReceiveQueue* GetPacketQueue(string QueueSN);
	CReceiveQueue* GetPacketQueue(string HashIdx,int Index);
	//void AddPacket(string hashIdxStr, unsigned char* data, unsigned dataSize);
	//void AddPacket(string hashIdxStr,Packet *pPacket);
	//Packet* GetPacket( string hashIdxStr);

	//bool HasPackets(string hashIdxStr);
	//void Free(string hashIdxStr,Packet* pPacket);
	//int  Size(string hashIdxStr);
	//void Clear(string hashIdxStr);
	void ClearAll();
	void Delete(string QueueSN);
	void DeleteALL();
	int Count(string HashIdx);

private:
	MapQueue m_mapQueues;
	CRITICAL_SECTION m_critSec;
};
