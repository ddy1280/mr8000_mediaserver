#pragma once
#include <windows.h>
#include <stdio.h>
#include <iomanip>
#include <iostream>
#include <algorithm>
#include <string>
#include <sstream>
#include <cstdlib>
#include <direct.h>


#ifdef _CONVERT_WITH_ATL
	#include <atlconv.h>
	#include <strsafe.h>

	#ifdef _DEBUG
		# pragma comment(lib, "atls.lib")
	#else
		# pragma comment(lib, "atls.lib")
	#endif
#endif


class StringUtil
{
public:


#ifdef _CONVERT_WITH_ATL
  static std::string wideToStl(const WCHAR* wszString)
  {
    //return "";
    USES_CONVERSION;
    char *szTemp = W2A(wszString);
    return std::string(szTemp);
  }

  static WCHAR* stlToWide(std::string sString)
  {
    return(asciiToWide(sString.c_str()));
  }

  /// The caller must free the memory for the result
  static WCHAR* asciiToWide(const char* szString)
  {
    size_t nLen = strlen(szString);
    WCHAR* wszBuffer = new WCHAR[nLen + 1];
    USES_CONVERSION;
    WCHAR* wszValue = A2W(szString);
    StringCchCopyW(wszBuffer, nLen + 1, wszValue);
    return wszBuffer;
  }
#else
  static std::string WideToStl(const wchar_t* wszString)
  {
    const unsigned max_len = 255;
    char szBuffer[max_len];
    size_t uiConverted(0);
    errno_t err = wcstombs_s(&uiConverted, szBuffer, max_len, wszString, _TRUNCATE);
    return (err == 0) ? std::string(szBuffer, uiConverted - 1) : ""; // subtract 1 for null terminator
  }

  static wchar_t* StlToWide(std::string sString)
  {
    return(AsciiToWide(sString.c_str()));
  }

  /// The caller must free the memory for the result
  static wchar_t* AsciiToWide(const char* szString)
  {
    size_t uiLen = strlen(szString);
		size_t uiRes;
		errno_t ret;
    wchar_t* wszBuffer = new wchar_t[uiLen+100];
// 
// Zero if successful, an error code on failure.
// EINVAL: wcstr is NULL and sizeInWords > 0	
// EINVAL: mbstr is NULL			
// ERANGE: The destination buffer is too small to contain the converted string (unless count is _TRUNCATE; see Remarks below)
// 			
		ret =mbstowcs_s(&uiRes,wszBuffer,uiLen+100,szString, strlen(szString));
		//size_t uiRes = mbstowcs(wszBuffer, szString, uiLen);
		int wsize=wcslen(wszBuffer);
    if (ret == 0)
    {
      wszBuffer[uiRes] = L'\0';
      return wszBuffer;
    }
    else
    {
      delete[] wszBuffer;
      return NULL;
    }
  }
#endif

  // Converts "1", "true", "TRUE" to true and "0", "FALSE" and "false" to false
  static bool StringToBool(const std::string& sValue)
  {
    if (sValue == "1" || sValue == "TRUE" || sValue == "true")
    {
      return true;
    }
    else if (sValue == "0" || sValue == "FALSE" || sValue == "false")
    {
      return false;
    }
    else
    {
      // Default
      return true;
    }
  }

  static std::string BoolToString(bool bValue)
  {
    std::string sResult = bValue?"1":"0";
    return sResult;
  }

  static std::string DoubleToString(double d, int nPrecision = 2)
  {
    std::ostringstream oss;

    oss << std::setprecision(nPrecision)
      << std::fixed
      << d;

    return oss.str();
  }

  static double StringToDouble(std::string sDouble)
  {
    std::istringstream i(sDouble);
    double x;
    if (!(i >> x))
    {
      return 0.0;
    }
    return x;
  }

	/// The caller must free the memory for the result
	static char* StringToChar(std::string Str)
	{
		char * szRet = new char[Str.size() + 1];
		std::copy(Str.begin(), Str.end(), szRet);
		szRet[Str.size()] = '\0';

		return szRet;
	}
	/// The caller must free the memory for the result
	static wchar_t* StringToChar(std::wstring Str)
	{
		wchar_t * szRet = new wchar_t[Str.size() + 1];
		std::copy(Str.begin(), Str.end(), szRet);
		szRet[Str.size()] = '\0';

		return szRet;
	}

	static std::wstring AnsiStr2WideStr(std::string szSrc)
	{
		char * szData = StringToChar(szSrc);

		int len = MultiByteToWideChar(CP_UTF8,0,szData,strlen(szData),NULL,0);  
		wchar_t *m_wchar=new wchar_t[len+1];  
		MultiByteToWideChar(CP_UTF8,0,szData,strlen(szData),m_wchar,len);  
		m_wchar[len]='\0';
		std::wstring strRet=std::wstring(m_wchar);	
		delete[] m_wchar;
		delete szData;

		return strRet;
	}

	static std::string WideStr2AnsiStr(std::wstring szSrc)
	{
		wchar_t * szData = StringToChar(szSrc);

		int nIndex = WideCharToMultiByte(CP_UTF8, 0, szData, -1, NULL, 0, NULL, NULL);
		char *pAnsi = new char[nIndex + 1];
		WideCharToMultiByte(CP_UTF8, 0, szData, -1, pAnsi, nIndex, NULL, NULL);
		std::string strRet=std::string(pAnsi);
		delete[] pAnsi;
		delete szData;

		return strRet;
	}
	static void str_sprintf(std::string& retStr,const char *fmt, ...) 
	{
		va_list args;
		va_start(args, fmt);

		char buf[1024];
		vsnprintf(buf,1024,fmt,args);
		va_end(args);

		retStr = std::string(buf);

	}
	static std::string StringReplaceA(const std::string &src,const std::string &oldstr,const std::string &newstr)
{
	/*
	size_t pos = 0;
	std::string buffer = src;
	std::string::size_type newlen = newstr.length();
	std::string::size_type oldlen = oldstr.length();
	while(true)
	{
		pos = buffer.find(oldstr, pos);
		if (pos == std::string::npos) break;
		buffer.replace(pos, oldlen, newstr);
		pos += newlen;
	}
	return(buffer);
*/
	std::string subject =src;
	size_t pos = 0;
	while ((pos = subject.find(oldstr, pos)) != std::string::npos)
	{
		std::string s1,s2;
		s1 = subject.substr(0,pos);
		s2 = subject.substr(pos+oldstr.length());

		subject = s1 + newstr + s2;

		pos += newstr.length();
	}
	return subject;
}
static std::wstring StringReplaceW(const std::wstring &src,const std::wstring &oldstr,const std::wstring &newstr)
{
	std::wstring subject =src;
	size_t pos = 0;
	while ((pos = subject.find(oldstr, pos)) != std::wstring::npos)
	{
		std::wstring s1,s2;
		s1 = subject.substr(0,pos);
		s2 = subject.substr(pos+oldstr.length());

		subject = s1 + newstr + s2;

		pos += newstr.length();
	}
	return subject;
}

static std::string IntToString(int value) {std::stringstream ss;ss << value;return ss.str();}

static std::wstring IntToWString(int value) {
	std::wostringstream ss;
	ss << value;
	return ss.str();
}
static std::string FloatToString(int value) {std::stringstream ss;ss << value;return ss.str();}
static std::wstring StringToWstring(std::string Value)
{
	std::wstring wsValue(Value.begin(),Value.end());
	return wsValue;
}
static std::string StringToUpper(std::string strToConvert)
{

	std::transform(strToConvert.begin(), strToConvert.end(), strToConvert.begin(), ::toupper);

	return strToConvert;
}
/*
	static string trimLeft(const string& str) {
		string t = str;
		for (string::iterator i = t.begin(); i != t.end(); i++) 
		{
			if (!isspace(*i)) {
				t.erase(t.begin(), i);
				break;
			}
		}
		return t;
	}

	static string trimRight(const string& str) {
		if (str.begin() == str.end()) {
			return str;
		}

		string t = str;
		for (string::iterator i = t.end() - 1; i != t.begin(); i--) {
			if (!isspace(*i)) {
				t.erase(i + 1, t.end());
				break;
			}
		}
		return t;
	}

	static string trim(const string& str) {
		string t = str;

		string::iterator i;
		for (i = t.begin(); i != t.end(); i++) {
			if (!isspace(*i)) {
				t.erase(t.begin(), i);
				break;
			}
		}

		if (i == t.end()) {
			return t;
		}

		for (i = t.end() - 1; i != t.begin(); i--) {
			if (!isspace(*i)) {
				t.erase(i + 1, t.end());
				break;
			}
		}

		return t;
	}*/


};
