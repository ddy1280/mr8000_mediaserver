#include "Protocol.h"
#include "Protocol_Media.h"
#include "Global.h"
#include "String/StringUtility.h"
#include <fstream>
#include <sstream>
#include <boost/filesystem.hpp>
#include <stdio.h>
#include <stdlib.h>  
#include <iostream>
#include <map>
#include <ctime>
#include "time.h"
#include <string> 
#include "tinyxml.h"  
#include "tinystr.h"
#include <io.h>
#include <fcntl.h>
#include "HS_Utility.h"
#include <cstdlib>
#include <process.h>
#include "Algorithm\md5.h"
using namespace HS;

using namespace std;
//#include "CConsoleDebug.h"
//#include <mallco.h>

#pragma comment (lib,"HTTPServerEx.lib")
#pragma comment (lib,"HttpClient.lib")
#pragma comment (lib,"sqlitepp.lib")
#pragma comment (lib,"String.lib")
#pragma comment (lib, "Algorithm.lib")

//===============================================================================
//Protocol Name: Log Protocol
//
//===============================================================================
#ifdef _WRITERAW
	int g_iFd=-1;
	unsigned int fileCnt[8];
	bool waitTheI=false;
	FILE *fpStartCode=NULL;
#endif
int OpenFileWrite(const wchar_t *sFname)
{
	int fHandle;
#if 1
	fHandle = _wopen(sFname, O_RDWR | O_CREAT | O_BINARY,0666);
#else
	fHandle = _wopen(sFname, O_RDWR | O_CREAT | O_TEXT,0666);
#endif
	return fHandle;
}

int WriteToFile(int Handle, const char *buf, int iSize)
{
	int isz;		
	isz = _write(Handle, buf, iSize);

	return isz;
}

using namespace HS::String;

unsigned long long g_currentTime[8];


UINT WINAPI CMediaProtocol::HttpWatchFunc(void* param)
{
	//std::cerr << "HttpWatchFunc thread start \r\n";
	//清除久未回應的DVR's ability
	SThreadInfo *threadInfo = (SThreadInfo*) param;
	CMediaProtocol *obj = (CMediaProtocol*) threadInfo->Parent;

	while(!threadInfo->exitflag)
	{
		unsigned int currentTime=time(0);
		std::map <std::string, S_StreamAbility*>::iterator it= obj->m_mapStreamAbility.begin();
		for (;it != obj->m_mapStreamAbility.end();)  
		{
			S_StreamAbility *pstreamAbility=(S_StreamAbility*)it->second;
			//兩個unsigned 相減可能會得到一個極大值
			if(currentTime > pstreamAbility->uiLastRequestTime+15)
			{

				delete pstreamAbility;
				pstreamAbility=NULL;
				it=obj->m_mapStreamAbility.erase(it);

			}
			else
				it++;		
		}	

		//須監控所有的share memory 更新時間
		



		Sleep(1000);
	}
	//std::cerr << "HttpWatchFunc thread end \r\n ";
	return 0;
}
CMediaProtocol::CMediaProtocol()
{
	m_Interval = 60;
	m_bIsSendRequest=false;

	m_strSavePath=L"D:\\tmp";
	m_iStreamSource=HTTP;

	m_sThreadInfo.exitflag = false;
	m_sThreadInfo.Parent = this;
	m_sThreadInfo.toggleCount = 0;
	m_sThreadInfo.hThreadListen = (HANDLE) _beginthreadex(NULL, 0, CMediaProtocol::HttpWatchFunc,(LPVOID) &m_sThreadInfo,0,&m_sThreadInfo.hListenThreadID);
	InitializeCriticalSection(&m_critSec);

}
CMediaProtocol::~CMediaProtocol()
{

	m_sThreadInfo.exitflag=true;
	DeleteCriticalSection(&m_critSec);
}		
void CMediaProtocol::InitialShmMgr(ShmMgr *pShmMgr)
{
	m_shmMgr=pShmMgr;
}
void CMediaProtocol::ClearAllQueue(string RID)
{
	/*map <string, SRequestInfo*>::iterator pos;
	for (pos = m_mapRequestInfo.begin(); pos != m_mapRequestInfo.end(); pos++)  
	{
		SRequestInfo *pReq=(SRequestInfo*)pos->second;
		if(pReq->strRID==RID)
		{
				string hashIdx=hashGen.Generate(pReq->strRID,pReq->nCH,pReq->strCmd,pReq->uiTime);
				m_rcvQueueMgr->GetPacketQueue(hashIdx)->Clear();
		}
	}	*/
}

int CMediaProtocol::GetConnectStatus(SOCKET s)
{
	return -1;
}

std::string CMediaProtocol::ProtocolImplement(const request& req, reply& rep)
{
	std::string repStr;
	std::string command = req.cmd;
	std::string param = req.post_data;
	mMethod = req.method;

	if (ToUpper(req.method)=="GET" && ToUpper(param).find("ACTION=")!=std::string::npos)
	{
		std::map <std::string, std::string> contentMap = ParameterToKeyPair(param);
		command = contentMap["ACTION"];
		param = param.substr(param.find("&")+1);
		
	}
	else if (ToUpper(req.method)=="POST" && ToUpper(param).find("ACTION=")!=std::string::npos)
	{
	
		param = ReplaceString(param,"&","\r\n");
		std::map <std::string, std::string> contentMap = ParameterToKeyPair(param);
		command = contentMap["ACTION"];
		param = param.substr(param.find("&")+1);
		
	}

	std::transform(command.begin(), command.end(),command.begin(), ::toupper);

	//分析Header
	std::string contenttype;

	for (int i=0;i<req.headers.size();i++)
	{
		if (req.headers[i].name== "Content-Type")
		{
			contenttype = req.headers[i].value;
		}
	}
	repStr = InnerMediaServerProtocol(UNICODE_DATA_NORMAL, command,param/*,req.hSocket*/);

	return repStr;
}

std::string CMediaProtocol::ProtocolPostProcessing(std::string command,std::string param)
{
	std::transform(command.begin(), command.end(),command.begin(), ::toupper);
	return param;
}
bool CMediaProtocol::to_bool(std::string str) {
    std::transform(str.begin(), str.end(), str.begin(), ::tolower);
    std::istringstream is(str);
    bool b;
    is >> std::boolalpha >> b;
    return b;
}
//RID=3a:23:e9:6c:38:39&CMD=LIVE&STREAM=1&CH=1&TIME=1404727200

std::string CMediaProtocol::GetStreamSN(string RID,string CMD,int StreamID,int CH,unsigned int UTC)
{
	std::stringstream hashSrc;
	hashSrc << RID << StreamID << CMD << CH << UTC; 
	std::string hashIdx = Algorithm::md5(hashSrc.str());

	return hashIdx;	
}
std::string CMediaProtocol::GetClientSN(string RID,string CMD,int StreamID,int CH,unsigned int UTC,int SessionID)
{
	std::stringstream hashSrc;
	hashSrc << RID << StreamID <<CMD << CH << UTC; 

	std::string hashIdx = Algorithm::md5(hashSrc.str());
	std::stringstream ret;
	ret << std::setfill ('0') << std::setw(8) << std::uppercase  << std::hex << SessionID;
	hashIdx = ret.str()+hashIdx;
	return hashIdx;	
}
void CMediaProtocol::DelRequestInfoFromTable(string HashIdx)
{
	EnterCriticalSection(&m_critSec);

	map <string, SRequestInfo*>::iterator it = m_mapRequestInfo.find(HashIdx);
	S_RequestInfo *sRequestInfo;
	if (it != m_mapRequestInfo.end())
	{
		sRequestInfo=it->second;
		if(sRequestInfo)
		{
			delete sRequestInfo;
			sRequestInfo=NULL;
		}
		m_mapRequestInfo.erase(HashIdx);
	}

	LeaveCriticalSection(&m_critSec);
}
bool CMediaProtocol::IsRequestExist(string QueueSN)
{
	EnterCriticalSection(&m_critSec);
	bool isExit=false;
	std::deque<S_RequestInfo*>::iterator it;  

	for (it = m_qRequest.begin(); it != m_qRequest.end();it++)  
	{
		S_RequestInfo *tmpReqInfo=(*it);
		if(tmpReqInfo->strRequestSN==QueueSN)
		{
			isExit = true;
		}								
	}		
	isExit = false;
	LeaveCriticalSection(&m_critSec);

	return isExit;
}
int CMediaProtocol::AddRequest(string RID,string StreamType,int StreamNum,int CH,unsigned int UTC,int SessionID)
{

	string clientSN=GetClientSN(RID,StreamType,StreamNum,CH,UTC,SessionID);
	string streamSN=GetStreamSN(RID,StreamType,StreamNum,CH,UTC);

	EnterCriticalSection(&m_critSec);

	map <string, SRequestInfo*>::iterator it = m_mapRequestInfo.find(streamSN);
	S_RequestInfo *sRequestInfo;

	if (it != m_mapRequestInfo.end())
	{	
		sRequestInfo=(S_RequestInfo *)it->second;
		sRequestInfo->strRID=RID;
		sRequestInfo->strCmd=StreamType;
		sRequestInfo->strRequestSN=clientSN;
		sRequestInfo->strStreamID=streamSN;
		sRequestInfo->uiTime=UTC;
		sRequestInfo->nCH=CH;
		sRequestInfo->nStream=StreamNum;
	}
	else
	{
		//Request info initial
		sRequestInfo=new S_RequestInfo;
		sRequestInfo->strRID=RID;
		sRequestInfo->strCmd=StreamType;
		sRequestInfo->strRequestSN=clientSN;
		sRequestInfo->strStreamID=streamSN;
		sRequestInfo->uiTime=UTC;
		sRequestInfo->nCH=CH;
		sRequestInfo->nStream=StreamNum;
		sRequestInfo->iConnectionNum=0;
		sRequestInfo->iMaxConnection=0;
		sRequestInfo->bResponsed=false;

		m_mapRequestInfo.insert(std::make_pair(streamSN, sRequestInfo));

	}

	m_mapRequestTable.insert(std::make_pair(clientSN, CS_NONE));


	const std::wstring wsCH=StringUtil::IntToWString(sRequestInfo->nCH);
	std::wstring wsRID=StringUtil::StringToWstring(RID);
	wsRID=StringUtil::StringReplaceW(wsRID,L":",L"_");
	sRequestInfo->wsrtJpegPath=m_strSavePath+L"\\"+wsRID+L"\\CH"+wsCH;

	LeaveCriticalSection(&m_critSec);


	//放到queue裡等待回應DVR Request, 這裡帶進去的SN是unique的queue SN

	if(!IsRequestExist(clientSN))
	{
		S_RequestInfo *sClientRequest=new S_RequestInfo;
		//memcpy(sClientRequest,sRequestInfo,sizeof(S_RequestInfo));//用memcpy好像有問題，會有exception
		sClientRequest->strRequestSN=clientSN;
		sClientRequest->strRID=sRequestInfo->strRID;
		sClientRequest->nCH=sRequestInfo->nCH;
		sClientRequest->strCmd=sRequestInfo->strCmd;
		sClientRequest->nStream=sRequestInfo->nStream;
		sClientRequest->strStreamID=sRequestInfo->strStreamID;

		EnterCriticalSection(&m_critSec);
		m_qRequest.push_back(sClientRequest);
		LeaveCriticalSection(&m_critSec);
	}


	double oldTime,currTime;
	int wait=10;
	time_t start=clock();

	while(true)
	{
		time_t currTime=clock();
		if(((double)(currTime - start) / CLOCKS_PER_SEC)>wait)
		{
			//若找到share memory則認定沒有這個request，故將此request從table中移除
			if(m_shmMgr->GetShm(streamSN)==NULL)
				DelRequestInfoFromTable(streamSN);

			return CS_CONNECT_FAIL;
		}

		if(m_mapRequestTable[clientSN]!=CS_NONE)
		{
			if(m_mapRequestTable[clientSN]==CS_SUCCESS)
			{
				m_shmMgr->InitShm(clientSN);	
				m_shmMgr->ClientAttach(clientSN);

				return CS_SUCCESS;
			}
			else
			{
				if(m_shmMgr->GetShm(streamSN)==NULL)
				{
					DelRequestInfoFromTable(streamSN);
				}

				return CS_OVER_CONNECTION_LIMIT;
			}
			break;
		}

		Sleep(100);
	}

	//表示沒有任何client
	if(m_shmMgr->GetShm(streamSN)==NULL)
		DelRequestInfoFromTable(streamSN);


	return CS_CONNECT_FAIL;

}
/*
std::string CMediaProtocol::GetStreamTime(string RID,int CH,string CMD,unsigned int UTC)
{

	string hashIdx=GetHashIndex(RID,CH,CMD,UTC);

	map <string, SRequestInfo*>::iterator it = m_mapRequestInfo.find(hashIdx);
	S_RequestInfo *sRequestInfo;
	if (it != m_mapRequestInfo.end())
	{
		std::ostringstream time;
		time << (it->second->ullCurrentTime>>32
		return time.str();
	}
	return reply::replyString(reply::no_content);

}*/
//RID=3a:23:e9:6c:38:39&CMD=LIVE&STREAM=1&CH=1&TIME=1404826430
bool CMediaProtocol::ReleaseRequest(string RID,string StreamType,int StreamNum,int CH,unsigned int UTC,int SessionID)
{
	string clientSN=GetClientSN(RID,StreamType,StreamNum,CH,UTC,SessionID);	
	string streamSN=GetStreamSN(RID,StreamType,StreamNum,CH,UTC);

	map <string, SRequestInfo*>::iterator it = m_mapRequestInfo.find(streamSN);

	CShm *pshm=m_shmMgr->GetShm(streamSN);

	if (it != m_mapRequestInfo.end())
	{
		it->second->strCmd="TEARDOWN";
		
		if(pshm!=NULL)
		{
				int ret=m_shmMgr->ClientDetach(clientSN);
				if(ret==-1)
					return false;

				if(pshm->LibShm_GetClientCount()==0)
				{
					if(!IsRequestExist(clientSN))
					{
						S_RequestInfo *sClientRequest=new S_RequestInfo;
						sClientRequest->strRequestSN=clientSN;
						sClientRequest->strRID=RID;
						sClientRequest->nCH=it->second->nCH;
						sClientRequest->strCmd="TEARDOWN";
						sClientRequest->nStream=it->second->nStream;
						sClientRequest->strStreamID=it->second->strStreamID;

						EnterCriticalSection(&m_critSec);
						m_qRequest.push_back(sClientRequest);
						LeaveCriticalSection(&m_critSec);
						//delete share memory space
						m_shmMgr->Delete(streamSN);
#ifdef _WRITERAW
						waitTheI=false;
#endif
					}
				}
				return true;
		}		
	}
#ifdef _WRITERAW
	if (g_iFd)
	{
			close(g_iFd); 
			g_iFd=-1;
			fclose(fpStartCode);
	}		
#endif
 return false;
}
std::string CMediaProtocol::GetRequestStr(S_RequestInfo *pstRequest)
{
	/*MapRequestInfo::iterator it = m_mapRequestInfo.find(HashIdx); 

	if(it != m_mapRequestInfo.end())
	{
			S_RequestInfo* pstRequest=m_mapRequestInfo[HashIdx];
			*/
			std::stringstream ss; 
			ss <<"<Command>";
			//response cmd
			if(pstRequest->strCmd=="LIVE")
			{
				ss <<"<REQUESTSN>"<< pstRequest->strRequestSN<<"</REQUESTSN>"
					<<"<STREAMID>"<<  pstRequest->strStreamID<<"</STREAMID>"					
					<<"<CMD>"<<  pstRequest->strCmd<<"</CMD>"
					<<"<CH>"<<  pstRequest->nCH<<"</CH>"
					<<"<STREAM>"<< pstRequest->nStream<<"</STREAM>";
			}
			else if(pstRequest->strCmd=="PLAYBACK")
			{
				ss <<"<REQUESTSN>"<< pstRequest->strRequestSN<<"</REQUESTSN>"
					<<"<STREAMID>"<<  pstRequest->strStreamID<<"</STREAMID>"
					<<"<CMD>"<<  pstRequest->strCmd<<"</CMD>"
					<<"<CH>"<<  pstRequest->nCH<<"</CH>"
					<<"<STREAM>"<< pstRequest->nStream<<"</STREAM>"
					<<"<TIME>"<< pstRequest->uiTime<<"</TIME>";
			}
			else if(pstRequest->strCmd=="TEARDOWN")
			{
				ss <<"<REQUESTSN>"<< pstRequest->strRequestSN<<"</REQUESTSN>"
					<<"<STREAMID>"<<  pstRequest->strStreamID<<"</STREAMID>"
					<<"<CMD>"<<  pstRequest->strCmd<<"</CMD>"
					<<"<CH>"<<  pstRequest->nCH<<"</CH>"
					<<"<STREAM>"<< pstRequest->nStream<<"</STREAM>";
			}
			else if(pstRequest->strCmd=="SEARCH")
			{
				ss <<"<REQUESTSN>"<< pstRequest->strRequestSN<<"</REQUESTSN>"
					<<"<CMD>"<<  pstRequest->strCmd<<"</CMD>"
					<<"<CH>"<<  pstRequest->nCH<<"</CH>"
					<<"<TIME>"<< pstRequest->uiTime<<"</TIME>";
			}
			else if(pstRequest->strCmd=="SET")
			{
				ss <<"<REQUESTSN>"<< pstRequest->strRequestSN<<"</REQUESTSN>"
					<<"<CMD>"<<  pstRequest->strCmd<<"</CMD>"
					<<"<CH>"<<  pstRequest->nCH<<"</CH>"
					<<"<HS-Force>"<< pstRequest->unForceI<<"</HS-Force>"
					<<"<HS-Mute>"<< pstRequest->unMute<<"</HS-Mute>"
					<<"<HS-Delay>" << pstRequest->unDelay<<"</HS-Delay>";
			}
			else if(pstRequest->strCmd=="GET")
			{
				ss <<"<REQUESTSN>"<< pstRequest->strRequestSN<<"</REQUESTSN>"
					<<"<CMD>"<<  pstRequest->strCmd<<"</CMD>"
					<<"<GETPARAM>"<<pstRequest->strGetParam<<"</GETPARAM>"
					<<"<CH>"<<  pstRequest->nCH<<"</CH>";
			}
			ss <<"</Command>";
			return ss.str();
	/*}
	else
		return "";*/
}

S_StreamAbility* CMediaProtocol::ParseAbilityToMap(std::map<std::string,std::string> contentMap,std::string param)
{
	
	S_StreamAbility* pstAbility;
	std::map <std::string, S_StreamAbility*>::iterator itr = m_mapStreamAbility.find(contentMap["RID"]);
	//insert   
	if(itr == m_mapStreamAbility.end()){

			pstAbility = new S_StreamAbility();

			if (CheckKeyExist(param,"DVR_MODEL"))				pstAbility->strModel =  contentMap["DVR_MODAL"];
			if (CheckKeyExist(param,"RID"))							pstAbility->strRID =  contentMap["RID"];
			if (CheckKeyExist(param,"VERSION"))					pstAbility->strVersion =  contentMap["VERSION"];
			if (CheckKeyExist(param,"STREAM_NUM"))			pstAbility->nStreamNum = ToInt(contentMap["STREAM_NUM"]);
			if (CheckKeyExist(param,"STREAM_CH"))				pstAbility->nCHNum = ToInt(contentMap["STREAM_CH"]);
			if (CheckKeyExist(param,"STREAM_CONTENT"))	pstAbility->nStreamContent = ToInt(contentMap["STREAM_CONTENT"]);
			if (CheckKeyExist(param,"VIDEO_TYPE"))			pstAbility->nVideoType = ToInt(contentMap["VIDEO_TYPE"]);
			if (CheckKeyExist(param,"VIDEO_BITRATE"))		pstAbility->nVideoBitrate = ToInt(contentMap["VIDEO_BITRATE"]);
			if (CheckKeyExist(param,"VIDEO_FPS"))				pstAbility->nVideoFPS = ToInt(contentMap["VIDEO_FPS"]);
			if (CheckKeyExist(param,"VIDEP_RES"))				pstAbility->nVideoResolution = ToInt(contentMap[" VIDEP_RES"]);
			if (CheckKeyExist(param,"AUDIO_TYPE"))			pstAbility->nAudioType = ToInt(contentMap["AUDIO_TYPE"]);
			if (CheckKeyExist(param,"AUDIO_SAMPLING"))	pstAbility->unAudioSampleRate = ToInt(contentMap["AUDIO_SAMPLING"]);
			if (CheckKeyExist(param,"AUDIO_BITDEPTH"))	pstAbility->nAudioBitDepth = ToInt(contentMap["AUDIO_BITDEPTH"]);
			if (CheckKeyExist(param,"AUDIO_CHANNEL"))		pstAbility->nAudioChannelNum = ToInt(contentMap["AUDIO_CHANNEL"]);
			//watch dog ，用來檢查DVR是否沒有再送Request，若是則判定DVR斷線
			pstAbility->uiLastRequestTime=time(0);
			m_mapStreamAbility.insert(pair<std::string, S_StreamAbility*>(contentMap["RID"], pstAbility));
	}
	//update
	else	
	{
		pstAbility=itr->second;
		pstAbility->strModel = contentMap["DVR_MODEL"];
		pstAbility->strRID =  contentMap["RID"];
		pstAbility->strVersion =  contentMap["VERSION"];
		pstAbility->nStreamNum = ToInt(contentMap["STREAM_NUM"]);
		pstAbility->nCHNum = ToInt(contentMap["STREAM_CH"]);
		pstAbility->nStreamContent = ToInt(contentMap["STREAM_CONTENT"]);
		pstAbility->nVideoType = ToInt(contentMap["VIDEO_TYPE"]);
		pstAbility->nVideoBitrate = ToInt(contentMap["VIDEO_BITRATE"]);
		pstAbility->nVideoFPS = ToInt(contentMap["VIDEO_FPS"]);
		pstAbility->nVideoResolution = ToInt(contentMap[" VIDEP_RES"]);
		pstAbility->nAudioType = ToInt(contentMap["AUDIO_TYPE"]);
		pstAbility->unAudioSampleRate = ToInt(contentMap["AUDIO_SAMPLING"]);
		pstAbility->nAudioBitDepth = ToInt(contentMap["AUDIO_BITDEPTH"]);
		pstAbility->nAudioChannelNum = ToInt(contentMap["AUDIO_CHANNEL"]);
		//watch dog ，用來檢查DVR是否沒有再送Request，若是則判定DVR斷線
		pstAbility->uiLastRequestTime=time(0);
	}	
	return pstAbility;
}

void CMediaProtocol::ParseStreamData(S_StreamData* pstStreamData,std::string param)
{
		int i=0;

		memset(pstStreamData->strRID,'\0',34);
		memset(pstStreamData->strType,'\0',34);
		memset(pstStreamData->strVideoTitle,'\0',34);
		memset(pstStreamData->strCarID,'\0',18);
		memset(pstStreamData->strDeviceSN,'\0',18);
		memset(pstStreamData->StrCompany,'\0',34);
		memset(pstStreamData->strDriver,'\0',34);
		memset(pstStreamData->strRequestSN,'\0',42);
		memset(pstStreamData->strStreamID,'\0',34);

		map <string, string> contentMap = DataKeyPair(param);

		if (CheckKeyExist(param,"RID"))
			if(contentMap["RID"].length()>0)
				_snprintf(pstStreamData->strRID,sizeof(pstStreamData->strRID),"%s\0",contentMap["RID"].c_str());
		if (CheckKeyExist(param,"TYPE")) 
				_snprintf(pstStreamData->strType,sizeof(pstStreamData->strType),"%s\0",contentMap["TYPE"].c_str());
		if (CheckKeyExist(param,"VIDEO_TITLE")) 
				_snprintf(pstStreamData->strVideoTitle,sizeof(pstStreamData->strVideoTitle),"%s\0",contentMap["VIDEO_TITLE"].c_str());
		if (CheckKeyExist(param,"CARID")) 
			if(contentMap["CARID"].length()>0)
				_snprintf(pstStreamData->strCarID,sizeof(pstStreamData->strCarID),"%s\0",contentMap["CARID"].c_str());

		if (CheckKeyExist(param,"DEVICESN"))
			if(contentMap["DEVICESN"].length()>0)
				_snprintf(pstStreamData->strDeviceSN,sizeof(pstStreamData->strDeviceSN),"%s\0",contentMap["DEVICESN"].c_str());
		if (CheckKeyExist(param,"COMPANY")) 
			if(contentMap["COMPANY"].length()>0)
				_snprintf(pstStreamData->StrCompany,sizeof(pstStreamData->StrCompany),"%s\0",contentMap["COMPANY"].c_str());
		if (CheckKeyExist(param,"REQUESTSN")) 
			if(contentMap["REQUESTSN"].length()>0)
				_snprintf(pstStreamData->strRequestSN,sizeof(pstStreamData->strRequestSN),"%s\0",contentMap["REQUESTSN"].c_str());
		if (CheckKeyExist(param,"STREAMID")) 
			if(contentMap["STREAMID"].length()>0)
				_snprintf(pstStreamData->strStreamID,sizeof(pstStreamData->strStreamID),"%s\0",contentMap["STREAMID"].c_str());

		if (CheckKeyExist(param,"DRIVER")) 
			if(contentMap["DRIVER"].length()>0)
			{
				_snprintf(pstStreamData->strDriver,sizeof(pstStreamData->strDriver),"%s\0",contentMap["DRIVER"].c_str());
			}			

		if (CheckKeyExist(param,"UTC")) pstStreamData->ulUTC = _atoi64(contentMap["UTC"].c_str());// atol(contentMap["UTC"].c_str());
		if (CheckKeyExist(param,"PTS")) pstStreamData->uiPTS = atoi(contentMap["PTS"].c_str());
		if (CheckKeyExist(param,"TIMEZONE")) pstStreamData->uiTimezone = atoi(contentMap["TIMEZONE"].c_str());

		if (CheckKeyExist(param,"G_X")) pstStreamData->iG_X = ToFloat(contentMap["G_X"]);
		if (CheckKeyExist(param,"G_Y")) pstStreamData->iG_Y = ToFloat(contentMap["G_Y"]);
		if (CheckKeyExist(param,"G_Z")) pstStreamData->iG_Z = ToFloat(contentMap["G_Z"]);
		if (CheckKeyExist(param,"EVENT_VLOSS")) pstStreamData->unEventVloss = ToInt(contentMap["EVENT_VLOSS"]);
		if (CheckKeyExist(param,"EVENT_MOTION")) pstStreamData->unEventMotion = ToInt(contentMap["EVENT_MOTION"]);
		if (CheckKeyExist(param,"EVENT_ALARM")) pstStreamData->unEventAlarm = ToInt(contentMap["EVENT_ALARM"]);
		if (CheckKeyExist(param,"AUDIO_SAMPLING")) pstStreamData->unAudioSampleRate = ToInt(contentMap["AUDIO_SAMPLING"]);
		if (CheckKeyExist(param,"CHANNEL")) pstStreamData->nChannel = ToInt(contentMap["CHANNEL"]);
		if (CheckKeyExist(param,"VIDEO_BITRATE")) pstStreamData->nVideoBitrate = ToInt(contentMap["VIDEO_BITRATE"]);
		if (CheckKeyExist(param,"VIDEO_FPS")) pstStreamData->nVideoFPS = ToInt(contentMap["VIDEO_FPS"]);
		if (CheckKeyExist(param,"VIDEP_RES")) pstStreamData->nVideoResolution = ToInt(contentMap["VIDEP_RES"]);
		if (CheckKeyExist(param,"AUDIO_BITDEPTH")) pstStreamData->nAudioBitDepth = ToInt(contentMap["AUDIO_BITDEPTH"]);
		if (CheckKeyExist(param,"VIDEO_FRAME")) pstStreamData->unVideoFrame = ToInt(contentMap["VIDEO_FRAME"]);
		if (CheckKeyExist(param,"EVENT_PANICL")) pstStreamData->unEventPanic = ToInt(contentMap["EVENT_PANIC"]);
		if (CheckKeyExist(param,"GPS_LATITUDE")) pstStreamData->fGPSLatitude = ToFloat(contentMap["GPS_LATITUDE"]);
		if (CheckKeyExist(param,"GPS_LONGITUDE")) pstStreamData->fGPSLognitude = ToFloat(contentMap["GPS_LONGITUDE"]);
		if (CheckKeyExist(param,"GPS_ALTITUDE")) pstStreamData->fGPSAltitude = ToFloat(contentMap["GPS_ALTITUDE"]);
		if (CheckKeyExist(param,"GPS_SPEED")) pstStreamData->fGPSSpeed = ToFloat(contentMap["GPS_SPEED"]);
		if (CheckKeyExist(param,"GPS_FIX")) pstStreamData->unGPSFix = ToInt(contentMap["GPS_FIX"]);	
		if (CheckKeyExist(param,"GPS_HEADING")) pstStreamData->nGPSHeading = ToInt(contentMap["GPS_HEADING"]);
}

std::string CMediaProtocol::ProcessCmdResult(std::string param)
{

	const string strData = "<?xml version=\"1.0\" ?><REQUEST><LIVE><REQUESTSN>123456</REQUESTSN><RESULT>200</RESULT></LIVE></REQUEST>";
	TiXmlDocument *myDocument = new TiXmlDocument();
	myDocument->Parse((const char*)param.c_str(), 0, TIXML_ENCODING_UTF8);


	TiXmlElement *rootElement = myDocument -> RootElement();
	TiXmlElement *command = rootElement -> FirstChildElement();

	while (command)
	{
		TiXmlElement *paremeter= command -> FirstChildElement();
		
		string clientSN="";
		string strResult,strStreamSN;
		string strSN;
		int connNum=0,maxConn=0;

		while(paremeter)			
		{
			string strParam=StringUtil::StringToUpper(string(paremeter->Value()));

  		if(strParam=="REQUESTSN")
			{
				clientSN=paremeter->GetText();
				if(clientSN.length()==40)
				{
					strStreamSN=clientSN.substr(8,32);
				}				
			}				
			else if(strParam=="RESULT")
				strResult=paremeter->GetText();
			else if(strParam=="CONNNUM")
				connNum=atoi(paremeter->GetText());
			else if(strParam=="MAXCONN")
				maxConn=atoi(paremeter->GetText());

			paremeter= paremeter -> NextSiblingElement();
		}

		if(strResult=="200")
		{
			MapRequestInfo::iterator it = m_mapRequestInfo.find(strStreamSN); 

			if(it != m_mapRequestInfo.end())
			{
				S_RequestInfo* pstRequest=it->second;

				if(pstRequest!=NULL)
				{
					if(StringUtil::StringToUpper(pstRequest->strCmd) ==	"LIVE" || StringUtil::StringToUpper(pstRequest->strCmd) == "PLAYBACK")	
					{
						////由於DVR目前還沒確定好，暫時先寫死///
						pstRequest->iMaxConnection=8;
						pstRequest->iConnectionNum=1;
						///////////////////////////////////////////

						if(pstRequest->iConnectionNum<=pstRequest->iMaxConnection)
						{
							m_mapRequestTable[clientSN]=CS_SUCCESS;
						}
						else
						{
							m_mapRequestTable[clientSN]=CS_OVER_CONNECTION_LIMIT;
						}

					}
					else if(StringUtil::StringToUpper(pstRequest->strCmd) == "TEARDOWN")
					{

						m_mapRequestTable[clientSN]=CS_NONE;
						//m_mapRequestTable.erase(clientSN);
					}
				}


			}
		}

		command = command -> NextSiblingElement();
	}
	
	delete myDocument;
	myDocument=NULL;

	return reply::replyString(reply::ok);
}
/*
std::string CMediaProtocol::ProcessGetStream(string Param)
{
	int i=0;
	string hashIdx="";
	std::map <std::string, std::string> contentMap = ParameterToKeyPair(Param);

	int ch=atoi(contentMap["CH"].c_str());
	unsigned int utc=atol(contentMap["UTC"].c_str());
	int sessionID==atoi(contentMap["SESSIONID"].c_str());
	

	hashIdx=GetRequestSN(contentMap["RID"],ch,contentMap["CMD"],utc,sessionID);

	if(m_rcvQueueMgr->GetPacketQueue(hashIdx)==NULL)
			m_rcvQueueMgr->InitPacketQueue(hashIdx);

	if(m_rcvQueueMgr->GetPacketQueue(hashIdx)->Size()>0)
	{
		Packet *packet=m_rcvQueueMgr->GetPacketQueue(hashIdx)->GetPacket();
		if(packet->m_miStream==3 || packet->m_iStreamType==2)//文字 or jpeg
		{
			std::string s( reinterpret_cast<char const*>(packet->m_pData), packet->m_iDataSize ) ;
			delete packet;
			packet=NULL;
			return s;		
		}
	}

	return reply::replyString(reply::no_content);
	
}*/
std::string CMediaProtocol::ProcessAbility(string Param)
{
	int i=0;
	string hashIdx="";

	std::map <std::string, std::string> contentMap = ParameterToKeyPair(Param);

	if (!CheckKeyExist(Param,"RID",true))
	{
		//fp<<"Hello ABILITY!!=>> NO RID"<<endl;
		return reply::replyString(reply::no_content);
	}	 

	S_StreamAbility *pStreamAbility=ParseAbilityToMap(contentMap,Param);

	//先檢查是否已有request若有則再重送一次給DVR
	//先暫時略過
	/*map <string, SRequestInfo*>::iterator it;

	for(it=m_mapRequestInfo.begin();it!=m_mapRequestInfo.end();it++)
	{
			if(it->second->strRID==pStreamAbility->strRID)
			{
				if(it->second->strCmd.length()>0)
				{
					m_qRequest.push_back(it->second);
				}
			}
	}*/
#ifdef _DVRTEST
	string rid=contentMap["RID"];
	int streamNum=1;
	int ch=2;
	string streamType="LIVE";
	unsigned int utc=0;
	int sessionID=1;
	rid=	StringUtil::StringReplaceA(rid,":","");
	AddRequest(rid,streamType,streamNum,ch,utc,sessionID);
#endif

#ifdef _WRITERAW
	if(g_iFd==-1)
	{
		wchar_t *VideoFile = L"D:\\video.h264";
#if 1
		g_iFd = _wopen(VideoFile, O_RDWR | O_CREAT | O_BINARY,0666);
#else
		fHandle = _wopen(VideoFile, O_RDWR | O_CREAT | O_TEXT,0666);
#endif
	}

	if(fpStartCode==NULL)
	{
		fpStartCode = fopen("StartCode.txt","a+");
		fprintf(fpStartCode,"----------------------------------------------------------------------------\r\n");
	}

#endif

	if(m_iStreamSource==JPEG)
	{
		//request 成功時先建立部分資料夾

		std::wstring wsRID(contentMap["RID"].begin(), contentMap["RID"].end());

		wsRID=StringUtil::StringReplaceW(wsRID,L":",L"_");
		std::wstring ridPath=m_strSavePath+L"\\"+wsRID;

		if(!DirIsExist(ridPath))
				_wmkdir( ridPath.c_str() );	

		for(i=0;i<pStreamAbility->nCHNum;i++)
		{
			std::wostringstream ch;
			ch << i+1;
			const std::wstring wsCH(ch.str());
			std::wstring channelPath=m_strSavePath+L"\\"+wsRID+L"\\CH"+wsCH;
			if(!DirIsExist(channelPath))
				_wmkdir( channelPath.c_str() );		
		}
	}

	return reply::replyString(reply::ok);	
}

std::string CMediaProtocol::ProcessRequest(std::string Param)
{
	string hashIdx="";
	int i=0;
	double oldTime,currTime;
	string replyStr="";
	bool hasRequest=false;
	string s=reply::replyString(reply::ok);	

	if (!CheckKeyExist(Param,"RID",true))
	{
		//fp<<"Hello REQUSET!! NO RID"<<endl;
		return reply::replyString(reply::no_content);
	}	

	map <string, string> contentMap = ParameterToKeyPair(Param);
	std::string rid=	StringUtil::StringReplaceA(contentMap["RID"],":","");
	std::map <std::string, S_StreamAbility*>::iterator itr = m_mapStreamAbility.find(contentMap["RID"]);
	//try the long polling,有就回應，沒有就等5秒
	time_t start=clock();

	int wait=5;//timeout 5秒
	if(itr != m_mapStreamAbility.end())
	{

		itr->second->uiLastRequestTime=time(0);

		while(true)
		{
				time_t currTime=clock();
				if(((double)(currTime - start) / CLOCKS_PER_SEC)>wait)
				{
					break;
				}
			EnterCriticalSection(&m_critSec);
				deque<S_RequestInfo*>::iterator pos; 
				for (pos = m_qRequest.begin(); pos != m_qRequest.end(); pos++)  
				{
					if((*pos)->strRID==rid)
					{
						hasRequest=true;
					}
				}	
			LeaveCriticalSection(&m_critSec);

			if(hasRequest)
				break;

			Sleep(100);
		}

		if(hasRequest)
		{
			replyStr="<CMDRequest>";
			EnterCriticalSection(&m_critSec);

			std::deque<S_RequestInfo*>::iterator it = m_qRequest.begin();  
			int cnt=m_qRequest.size();
			for (;it != m_qRequest.end();)  
			{
					S_RequestInfo *tmpReqInfo=(*it);
					if(tmpReqInfo->strRID==rid)
					{
						replyStr+= GetRequestStr(tmpReqInfo); 
						delete tmpReqInfo;
						tmpReqInfo=NULL;
						it=m_qRequest.erase(it);
					}
					else
						it++;					
			}		
			LeaveCriticalSection(&m_critSec);
			replyStr+="</CMDRequest>";
		}
		else
		{
			return reply::replyString(reply::no_content);
		}
	}
	else
	{
		while(true)
		{
			time_t currTime=clock();
			if(((double)(currTime - start) / CLOCKS_PER_SEC)>wait)
			{
				break;
			}
		}
		return reply::replyString(reply::no_content);
	}

	return replyStr;
}
/*
char* CH264Parser::FindNextStartCode(char *address)
{
	char *p = address;

	while (1)
	{	
		if (p>m_endPos-4)  return NULL;
		if ( *(p+0)==0x00 && *(p+1)==0x00 && *(p+2)==0x00 && *(p+3)==0x01 )
		{
			return p;
		}
		else
			p++;
	}
}*/
bool bStart=false;
string CMediaProtocol::ProcessStream(std::string Param,unsigned int hSocket)
{
	int i=0;
	char tmpBuffer[SHM_MAX_FRAME_SIZE];
	DVR_SM_HEADER sSmHeader;

	if (!CheckKeyExist(Param,"RID",true))
	{			
		return reply::replyString(reply::no_content);
	}

	m_multiPart.Read(Param);

	S_StreamData* pstStreamData = new S_StreamData;

	for(i=0;i<m_multiPart.Count();i++)
	{		
		MultiContent_S *pstContent=m_multiPart.GetItem(i);

		if(pstContent->ContentType== "text/plain")
		{		
			memset(&tmpBuffer[0], 0, SHM_MAX_FRAME_SIZE);
			ParseStreamData(pstStreamData,pstContent->Content);

			sSmHeader.iFps==0;
			sSmHeader.iFrameType= 0;

			if(string(pstStreamData->strType)=="Video")
			{
					sSmHeader.iStreamType=VIDEO;
					sSmHeader.iFps=pstStreamData->nVideoFPS;
					sSmHeader.iFrameType= pstStreamData->unVideoFrame;
			}				
			else if(string(pstStreamData->strType)=="Audio")
				sSmHeader.iStreamType=AUDIO;	




			///////////////////////////////
			///////////////////////////////
			std::ostringstream wosText;
			wosText<<"CARID="<<pstStreamData->strCarID<<",RID="<<pstStreamData->strRID<<",LON="<<pstStreamData->fGPSLognitude<<",LAT="<<
				pstStreamData->fGPSLatitude<<",ALT"<<pstStreamData->fGPSAltitude<<",SPEED="<<pstStreamData->fGPSSpeed<<",HEAD="<<
				pstStreamData->nGPSHeading<<",FIX="<<pstStreamData->unGPSFix<<",GX="<<pstStreamData->iG_X<<",GY="<<pstStreamData->iG_Y<<
				",GZ="<<pstStreamData->iG_Z<<",SN="<<pstStreamData->strDeviceSN<<",COMPANY="<<pstStreamData->StrCompany<<",DRIVER="<<
				pstStreamData->strDriver<<",TIMEZONE="<<pstStreamData->uiTimezone;

			CShm *pshm=m_shmMgr->GetShm(pstStreamData->strStreamID);
			if(pshm!=NULL)
			{
				if(strlen(pstStreamData->strCarID)>0)
				{
					DVR_SM_HEADER sSmTextHeader;
					int offset=0;
					int totalSize=0;
					sSmTextHeader.iStreamType=TEXT;
					sSmTextHeader.iFps=pstStreamData->nVideoFPS;
					sSmTextHeader.iFrameType= pstStreamData->unVideoFrame;
					sSmTextHeader.iDataSize=wosText.str().length();

					memcpy( &tmpBuffer[0], &sSmTextHeader, sizeof(sSmTextHeader));
					offset += sizeof(sSmTextHeader);
					totalSize  += offset;
					memcpy( &tmpBuffer[offset],wosText.str().c_str(), wosText.str().length());
					totalSize  +=  wosText.str().length();

					pshm->LibShm_WriteFrame(&tmpBuffer[0], MEDIA_OTHER, pstStreamData->unVideoFrame, totalSize);
				}
			}
			
		}
		else if(pstContent->ContentType == "application/octet-stream")
		{
			if(pstContent->ContentLength>0)
			{
				CShm *pshm=m_shmMgr->GetShm(pstStreamData->strStreamID);
				if(pshm!=NULL)
				{

					if(!bStart)
					{
						if(pstStreamData->unVideoFrame==V_IFREAM)
							bStart=true;	
						else
							continue;
					}

						int mediaType=1;
						if(sSmHeader.iStreamType==VIDEO)
							mediaType=MEDIA_VIDEO;		
						else if(sSmHeader.iStreamType==AUDIO)
							mediaType=MEDIA_AUDIO;
						else
							mediaType=MEDIA_OTHER;


						int offset=0;
						memset(&tmpBuffer[0], 0, SHM_MAX_FRAME_SIZE);

						sSmHeader.iDataSize=pstContent->ContentLength;
						memcpy( &tmpBuffer[0], &sSmHeader, sizeof(sSmHeader));
						offset += sizeof(sSmHeader);
						memcpy( &tmpBuffer[offset],pstContent->Content, pstContent->ContentLength);						
					  //printf("[%d]Write frame\n",pstStreamData->nChannel);
						pshm->LibShm_WriteFrame( &tmpBuffer[0],mediaType, pstStreamData->unVideoFrame, pstContent->ContentLength+offset);


#ifdef _WRITERAW
						//--測試寫raw檔
						if(sSmHeader.iStreamType==VIDEO)
						{
								if(g_iFd>=0)
								{
									//_write(g_iFd, Param.c_str(), Param.length());
									_write(g_iFd, (char*)pstContent->Content,pstContent->ContentLength);
								}		
								for (int i=0;i<64;i++)
									fprintf(fpStartCode,"%02x ",(unsigned char)(*(tmpBuffer+offset+i)));
								fprintf(fpStartCode,"\n");	
}


#endif
				}
			}
		}
//#ifdef _WRITEJPG
		else if(pstContent->ContentType =="image/jpeg")
		{
			if(m_iStreamSource==JPEG)
			{
				//std::ostringstream wosText;
				std::wostringstream ch,utc;
				ch << pstStreamData->nChannel;
				const std::wstring wsCH=StringUtil::IntToWString(pstStreamData->nChannel);
				unsigned int iUTC=pstStreamData->ulUTC>>32;
				unsigned int iUTCMSec=pstStreamData->ulUTC;
				utc << iUTC;
				
				const std::wstring wsUTC(utc.str());

				string rid=string(pstStreamData->strRID);
				std::wstring wsRID(rid.begin(), rid.end());

				wsRID=StringUtil::StringReplaceW(wsRID,L":",L"_");
				wstring wsPath=m_strSavePath+L"\\"+wsRID+L"\\CH"+wsCH+L"\\"+wsUTC+L".jpeg";
				//wstring wsTextPath=m_strSavePath+L"\\"+wsRID+L"\\CH"+wsCH+L"\\"+wsUTC+L".dat";
				
				/*int iFdText;
				iFdText = _wopen(wsTextPath.c_str(), O_RDWR | O_CREAT,0666);


				int textSize=wosText.str().size();
				WriteToFile(iFdText,wosText.str().c_str(),textSize);
				close(iFdText);*/

				int iFd=0;
				iFd = OpenFileWrite(wsPath.c_str());	
				//int ret=WriteToFile(iFd, (char*)pPacket->m_pData,pPacket->m_iDataSize);
				int ret=WriteToFile(iFd, (char*)pstContent->Content,pstContent->ContentLength);
				close(iFd);
			}
		}
	
//#endif
	}
/*
	string hashIdx=hashGen.Generate(pstStreamData->strRID,ToString(pstStreamData->nChannel));

	map <string, SRequestInfo*>::iterator it = m_mapRequestInfo.find(hashIdx);
	if (it != m_mapRequestInfo.end())
	{
		it->second->ullCurrentTime=pstStreamData->ulUTC;
	}*/

	
	//Sleep(10);
	delete pstStreamData;
	pstStreamData=NULL;

	m_multiPart.Clear();	
	return reply::replyString(reply::ok);
}
std::string CMediaProtocol::InnerMediaServerProtocol(UNICODE_MODE_ENUM uniMode,std::string command, std::string param,SOCKET hSocket)
{
	string replyStr;
	transform(command.begin(), command.end(),command.begin(), ::toupper);
	map <string, string> contentMap = ParameterToKeyPair(param);
	

	if (command == "/ABILITY")//當DVR第一次連Server時,發送DVR相關資訊給Server
	{
			string ret=ProcessAbility(param);
			return ret;
	}
	else if (command == "CMDREQUEST" || command == "/CMDREQUEST")//DVR 發送Request,詢問是否有VMS連線
	{		
			return ProcessRequest(param);		
	}  
	else if (command == "CMDRESULT" || command == "/CMDRESULT")//當dvr收到hs_server送的連線數目後的回應
	{
		//CStreamCmdResult* pstCmdResult = new CStreamCmdResult();
		return ProcessCmdResult(param);
	}
	else if (command == "STREAMING" || command == "/STREAMING")
	{
			return ProcessStream(param/*,hSocket*/);
	}
	/*else if (command == "/GETSTREAM")//當DVR第一次連Server時,發送DVR相關資訊給Server
	{
		return ProcessGetStream(param);
	}*/
	else if(command =="/CONNECT")
	{
			map <string, string> contentMap = ParameterToKeyPair(param);
			string rid=contentMap["RID"];
			int streamID=atoi(contentMap["STREAMID"].c_str());
			int ch=atoi(contentMap["CH"].c_str());
			string streamType=contentMap["CMD"];
			unsigned int utc=atol(contentMap["UTC"].c_str());
			int sessionID=atoi(contentMap["SESSIONID"].c_str());
			rid=	StringUtil::StringReplaceA(rid,":","");
		  AddRequest(rid,streamType,streamID,ch,utc,sessionID);
	}
	else if(command =="/DISCONNECT")
	{
		map <string, string> contentMap = ParameterToKeyPair(param);
		string rid=contentMap["RID"];
		int streamID=atoi(contentMap["STREAMID"].c_str());
		int ch=atoi(contentMap["CH"].c_str());
		string streamType=contentMap["CMD"];
		unsigned int utc=atol(contentMap["UTC"].c_str());
		int sessionID=atoi(contentMap["SESSIONID"].c_str());
		rid=	StringUtil::StringReplaceA(rid,":","");
		ReleaseRequest(rid,streamType,streamID,ch,utc,sessionID);
	}
	/*else if(command =="/TIME")
	{
		;//return GetStreamTime(param);
	}*/
	else
	{
		return reply::replyString(reply::not_found);
	}

//fp.close();//關閉檔案
return replyStr;
}
#ifdef _CLIENT_SERVICE
std::string CMediaProtocol::InnerMediaServerProtocol2(UNICODE_MODE_ENUM uniMode,std::string command, std::string param)
{
	string replyStr;
	transform(command.begin(), command.end(),command.begin(), ::toupper);
	map <string, string> contentMap = ParameterToKeyPair(param);

	if (command == "/GETSTREAM")//當DVR第一次連Server時,發送DVR相關資訊給Server
	{
			return ProcessGetStream(param);
	}
	else if(command =="/CONNECT")
	{
		return AddRequest(param);
	}
	else if(command =="/DISCONNECT")
	{
		return ReleaseRequest(param);
	}
	else if(command =="/TIME")
	{
		return GetStreamTime(param);
	}
	else
	{
		//fp<<"Hello NO ACTION ==> Not CMD.... !!"<<endl;
		return reply::replyString(reply::not_found);
	}

//fp.close();//關閉檔案
return replyStr;
}
std::string CMediaProtocol::ProtocolImplement2(const request& req, reply& rep)
{
	std::string repStr;
	std::string command = req.cmd;
	std::string param = req.post_data;
	mMethod = req.method;

	if (ToUpper(req.method)=="GET" && ToUpper(param).find("ACTION=")!=std::string::npos)
	{
		std::map <std::string, std::string> contentMap = ParameterToKeyPair(param);
		command = contentMap["ACTION"];
		param = param.substr(param.find("&")+1);

	}
	else if (ToUpper(req.method)=="POST" && ToUpper(param).find("ACTION=")!=std::string::npos)
	{

		param = ReplaceString(param,"&","\r\n");
		std::map <std::string, std::string> contentMap = ParameterToKeyPair(param);
		command = contentMap["ACTION"];
		param = param.substr(param.find("&")+1);

	}

	std::transform(command.begin(), command.end(),command.begin(), ::toupper);

	//分析Header
	std::string contenttype;

	for (int i=0;i<req.headers.size();i++)
	{
		if (req.headers[i].name== "Content-Type")
		{
			contenttype = req.headers[i].value;
		}
	}


	repStr = InnerMediaServerProtocol2(UNICODE_DATA_NORMAL, command,param);

	return repStr;
}
std::string CMediaProtocol::ProtocolPostProcessing2(std::string command,std::string param)
{
	std::transform(command.begin(), command.end(),command.begin(), ::toupper);
	return param;
}
#endif
//WriteStreamIntoQueue();

//============rex==============	
//  ethan
//	WriteStreamIntoQueue();
	//if (command=="/PUSHDATA")
	//{
	//	LogPushData_S content;
	//	unsigned __int64 PrimaryKey = GetUniqueID();
	//	
	//	if (!CheckKeyExist(param,"RID",true))
	//	{
	//		return reply::replyString(reply::no_content);
	//	}
	//	if (!CheckKeyExist(param,"CARID",true))
	//	{
	//		return reply::replyString(reply::no_content);
	//	}
	//	if (!CheckKeyExist(param,"UTC",true))
	//	{
	//		return reply::replyString(reply::no_content);
	//	}
	//													   
	//	if (CheckKeyExist(param,"RID")) content.RID = contentMap["RID"];
	//	if (CheckKeyExist(param,"CARID")) content.CARID = UTF8Decode(contentMap["CARID"]);
	//	if (CheckKeyExist(param,"UTC")) content.UTC = ToInt(contentMap["UTC"]);
	//	std::string queryRet = LogPushDataStructureToString(content,",");
	//	
	//	return queryRet;
	//}

std::map <std::string, std::string> CMediaProtocol::DataKeyPair(std::string param)
{
	std::map <std::string, std::string> mKeyPpairs;

	std::string reqStr = param;
	if (reqStr.rfind("\r\n")!=reqStr.length()-2)	
		reqStr.append("\r\n");
	mKeyPpairs = KeyPairToMap(reqStr,"\n"); 
	
	return mKeyPpairs;
}


std::map <std::string, std::string> CMediaProtocol::ParameterToKeyPair(std::string param)
{
	std::map <std::string, std::string> mKeyPpairs;
	if (mMethod == "POST")
	{
		std::string reqStr = param;
		if (reqStr.rfind("\r\n")!=reqStr.length()-2)	
			reqStr.append("\r\n");
		mKeyPpairs = KeyPairToMap(reqStr,"\n"); 
	}
	else if (mMethod == "GET")
	{
		std::string reqStr = param;
		reqStr.append("&");

		mKeyPpairs = KeyPairToMap(reqStr,"&");		
	}
	return mKeyPpairs;
}

time_t CMediaProtocol::StringToTimestamp(std::string DateTimeString,bool isGMT)
{
	struct tm t;
	struct tm * ptm;
	int year,month,day, hour,minute,second;

	std::istringstream istr (DateTimeString);

	istr >> year;
	istr.ignore();
	istr >> month;
	istr.ignore();
	istr >> day;
	istr.ignore();
	istr >> hour;
	istr.ignore();
	istr >> minute;
	istr.ignore();
	istr >> second;

	t.tm_year = year-1900;
	t.tm_mon = month-1;
	t.tm_mday = day;
	t.tm_hour = hour;
	t.tm_min = minute;
	t.tm_sec = second;
	t.tm_isdst = 0; 


	time_t t_of_day;
	time_t t_of_gmt;

	if(isGMT)
	{
		t_of_day=mktime(&t);//VC會扣掉timezone時間
		ptm = gmtime ( &t_of_day );
		t_of_gmt=mktime(ptm);  
		int diff=t_of_day-t_of_gmt;
		t_of_day=diff+t_of_day;
	}
	else
	{
		t_of_day= mktime(&t);  // timestamp in current timezone	
	}

	return t_of_day;

}

void CMediaProtocol::SetStreamSource(int Mode)
{
	m_iStreamSource=Mode;
}
void CMediaProtocol::SetJpegPath(wchar_t *Path)
{
	
	m_strSavePath=std::wstring (Path);
}
unsigned __int64 CMediaProtocol::GetUniqueID()
{
	unsigned __int64 uninum;
	time_t now;
	clock_t c_now;
	now = time(0);

	uninum = now*CLOCKS_PER_SEC + (float(now%(60*CLOCKS_PER_SEC))/CLOCKS_PER_SEC)*CLOCKS_PER_SEC  ;
	return uninum;
}


//bool CMediaProtocol::WriteStreamIntoQueue()
//{
//	return true;
//}