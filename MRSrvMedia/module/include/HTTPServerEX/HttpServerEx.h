//---------------------------------------------------------------------------

#ifndef HttpServerH
#define HttpServerH
//---------------------------------------------------------------------------



#include <string>
#include <vector>

#include "CLock.h"

#include "request.h"
#include "reply.h"
namespace HS
{
namespace Net
{

	typedef void* HTTP_SERVER_HANDLE;
	typedef void* SOCKET_HANDLE;
	typedef void (*EXT_HTTP_HANDLER)(const request& req, reply& rep,void* param);
	typedef void (*EXT_HTTP_CALLBACK)(void);
	typedef int (*CALLBACK_HTTP_STATUS)(SOCKET);

	typedef enum SOCKET_MODE {BLOCKING=0, NONBLOCKING};
	typedef struct THeaderMember
	{
		std::string RequestLine;
		std::string Document;
		std::string Param;
		std::string Accept;
		std::string AcceptLanguage;
		std::string UserAgent;
		std::string AcceptEncoding;
		std::string Host;
		int ContentLength;
		std::string ConnectionType;
		std::string ContentType;
		std::string HttpVersion;
	}THeaderMember;


	typedef struct TListenInfo
	{
		HANDLE hThreadListen;
		unsigned int hListenThreadID;
		void *Parent;
		bool exitflag;
	}TListenInfo;

	typedef struct TConnectInfo
	{
		std::string keyID;
		int ThreadIndex;
		bool exitflag; 
		unsigned int hAcceptThreadID;
		HANDLE hThreadAccept;
		SOCKET_HANDLE socket;
		void *Parent;
		THeaderMember Header;

	}TConnectInfo;

	class HttpServer
	{
	private:

		SOCKET_MODE mType;
		int mPort;
		int mConnections;
		int mCount;
		bool m_status;
		unsigned int mUniID;



		HTTP_SERVER_HANDLE httpHandle;
		TListenInfo listenInfo;
		EXT_HTTP_CALLBACK handleHttpCallback;//在Server thread 不管有沒有Request 都會呼叫的callback   
		EXT_HTTP_HANDLER handleHttpRequest;	 //只有收到Request 才會呼叫的 callback
		CALLBACK_HTTP_STATUS handleHttpStatus;

		void *handleHttpCallback_Param;
		void *handleHttpRequest_Param;
		void *handleHttpStatus_Param;

		bool ExitFlag;
		CCriticalSection cs;
		CEvent *evWaitThread;
		std::vector <TConnectInfo*> m_ClientConnections;
		CEvent *evWaitWorkingThread;

		std::string StringBetween(std::string s,std::string startstr,std::string endstr);
		bool GetHeaderMember(TConnectInfo *info);
		std::string HeaderString;
		bool isDebug;
		std::string mInformation;
		std::string DebugOptionContents;
		static UINT __stdcall HttpListenFunc(void* param);
		static UINT __stdcall HttpWorkingThreadFunc(void* param);

	public:
		CEvent *evWaitcallback;
		HttpServer(int port, int connections, SOCKET_MODE type=NONBLOCKING);
		HttpServer();
		~HttpServer();

		void Listen(int port,int connections, SOCKET_MODE type=NONBLOCKING);
		void SetPort(int port){mPort = port;};
		void Start();
		void Stop();
		

		void SetHttpRequestHandler(EXT_HTTP_HANDLER handler,void *param);
		void SetHttpUpdateHandler(EXT_HTTP_CALLBACK handler,void *param);
		void SetHttpStatusHandler(CALLBACK_HTTP_STATUS handler,void *param);
		void DeleteHttpRequestHandler();
		void SetServerInformation(const std::string &info){mInformation = info;};
		bool GetStatus() {return m_status;};


	};

}//end of namespace Net
}//end of namespace HS

#endif
