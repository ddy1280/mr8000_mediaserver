//---------------------------------------------------------------------------

#ifndef CLockH
#define CLockH
//---------------------------------------------------------------------------
#include <windows.h>

class CCriticalSection
{
private:
	CRITICAL_SECTION mCritSect;
public:
	CCriticalSection()
	{
		InitializeCriticalSection(&mCritSect);
	}
	~CCriticalSection()
	{
		DeleteCriticalSection(&mCritSect);
	}
	void Enter()
	{
		EnterCriticalSection(&mCritSect);
	}
	void Leave()
	{
		LeaveCriticalSection(&mCritSect);
	}
};

//自動釋放 CriticalSection , 適合使用在區域函式內
class CLock
{
private:
	CCriticalSection mCs;
public:
	CLock()
	{
		mCs.Enter();
	}
	~CLock()
	{
		mCs.Leave();
	}
};


//=========================================================================================================================================
//CEvent
//=========================================================================================================================================
class CEvent
{
private:
	HANDLE mEvent;
public:
	CEvent(bool bManualReset,bool bInitialState,LPCWSTR cEventObjName)
	{
		mEvent = ::CreateEvent(NULL,bManualReset,bInitialState,cEventObjName);
	}
	~CEvent()
	{
		if (mEvent) CloseHandle(mEvent);
	}

	void SetEvent()
	{
		if (mEvent) ::SetEvent(mEvent);
	}
	void ResetEvent()
	{
		if (mEvent) ::ResetEvent(mEvent);
	}
	void WaitFor(DWORD timeout=INFINITE)
	{
		if (mEvent) ::WaitForSingleObject(mEvent,timeout);
	}


};


#endif
