#include "StdAfx.h"
#include "libshm.h"


//Share memory
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
CShm::CShm(void)
{

}
CShm::~CShm(void)
{

}
//---------------------------------------------------------------------------
// First create should be master ==> the process to generate media frames
int CShm::Init(int iMode,const char *Name)
{
	int iCh, iShmSize, iFrame, iClient;

#ifdef LIBSHM_SUPPORT_SUBSTREAM
	iShmSize = (STREAM_SHM_SIZE_SUB * LIBSHM_CHANNELS) + (STREAM_SHM_SIZE_MAIN * LIBSHM_CHANNELS) +
		(LIBSHM_MANAGE_SIZE * LIBSHM_CHANNELS * 2);
#else
	iShmSize = (STREAM_SHM_SIZE_MAIN * LIBSHM_CHANNELS) + (LIBSHM_MANAGE_SIZE * LIBSHM_CHANNELS);
#endif

	m_LibshmId = CreateFileMappingA(
		(HANDLE)LIBSHM_KEY,      // use paging file
		NULL,                    // default security 
		PAGE_READWRITE,          // read/write access
		0,                       // maximum object size (high-order DWORD) 
		iShmSize,                // maximum object size (low-order DWORD)  
		Name);                   // name of mapping object

	if (m_LibshmId >= 0)
	{
		printf("Create share memory");
	} 
	else {
		m_LibshmId = CreateFileMappingA((HANDLE)LIBSHM_KEY,NULL,PAGE_READONLY,0,0,Name);   
		printf("Maybe some other daemon created share memory");
	}

	m_pLibShm = (char*) MapViewOfFile(m_LibshmId,   // handle to map object
		FILE_MAP_ALL_ACCESS, // read/write permission
		0,                   
		0,                   
		0);  



	m_pstShmHandler = (S_SHM_HANDLER *) m_pLibShm;

	if (iMode == SHM_MODE_MASTER)
	{
		memset((void*)m_pLibShm, 0x00, iShmSize);

		for (iCh=0; iCh<LIBSHM_CHANNELS; iCh++)
		{
			m_pstShmHandler[iCh].Source = -1;
			m_pstShmHandler[iCh].iShmSize = STREAM_SHM_SIZE_MAIN;
#ifndef LIBSHM_SUPPORT_SUBSTREAM
			//      m_pstShmHandler[iCh].Shm_Ptr = m_pLibShm + (LIBSHM_MANAGE_SIZE * LIBSHM_CHANNELS) + (STREAM_SHM_SIZE_MAIN * iCh);
			m_pstShmHandler[iCh].Shm_Ptr = (LIBSHM_MANAGE_SIZE * LIBSHM_CHANNELS) + (STREAM_SHM_SIZE_MAIN * iCh);
#else
			//      m_pstShmHandler[iCh].Shm_Ptr = m_pLibShm + (LIBSHM_MANAGE_SIZE * LIBSHM_CHANNELS * 2) + (STREAM_SHM_SIZE_MAIN * iCh);
			m_pstShmHandler[iCh].Shm_Ptr = (LIBSHM_MANAGE_SIZE * LIBSHM_CHANNELS * 2) + (STREAM_SHM_SIZE_MAIN * iCh);
#endif
			m_pstShmHandler[iCh].Shm_Ptr_Buttom = m_pstShmHandler[iCh].Shm_Ptr + STREAM_SHM_SIZE_MAIN;
			m_pstShmHandler[iCh].Wr_ptr = 0;
			for (iFrame=0; iFrame<MAX_KEY_FRAME_NUM; iFrame++)
				m_pstShmHandler[iCh].iKeyFrameOffset[iFrame] = 0;
			m_pstShmHandler[iCh].iKeyFrameCnt = 0;
			m_pstShmHandler[iCh].iKeyPtrS = 0;
			m_pstShmHandler[iCh].iKeyPtrE = 0;
			m_pstShmHandler[iCh].iOvweWrite = 0;
			m_pstShmHandler[iCh].iKeyChkE = 0;
			for (iClient=0; iClient<MAX_SHM_CLIENT; iClient++)
			{
				m_pstShmHandler[iCh].stShmClient[iClient].iClientHandle = -1;
				m_pstShmHandler[iCh].stShmClient[iClient].mode = -1;
				m_pstShmHandler[iCh].stShmClient[iClient].Rd_ptr = 0;
				m_pstShmHandler[iCh].stShmClient[iClient].iReadKeySeq = 0;
			}
			m_pstShmHandler[iCh].iFreeSize = STREAM_SHM_SIZE_MAIN;
			m_pstShmHandler[iCh].Space_Status = 0;
			m_pstShmHandler[iCh].Shm_Release = 0;
			m_pstShmHandler[iCh].Shm_ReleaseCnt = 0;
			m_pstShmHandler[iCh].ShmLock = 0;

#ifdef LIBSHM_SUPPORT_SUBSTREAM
			m_pstShmHandler[iCh + LIBSHM_CHANNELS].Source = -1;
			m_pstShmHandler[iCh + LIBSHM_CHANNELS].iShmSize = STREAM_SHM_SIZE_SUB;
			//      m_pstShmHandler[iCh + LIBSHM_CHANNELS].Shm_Ptr = m_pLibShm + (LIBSHM_MANAGE_SIZE * LIBSHM_CHANNELS * 2) + (STREAM_SHM_SIZE_MAIN * LIBSHM_CHANNELS) + (STREAM_SHM_SIZE_SUB * iCh);
			m_pstShmHandler[iCh + LIBSHM_CHANNELS].Shm_Ptr = (LIBSHM_MANAGE_SIZE * LIBSHM_CHANNELS * 2) + \
				(STREAM_SHM_SIZE_MAIN * LIBSHM_CHANNELS) + (STREAM_SHM_SIZE_SUB * iCh);											  
			m_pstShmHandler[iCh + LIBSHM_CHANNELS].Shm_Ptr_Buttom = m_pstShmHandler[iCh+LIBSHM_CHANNELS].Shm_Ptr + STREAM_SHM_SIZE_SUB;
			m_pstShmHandler[iCh + LIBSHM_CHANNELS].Wr_ptr = 0;
			for (iFrame=0; iFrame<MAX_KEY_FRAME_NUM; iFrame++)
				m_pstShmHandler[iCh + LIBSHM_CHANNELS].iKeyFrameOffset[iFrame] = 0;
			m_pstShmHandler[iCh + LIBSHM_CHANNELS].iKeyFrameCnt = 0;
			m_pstShmHandler[iCh + LIBSHM_CHANNELS].iKeyPtrS = 0;
			m_pstShmHandler[iCh + LIBSHM_CHANNELS].iKeyPtrE = 0;
			m_pstShmHandler[iCh + LIBSHM_CHANNELS].iOvweWrite = 0;
			m_pstShmHandler[iCh + LIBSHM_CHANNELS].iKeyChkE = 0;
			for (iClient=0; iClient<MAX_SHM_CLIENT; iClient++)
			{
				m_pstShmHandler[iCh + LIBSHM_CHANNELS].stShmClient[iClient].iClientHandle = -1;
				m_pstShmHandler[iCh + LIBSHM_CHANNELS].stShmClient[iClient].mode = -1;
				m_pstShmHandler[iCh + LIBSHM_CHANNELS].stShmClient[iClient].Rd_ptr = 0;
				m_pstShmHandler[iCh + LIBSHM_CHANNELS].stShmClient[iClient].iReadKeySeq = 0;
			}
			m_pstShmHandler[iCh + LIBSHM_CHANNELS].iFreeSize = STREAM_SHM_SIZE_SUB;
			m_pstShmHandler[iCh + LIBSHM_CHANNELS].Space_Status = 0;
			m_pstShmHandler[iCh + LIBSHM_CHANNELS].Shm_Release = 0;
			m_pstShmHandler[iCh + LIBSHM_CHANNELS].Shm_ReleaseCnt = 0;
			m_pstShmHandler[iCh + LIBSHM_CHANNELS].ShmLock = 0;
#endif
		}
	} else {
		for (iCh=0; iCh<LIBSHM_CHANNELS; iCh++)
		{
#ifndef LIBSHM_SUPPORT_SUBSTREAM    
			//      m_pstShmHandler[iCh].Shm_Ptr = m_pLibShm + (LIBSHM_MANAGE_SIZE * LIBSHM_CHANNELS) + (STREAM_SHM_SIZE_MAIN * iCh);
			//      m_pstShmHandler[iCh].Shm_Ptr_Buttom = m_pstShmHandler[iCh].Shm_Ptr + STREAM_SHM_SIZE_MAIN;
#else
			//      m_pstShmHandler[iCh].Shm_Ptr = m_pLibShm + (LIBSHM_MANAGE_SIZE * LIBSHM_CHANNELS * 2) + (STREAM_SHM_SIZE_MAIN * iCh);
			//      m_pstShmHandler[iCh].Shm_Ptr_Buttom = m_pstShmHandler[iCh].Shm_Ptr + STREAM_SHM_SIZE_MAIN;
			//      m_pstShmHandler[iCh + LIBSHM_CHANNELS].Shm_Ptr = m_pLibShm + (LIBSHM_MANAGE_SIZE * LIBSHM_CHANNELS * 2) + (STREAM_SHM_SIZE_MAIN * LIBSHM_CHANNELS) + (STREAM_SHM_SIZE_SUB * iCh);
			//      m_pstShmHandler[iCh + LIBSHM_CHANNELS].Shm_Ptr_Buttom = m_pstShmHandler[iCh+LIBSHM_CHANNELS].Shm_Ptr + STREAM_SHM_SIZE_SUB;	  
#endif
		}
	}
	return 0;
}

//---------------------------------------------------------------------------
// Return handle for creator, return -1 if source already created
// Source 0 ~ 7 for major channel, 8 ~ 15 for sub-channel
int CShm::LibShm_CreateChannel(int source)
{
	int Handle = 0, iClient;

#ifndef LIBSHM_SUPPORT_SUBSTREAM
	do {
		if (m_pstShmHandler[Handle].Source != -1)
			Handle += 1;
	} while ((m_pstShmHandler[Handle].Source != -1) && (Handle < LIBSHM_CHANNELS));
#else
	if (source >= LIBSHM_CHANNELS)    // Check sub-channel
		Handle = LIBSHM_CHANNELS;
	do {
		if (m_pstShmHandler[Handle].Source != -1)
			Handle += 1;
	} while ((m_pstShmHandler[Handle].Source != -1) && (Handle < (LIBSHM_CHANNELS * 2)));
#endif
#ifndef LIBSHM_SUPPORT_SUBSTREAM
	if (Handle < LIBSHM_CHANNELS) {
#else
	if (Handle < LIBSHM_CHANNELS*2) {
#endif
		m_pstShmHandler[Handle].Source = source;
		m_pstShmHandler[Handle].Wr_ptr = 0;
		m_pstShmHandler[Handle].iKeyFrameCnt = 0;
		m_pstShmHandler[Handle].iKeyPtrS = 0;
		m_pstShmHandler[Handle].iKeyPtrE = 0;
		m_pstShmHandler[Handle].iOvweWrite = 0;

		for (iClient=0; iClient<MAX_SHM_CLIENT; iClient++)
		{
			m_pstShmHandler[Handle].stShmClient[iClient].iClientHandle = -1;
			m_pstShmHandler[Handle].stShmClient[iClient].mode = -1;
			m_pstShmHandler[Handle].stShmClient[iClient].Rd_ptr = 0;
			m_pstShmHandler[Handle].stShmClient[iClient].iReadKeySeq = 0;
		}
#ifndef LIBSHM_SUPPORT_SUBSTREAM
		m_pstShmHandler[Handle].iFreeSize = STREAM_SHM_SIZE_MAIN;
#else
		if (source < LIBSHM_CHANNELS)
			m_pstShmHandler[Handle].iFreeSize = STREAM_SHM_SIZE_MAIN;
		else
			m_pstShmHandler[Handle].iFreeSize = STREAM_SHM_SIZE_SUB;
#endif
		m_pstShmHandler[Handle].Space_Status = 0;
		m_pstShmHandler[Handle].ShmLock = 0;
		m_pstShmHandler[Handle].Shm_Release = 0;
		m_pstShmHandler[Handle].Shm_ReleaseCnt = 0;
		return Handle;
	} else {
		return -1;
	}
}
//---------------------------------------------------------------------------
//
int CShm::LibShm_ReleaseChannel(int Handle)
{

	int idx = 0, ClientConnected = 0;

#ifndef LIBSHM_SUPPORT_SUBSTREAM
	if ((Handle < 0) || (Handle >= LIBSHM_CHANNELS))
		return -1;
#else
	if ((Handle < 0) || (Handle >= (LIBSHM_CHANNELS * 2)))
		return -1;
#endif
	for (idx=0; idx<MAX_SHM_CLIENT; idx++)
	{
		if (m_pstShmHandler[Handle].stShmClient[idx].iClientHandle != -1) {
			ClientConnected += 1;
		}
	}
	if (ClientConnected == 0) {
		m_pstShmHandler[Handle].Source = -1;
		return 0;
	} else {
		m_pstShmHandler[Handle].Shm_Release = 1;
		return ClientConnected;
	}
}
//---------------------------------------------------------------------------
int CShm::LibShm_UpdateState(void)
{
	int iHandle, idx, iConnected = 0, iReleased = 0;

	for (iHandle=0; iHandle<LIBSHM_CHANNELS; iHandle++)
	{
		if (m_pstShmHandler[iHandle].Shm_Release)
		{
			for (idx=0; idx<MAX_SHM_CLIENT; idx++)
			{
				if (m_pstShmHandler[iHandle].stShmClient[idx].iClientHandle != -1)
					iConnected += 1;
			}
			if (iConnected) {
				m_pstShmHandler[iHandle].Shm_ReleaseCnt += 1;
				if (m_pstShmHandler[iHandle].Shm_ReleaseCnt >= SHM_RELEASE_MAX_COUNT)
					m_pstShmHandler[iHandle].Source = -1;
			} else {
				m_pstShmHandler[iHandle].Source = -1;
				iReleased += 1;
			}
		}
	}
	return iReleased;
}
//---------------------------------------------------------------------------
// Return client attach handle, -1 attach failed
int CShm::LibShm_ClientAttach(int source)
{
	int iHandle = 0, idx = 0, iClientHandle = -1, iMaxChannel;

#ifndef LIBSHM_SUPPORT_SUBSTREAM
	iMaxChannel = LIBSHM_CHANNELS;
#else
	iMaxChannel = LIBSHM_CHANNELS * 2;
#endif
	do {
		if (m_pstShmHandler[iHandle].Source != source)
			iHandle += 1;
	} while((m_pstShmHandler[iHandle].Source != source) && (iHandle < iMaxChannel));

	if (iHandle < iMaxChannel)
	{
		do {
			if (m_pstShmHandler[iHandle].stShmClient[idx].iClientHandle != -1)
				idx += 1;
		} while((m_pstShmHandler[iHandle].stShmClient[idx].iClientHandle != -1) && (idx < MAX_SHM_CLIENT));
		if (idx < MAX_SHM_CLIENT) {
			iClientHandle = (iHandle << 8) + idx;
			m_pstShmHandler[iHandle].stShmClient[idx].iClientHandle = iClientHandle;
			m_pstShmHandler[iHandle].stShmClient[idx].mode = 0;
			m_pstShmHandler[iHandle].stShmClient[idx].Rd_ptr = 0;
			m_pstShmHandler[iHandle].stShmClient[idx].iReadKeySeq = -1;
		}
	} else
		return -1;

	return iClientHandle;
}

//---------------------------------------------------------------------------
int CShm::LibShm_ClientDetach(int Handle)
{
	int Src_Handle, idx;
	int iMaxChannel;

	if(Handle<0)
		return -1;
#ifndef LIBSHM_SUPPORT_SUBSTREAM
	iMaxChannel = LIBSHM_CHANNELS;
#else
	iMaxChannel = LIBSHM_CHANNELS * 2;
#endif
	Src_Handle = (Handle >> 8) & 0xFF;
	idx = Handle & 0xFF;

	if ((Src_Handle < 0) || (Src_Handle >= iMaxChannel))
		return -1;

	if ((idx < 0) || (idx >= MAX_SHM_CLIENT))
		return -1;

	if (m_pstShmHandler[Src_Handle].Source == -1)
		return -1;

	m_pstShmHandler[Src_Handle].stShmClient[idx].iClientHandle = -1;

	return 0;
}
int CShm::LibShm_GetClientCount(int Handle)
{
		int i=0;
		int ret=0;
		for(i=0;i<MAX_SHM_CLIENT;i++)
			if(m_pstShmHandler[Handle].stShmClient[i].iClientHandle !=-1)
				ret++;

		return ret;

}
//---------------------------------------------------------------------------
int CShm::LibShm_Destory(void)
{

	UnmapViewOfFile(m_pLibShm);
	CloseHandle (m_LibshmId);
//	shmdt(m_pLibShm);
//	shmctl(m_LibshmId, 0, NULL);
	return 0;
}

//---------------------------------------------------------------------------
#if 0
int LibShm_Lock(int Handle)
{
	S_SHM_HANDLER *pstShmHandle;

	pstShmHandle = &m_pstShmHandler[Handle];
	pstShmHandle->ShmLock = 1;
	return 0;
}
#endif

int CShm::LibShm_IsLock( int Handle )
{
	S_SHM_HANDLER *pstShmHandle;
	int retry = 10;

	pstShmHandle = &m_pstShmHandler[Handle];
	while( (pstShmHandle->ShmLock) && (retry))
	{
		mysleep(1);
		retry -= 1;
	}

	if( pstShmHandle->ShmLock )
	{
		return -1;
	} else {
		return 0;
	}
}
#if 1
int CShm::LibShm_Lock(int Handle)
{
	S_SHM_HANDLER *pstShmHandle;
	int retry = 10;

	pstShmHandle = &m_pstShmHandler[Handle];
	while ((pstShmHandle->ShmLock) && (retry))
	{
		mysleep(1);
		retry -= 1;
	}

	if (pstShmHandle->ShmLock) {
		return -1;
	} else {
		pstShmHandle->ShmLock = 1;
		return 0;
	}
}
#endif
//---------------------------------------------------------------------------
int CShm::LibShm_Unlock(int Handle)
{
	S_SHM_HANDLER *pstShmHandle;

	pstShmHandle = &m_pstShmHandler[Handle];
	pstShmHandle->ShmLock = 0;

	return 0;
}
//---------------------------------------------------------------------------
int CShm::LibShm_ClientLock(int Handle)
{
	S_SHM_HANDLER *pstShmHandle;
	int retry = 10, Wr_Handle;
	int iMaxChannel;

#ifndef LIBSHM_SUPPORT_SUBSTREAM
	iMaxChannel = LIBSHM_CHANNELS;
#else
	iMaxChannel = LIBSHM_CHANNELS * 2;
#endif

	Wr_Handle = Handle >> 8 & 0xFF;
	if (Wr_Handle >= iMaxChannel)
		return -1;

	pstShmHandle = &m_pstShmHandler[Wr_Handle];
	while ((pstShmHandle->ShmLock) && (retry))
	{
		mysleep(1);
		retry -= 1;
	}

	if (pstShmHandle->ShmLock) {
		return -1;
	} else {
		pstShmHandle->ShmLock = 1;
		return 0;
	}
}
//---------------------------------------------------------------------------
int CShm::LibShm_ClientUnlock(int Handle)
{
S_SHM_HANDLER *pstShmHandle;
int iMaxChannel, Wr_Handle;

#ifndef LIBSHM_SUPPORT_SUBSTREAM
  iMaxChannel = LIBSHM_CHANNELS;
#else
  iMaxChannel = LIBSHM_CHANNELS * 2;
#endif

  Wr_Handle = Handle >> 8 & 0xFF;
  if (Wr_Handle >= iMaxChannel)
    return -1;

  pstShmHandle = &m_pstShmHandler[Wr_Handle];
  pstShmHandle->ShmLock = 0;

  return 0;
}

//---------------------------------------------------------------------------
/*
int Maintain_KeyFrameIndex(int Handle, int iSize)
{
S_SHM_HANDLER *pstShmHandle;
int idx;

  pstShmHandle = &m_pstShmHandler[Handle];

}
*/
//---------------------------------------------------------------------------
// Use current Wr_ptr as key frame address, use it to calculate offset. reserve frame length for calculate
int CShm::LibShm_IndexKeyFrame(int Handle)
{
S_SHM_HANDLER *pstShmHandle;
int idx, S_idx, cidx, found = 0, ridx;

  pstShmHandle = &m_pstShmHandler[Handle];
  idx = pstShmHandle->iKeyPtrE;
  pstShmHandle->iKeyFrameOffset[idx] = pstShmHandle->Wr_ptr;

  if ((idx > 1) && (pstShmHandle->iOvweWrite == 0)) {
    if (pstShmHandle->iKeyFrameOffset[idx] < pstShmHandle->iKeyFrameOffset[idx - 1])
      pstShmHandle->iOvweWrite = 1;
  }

  if ( pstShmHandle->iOvweWrite )
  {
    S_idx = pstShmHandle->iKeyPtrS;
//    DVR_LOG(DVRLOm_INFO,"Offset [%d[0x%x] - %d[0x%x]]", S_idx, pstShmHandle->iKeyFrameOffset[S_idx], idx,pstShmHandle->iKeyFrameOffset[idx]);

    if (idx == 0)
      ridx = MAX_KEY_FRAME_NUM - 1;
    else
      ridx = idx - 1;
    if (pstShmHandle->iKeyFrameOffset[idx] < pstShmHandle->iKeyFrameOffset[ridx]) {
#if 1
    int iNewE_idx = ridx;
    int iFound = 0;

      pstShmHandle->iKeyChkE = idx;
      while( iNewE_idx != S_idx )
      {
	    cidx = iNewE_idx-1;
	    if( cidx < 0 )
		  cidx = MAX_KEY_FRAME_NUM-1;
//	    DVR_LOG(DVRLOm_INFO," -Fix [%d[0x%x] - %d[0x%x]]", iNewE_idx, pstShmHandle->iKeyFrameOffset[iNewE_idx], cidx,pstShmHandle->iKeyFrameOffset[cidx]);
	    if( pstShmHandle->iKeyFrameOffset[iNewE_idx] < pstShmHandle->iKeyFrameOffset[cidx] )
	    {
		  iFound = 1;
		  if( iNewE_idx == S_idx )
			  iFound = 0;
		  break;
	    }
	    iNewE_idx--;
	    if( iNewE_idx < 0 )
		  iNewE_idx = MAX_KEY_FRAME_NUM-1;
      }
      if( iFound )
	    S_idx = iNewE_idx;
#else
      S_idx += 1;
      if (S_idx >= MAX_KEY_FRAME_NUM)
        S_idx = 0;        
#endif
    }

    while((!found) && (pstShmHandle->iKeyFrameOffset[S_idx] <= pstShmHandle->iKeyFrameOffset[idx]) && (S_idx != pstShmHandle->iKeyChkE))
    {
      cidx = S_idx;
//	  DVR_LOG(DVRLOm_INFO," -Del [%d[0x%x] - %d[0x%x]]", S_idx, pstShmHandle->iKeyFrameOffset[S_idx], idx,pstShmHandle->iKeyFrameOffset[idx]);
      S_idx += 1;
      if (S_idx >= MAX_KEY_FRAME_NUM) {
        S_idx = 0;
      }
      if (pstShmHandle->iKeyFrameOffset[S_idx] < pstShmHandle->iKeyFrameOffset[cidx]) {
        found = 1;
      }
    }
    pstShmHandle->iKeyPtrS = S_idx;
  }

/*
  if (idx > 1)
  {
    if (pstShmHandle->iKeyFrameOffset[idx] < pstShmHandle->iKeyFrameOffset[idx - 1])
    {
      S_idx = pstShmHandle->iKeyPtrS;
      while( pstShmHandle->iKeyFrameOffset[S_idx] < pstShmHandle->iKeyFrameOffset[idx])
      {
        S_idx++;
        if (S_idx >= MAX_KEY_FRAME_NUM)
          S_idx = 0;
      }
      pstShmHandle->iKeyPtrS = S_idx;
    }
  }
*/

  if ((idx + 1) >= MAX_KEY_FRAME_NUM)
    idx = 0;
  else
    idx += 1;

  pstShmHandle->iKeyPtrE = idx;
//  DVR_LOG(DVRLOm_INFO, "Index [%d - %d]",  pstShmHandle->iKeyPtrS, pstShmHandle->iKeyPtrE);

  return 0;
}
//---------------------------------------------------------------------------
int CShm::LibShm_PushFrame(int Handle, S_SHM_FRAME_HEADER *pstShmFrameHeader, char *pFrame, int IsKeyFrame)
{
S_SHM_HANDLER *pstShmHandle;
S_SHM_FRAME_HEADER  stShmHeader;
char *cptr, *dstptr;
int iSize;

  if (LibShm_Lock(Handle))
    return -1;
//  LibShm_Lock(Handle);

  pstShmHandle = &m_pstShmHandler[Handle];

  iSize = (sizeof(S_SHM_FRAME_HEADER) * 2) + pstShmFrameHeader->iLen;
#if 1
  if ((pstShmHandle->Wr_ptr + iSize) >= pstShmHandle->iShmSize ) {
#else
  if ((pstShmHandle->Wr_ptr + iSize) >= STREAM_SHM_SIZE_MAIN) {
#endif
    stShmHeader.Identify = SHM_FRAME_IDENTIFY;
    stShmHeader.Frame_mode = IS_EOF_FRAME;
//    dstptr = pstShmHandle->Shm_Ptr + pstShmHandle->Wr_ptr;
    dstptr = m_pLibShm + (pstShmHandle->Shm_Ptr + pstShmHandle->Wr_ptr);
    memcpy(dstptr, (char *)&stShmHeader, sizeof(S_SHM_FRAME_HEADER));

    pstShmHandle->Wr_ptr = 0;
  }
  if (pstShmFrameHeader->Frame_mode == IS_KEY_FRAME)
    LibShm_IndexKeyFrame(Handle);

//  dstptr = pstShmHandle->Shm_Ptr + pstShmHandle->Wr_ptr;
  dstptr = m_pLibShm + (pstShmHandle->Shm_Ptr + pstShmHandle->Wr_ptr);

  memcpy(dstptr, (char *)pstShmFrameHeader, sizeof(S_SHM_FRAME_HEADER));
  dstptr += sizeof(S_SHM_FRAME_HEADER);
  memcpy(dstptr, pFrame, pstShmFrameHeader->iLen);

  iSize = sizeof(S_SHM_FRAME_HEADER) + pstShmFrameHeader->iLen;
  pstShmHandle->Wr_ptr += iSize;

  LibShm_Unlock(Handle);
  return 0;
}

//---------------------------------------------------------------------------

int CShm::LibShm_WriteFrame(char *Frame, char Frame_type, char Frame_mode, int iLen)
{

int Handle=0;

S_SHM_FRAME_HEADER stShmFrameHeader;
int iMaxChannel;

#ifndef LIBSHM_SUPPORT_SUBSTREAM
  iMaxChannel = LIBSHM_CHANNELS;
#else
  iMaxChannel = LIBSHM_CHANNELS * 2;
#endif
  if ((Handle < 0) || (Handle >= iMaxChannel))
    return -1;

  stShmFrameHeader.Identify = SHM_FRAME_IDENTIFY;
  stShmFrameHeader.Frame_Type = Frame_type;
  stShmFrameHeader.Frame_mode = Frame_mode;
  stShmFrameHeader.iLen = iLen;

  if ((iLen > 0) && (Frame != NULL) && (iLen < SHM_MAX_FRAME_SIZE))
    LibShm_PushFrame(Handle, &stShmFrameHeader, Frame, Frame_mode);
  else
    return -1;

  return 0;
}

//---------------------------------------------------------------------------
//  return 0 when success, -1 for get failed
char* CShm::LibShm_GetLastKeyFrame(int Handle, int *Key_Idx)
{
int Wr_Handle, RdHandle;
int idx, offset;
S_SHM_HANDLER *pstShmHandle;
S_SHM_FRAME_HEADER *pstShmFrameHeader;
char *cptr;
int iMaxChannel;

#ifndef LIBSHM_SUPPORT_SUBSTREAM
  iMaxChannel = LIBSHM_CHANNELS;
#else
  iMaxChannel = LIBSHM_CHANNELS * 2;
#endif
  Wr_Handle = Handle >> 8;
  RdHandle = Handle & 0xFF;

  if ((Wr_Handle < 0) || (Wr_Handle >= iMaxChannel))
    return NULL;

  if ((RdHandle < 0) || (RdHandle >= MAX_SHM_CLIENT))
    return NULL;

  pstShmHandle = &m_pstShmHandler[Wr_Handle];
  idx = pstShmHandle->iKeyPtrS;
  offset = pstShmHandle->iKeyFrameOffset[idx];
#if 1
  if ((offset < 0) || (offset > pstShmHandle->iShmSize)) {
#else
  if ((offset < 0) || (offset > STREAM_SHM_SIZE_MAIN)) {
#endif
    printf("Frame offset error - %d", offset);
  }

//  cptr = &pstShmHandle->Shm_Ptr[offset];
  cptr = m_pLibShm+(pstShmHandle->Shm_Ptr+offset);
  pstShmFrameHeader = (S_SHM_FRAME_HEADER *) cptr;
  if (pstShmFrameHeader->Identify == SHM_FRAME_IDENTIFY) {
    pstShmHandle->stShmClient[RdHandle].Rd_ptr = offset + pstShmFrameHeader->iLen + sizeof(S_SHM_FRAME_HEADER);
    cptr += sizeof(S_SHM_FRAME_HEADER);
    // FrameBuf = cptr;
    *Key_Idx = idx;
    pstShmHandle->stShmClient[RdHandle].iReadKeySeq = idx;
    return cptr;
    //return idx;
  } else {
// if p-frame is overight I-frame, so need to find next I-Frame
    offset = pstShmHandle->iKeyFrameOffset[idx+1];
#if 1
    if ((offset < 0) || (offset > pstShmHandle->iShmSize)) {
#else
    if ((offset < 0) || (offset > STREAM_SHM_SIZE_MAIN)) {
#endif
      printf("Frame offset error - %d", offset);
    }
//    cptr = &pstShmHandle->Shm_Ptr[offset];
	cptr = m_pLibShm+(pstShmHandle->Shm_Ptr+offset);
    pstShmFrameHeader = (S_SHM_FRAME_HEADER *) cptr;	
	if( pstShmFrameHeader->Identify == SHM_FRAME_IDENTIFY )
	{
	  pstShmHandle->stShmClient[RdHandle].Rd_ptr = offset + pstShmFrameHeader->iLen + sizeof(S_SHM_FRAME_HEADER);
	  cptr += sizeof(S_SHM_FRAME_HEADER);
	  // FrameBuf = cptr;
	  *Key_Idx = idx+1;
	  pstShmHandle->stShmClient[RdHandle].iReadKeySeq = idx+1;
      pstShmHandle->iKeyPtrS = idx+1;
	  return cptr;
	}
    return NULL;
  }
}



//---------------------------------------------------------------------------
//  return 0 when success, -1 for get failed
char* CShm::LibShm_GetNewestKeyFrame(int Handle, int *Key_Idx)
{
	int Wr_Handle, RdHandle;
	int idx, offset;
	S_SHM_HANDLER *pstShmHandle;
	S_SHM_FRAME_HEADER *pstShmFrameHeader;
	char *cptr;
	int iMaxChannel;

#ifndef LIBSHM_SUPPORT_SUBSTREAM
	iMaxChannel = LIBSHM_CHANNELS;
#else
	iMaxChannel = LIBSHM_CHANNELS * 2;
#endif
	Wr_Handle = Handle >> 8;
	RdHandle = Handle & 0xFF;

	if ((Wr_Handle < 0) || (Wr_Handle >= iMaxChannel))
		return NULL;

	if ((RdHandle < 0) || (RdHandle >= MAX_SHM_CLIENT))
		return NULL;

	pstShmHandle = &m_pstShmHandler[Wr_Handle];

	if (pstShmHandle->iKeyPtrE == 0)
		idx = MAX_KEY_FRAME_NUM - 1;
	else
		idx = pstShmHandle->iKeyPtrE - 1;
	offset = pstShmHandle->iKeyFrameOffset[idx];
#if 1
	if ((offset < 0) || (offset > pstShmHandle->iShmSize)) {
#else
	if ((offset < 0) || (offset > STREAM_SHM_SIZE_MAIN)) {
#endif
		printf("Frame offset error - %d", offset);
	}

	//  cptr = &pstShmHandle->Shm_Ptr[offset];
	cptr = m_pLibShm+(pstShmHandle->Shm_Ptr+offset);
	pstShmFrameHeader = (S_SHM_FRAME_HEADER *) cptr;
	if (pstShmFrameHeader->Identify == SHM_FRAME_IDENTIFY) {
		pstShmHandle->stShmClient[RdHandle].Rd_ptr = offset + pstShmFrameHeader->iLen + sizeof(S_SHM_FRAME_HEADER);
		cptr += sizeof(S_SHM_FRAME_HEADER);
		// FrameBuf = cptr;
		pstShmHandle->stShmClient[RdHandle].iReadKeySeq = idx;
		*Key_Idx = idx;
		return cptr;
	} else {
		return NULL;
	}
}

//---------------------------------------------------------------------------
int CShm::LibShm_CheckNewestKeyIdx(int Handle)
{
	int Wr_Handle, RdHandle;
	S_SHM_HANDLER *pstShmHandle;
	int iMaxChannel;

#ifndef LIBSHM_SUPPORT_SUBSTREAM
	iMaxChannel = LIBSHM_CHANNELS;
#else
	iMaxChannel = LIBSHM_CHANNELS * 2;
#endif
	Wr_Handle = Handle >> 8;
	RdHandle = Handle & 0xFF;

	if ((Wr_Handle < 0) || (Wr_Handle >= iMaxChannel))
		return -1;

	if ((RdHandle < 0) || (RdHandle >= MAX_SHM_CLIENT))
		return -1;

	pstShmHandle = &m_pstShmHandler[Wr_Handle];

	return pstShmHandle->iKeyPtrE;
}

//---------------------------------------------------------------------------
char* CShm::LibShm_GetNextFrame(int Handle)
{
	int Wr_Handle, RdHandle;
	S_SHM_HANDLER *pstShmHandle;
	S_SHM_FRAME_HEADER *pstShmFrameHeader;
	char *cptr;
	int iMaxChannel;

#ifndef LIBSHM_SUPPORT_SUBSTREAM
	iMaxChannel = LIBSHM_CHANNELS;
#else
	iMaxChannel = LIBSHM_CHANNELS * 2;
#endif
	Wr_Handle = Handle >> 8;
	RdHandle = Handle & 0xFF;

	if ((Wr_Handle < 0) || (Wr_Handle >= iMaxChannel))
		return NULL;

	if ((RdHandle < 0) || (RdHandle >= MAX_SHM_CLIENT))
		return NULL;

	pstShmHandle = &m_pstShmHandler[Wr_Handle];

	if (pstShmHandle->Wr_ptr == pstShmHandle->stShmClient[RdHandle].Rd_ptr)
		return NULL;

	//  cptr = &pstShmHandle->Shm_Ptr[pstShmHandle->stShmClient[RdHandle].Rd_ptr];
	cptr = m_pLibShm+(pstShmHandle->Shm_Ptr+pstShmHandle->stShmClient[RdHandle].Rd_ptr);
	pstShmFrameHeader = (S_SHM_FRAME_HEADER *) &cptr[0];
	if (pstShmFrameHeader->Identify != SHM_FRAME_IDENTIFY) {
		printf("Identify header error ! 0x%x", Handle);
		return NULL;
	} else if (pstShmFrameHeader->Frame_mode == IS_EOF_FRAME) {
		pstShmHandle->stShmClient[RdHandle].Rd_ptr = 0;
		//    cptr = &pstShmHandle->Shm_Ptr[pstShmHandle->stShmClient[RdHandle].Rd_ptr];
		cptr = m_pLibShm+(pstShmHandle->Shm_Ptr+pstShmHandle->stShmClient[RdHandle].Rd_ptr);
		pstShmFrameHeader = (S_SHM_FRAME_HEADER *) &cptr[0];
		if (pstShmFrameHeader->Identify != SHM_FRAME_IDENTIFY) {
			printf("Identify header error ! 0x%x", Handle);
			return NULL;
		}
	}
	if (pstShmFrameHeader->Frame_mode == IS_KEY_FRAME) {
		if (pstShmHandle->stShmClient[RdHandle].iReadKeySeq < (MAX_KEY_FRAME_NUM - 1))
			pstShmHandle->stShmClient[RdHandle].iReadKeySeq += 1;
		else
			pstShmHandle->stShmClient[RdHandle].iReadKeySeq = 0;
	}

	pstShmHandle->stShmClient[RdHandle].Rd_ptr += (sizeof(S_SHM_FRAME_HEADER) + pstShmFrameHeader->iLen);

	cptr += sizeof(S_SHM_FRAME_HEADER);
	// FrameBuf = cptr;
	return cptr;
}

//---------------------------------------------------------------------------
// By client handle
int CShm::LibShm_GetFreeSpace(int Handle)
{
	int Wr_Handle, RdHandle, iFreeSpace = 0;
	S_SHM_HANDLER *pstShmHandle;
	int iMaxChannel;

#ifndef LIBSHM_SUPPORT_SUBSTREAM
	iMaxChannel = LIBSHM_CHANNELS;
#else
	iMaxChannel = LIBSHM_CHANNELS * 2;
#endif
	Wr_Handle = Handle >> 8;
	RdHandle = Handle & 0xFF;

	if ((Wr_Handle < 0) || (Wr_Handle >= iMaxChannel))
		return -1;

	if ((RdHandle < 0) || (RdHandle >= MAX_SHM_CLIENT))
		return -1;

	pstShmHandle = &m_pstShmHandler[Wr_Handle];

	if (pstShmHandle->Wr_ptr > pstShmHandle->stShmClient[RdHandle].Rd_ptr)
	{
#if 1
		iFreeSpace = pstShmHandle->iShmSize- pstShmHandle->Wr_ptr + pstShmHandle->stShmClient[RdHandle].Rd_ptr;
#else
		iFreeSpace = STREAM_SHM_SIZE_MAIN - pstShmHandle->Wr_ptr + pstShmHandle->stShmClient[RdHandle].Rd_ptr;
#endif
	} else if (pstShmHandle->Wr_ptr < pstShmHandle->stShmClient[RdHandle].Rd_ptr){
		iFreeSpace = pstShmHandle->stShmClient[RdHandle].Rd_ptr - pstShmHandle->Wr_ptr;
	} else if (pstShmHandle->Wr_ptr == pstShmHandle->stShmClient[RdHandle].Rd_ptr){
#if 1
		iFreeSpace = pstShmHandle->iShmSize;
#else
		iFreeSpace = STREAM_SHM_SIZE_MAIN;
#endif
	}
	return iFreeSpace;
}

//---------------------------------------------------------------------------
// By client handle
int CShm::LibShm_GetValidSize(int Handle)
{
	int Wr_Handle, RdHandle, iValidSize = 0;
	S_SHM_HANDLER *pstShmHandle;
	int iMaxChannel;

#ifndef LIBSHM_SUPPORT_SUBSTREAM
	iMaxChannel = LIBSHM_CHANNELS;
#else
	iMaxChannel = LIBSHM_CHANNELS * 2;
#endif
	Wr_Handle = Handle >> 8;
	RdHandle = Handle & 0xFF;

	if ((Wr_Handle < 0) || (Wr_Handle >= iMaxChannel))
		return -1;

	if ((RdHandle < 0) || (RdHandle >= MAX_SHM_CLIENT))
		return -1;

	pstShmHandle = &m_pstShmHandler[Wr_Handle];

	if (pstShmHandle->Wr_ptr > pstShmHandle->stShmClient[RdHandle].Rd_ptr)
	{
		iValidSize = pstShmHandle->Wr_ptr - pstShmHandle->stShmClient[RdHandle].Rd_ptr;
	} else if (pstShmHandle->Wr_ptr < pstShmHandle->stShmClient[RdHandle].Rd_ptr){
#if 1
		iValidSize = pstShmHandle->iShmSize - pstShmHandle->stShmClient[RdHandle].Rd_ptr + pstShmHandle->Wr_ptr;
#else
		iValidSize = STREAM_SHM_SIZE_MAIN - pstShmHandle->stShmClient[RdHandle].Rd_ptr + pstShmHandle->Wr_ptr;
#endif
	} else if (pstShmHandle->Wr_ptr == pstShmHandle->stShmClient[RdHandle].Rd_ptr){
		iValidSize = 0;
	}
	return iValidSize;
}

//---------------------------------------------------------------------------
// Reserve for fast read & free more memory, will porting if necessary 
int CShm::LibShm_ReadGop(char *FrameBuf, int iBlkSize)
{
	return 0;
}

//---------------------------------------------------------------------------