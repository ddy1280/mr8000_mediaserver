#pragma once

#include "libshm.h"
#include <string>
#include <map>
#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct __dvr_sm_header__ {
	int						iFrameType; //i-frame or p-frame
	int						iFps;
	int						iStreamType;//0:video,1:audio,2:text
	unsigned int	iUTC_sec;
	unsigned int	iUTC_us;
	int						iDataSize;
}DVR_SM_HEADER;


typedef struct SPacket
{
	DVR_SM_HEADER header;
	char *Data;
}Packet;


#define MEDIA_VIDEO    0x01
#define MEDIA_AUDIO    0x02
#define MEDIA_OTHER    0x04


typedef enum
{
	VIDEO_P_FRAME = 0,
	VIDEO_I_FRAME
} E_VIDEO_FORMAT;

class CReceiverQueue;
using namespace std;

typedef std::map<std::string, CShm*> MapShm;
typedef std::map<std::string, int> MapClient;

class ShmMgr
{
	//friend class ReceiverQueue;

public:

	ShmMgr(void);
	~ShmMgr(void);

	CShm* InitShm(string ClientSN);
	CShm* GetShm(string StreamSN);
	int ShmMgr::ClientAttach(string ClientSN);
		int ShmMgr::ClientDetach(string ClientSN);
	int GetClientHandle(string ClientSN);
	char* GetNextFrame(string ClientSN);

	//void AddPacket(string hashIdxStr, unsigned char* data, unsigned dataSize);
	//void AddPacket(string hashIdxStr,Packet *pPacket);
	//Packet* GetPacket( string hashIdxStr);

	//bool HasPackets(string hashIdxStr);
	//void Free(string hashIdxStr,Packet* pPacket);
	//int  Size(string hashIdxStr);
	//void Clear(string hashIdxStr);
	void ClearAll();
	void Delete(string StreamSN);
	void DeleteALL();

private:
	MapShm m_mapShms;
	MapClient m_mapClients;
	CRITICAL_SECTION m_critSec;
};
