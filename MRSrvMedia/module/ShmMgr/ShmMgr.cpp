#include "StdAfx.h"
#include "ShmMgr.h"
#include <assert.h>
#include <iomanip>// std::setfill, std::setw

ShmMgr::ShmMgr(void)
{
	InitializeCriticalSection(&m_critSec);
}

ShmMgr::~ShmMgr(void)
{
	DeleteCriticalSection(&m_critSec);

	m_mapClients.clear();
}

CShm* ShmMgr::InitShm(string ClientSN)
{

	//不存在則新增
	string streamSN="";
	string sessionID="";

	if(ClientSN.length()==40)
	{
		sessionID=ClientSN.substr(0,8);
		streamSN=ClientSN.substr(8,32);
	}
	else
		return NULL;

	EnterCriticalSection(&m_critSec);
	MapShm::iterator it = m_mapShms.find(streamSN);
	LeaveCriticalSection(&m_critSec);

	CShm *pshm;
	//不存在則新增
	if(it == m_mapShms.end())
	{
			EnterCriticalSection(&m_critSec);
			pshm= new CShm();
			m_mapShms.insert(pair<std::string, CShm*>(streamSN, pshm));
			LeaveCriticalSection(&m_critSec);
			pshm->Init(SHM_MODE_MASTER,streamSN.c_str());
			pshm->LibShm_CreateChannel();
	}
	else
	{
			pshm=it->second;
	}

	return pshm;
}

CShm* ShmMgr::GetShm(string StreamSN)
{

	map <std::string, CShm*>::iterator it = m_mapShms.find(StreamSN);

	if (it != m_mapShms.end())
		return it->second;

	return NULL;
}

int ShmMgr::ClientAttach(string ClientSN)
{

	string streamSN="";
	if(ClientSN.length()==40)
	{
		streamSN=ClientSN.substr(8,32);
	}
	else
		return -1;
	EnterCriticalSection(&m_critSec);
	map <std::string, int>::iterator it = m_mapClients.find(ClientSN);
	LeaveCriticalSection(&m_critSec);

	int clientHandle=-1;

	if (it == m_mapClients.end())
	{
		CShm* pshm=GetShm(streamSN);
		clientHandle=pshm->LibShm_ClientAttach();	
		EnterCriticalSection(&m_critSec);
		if(clientHandle!=-1)
		{
			m_mapClients.insert(pair<std::string, int>(ClientSN, clientHandle));
		}
		LeaveCriticalSection(&m_critSec);
	}

	return clientHandle;
}

int ShmMgr::ClientDetach(string ClientSN)
{
	string streamSN="";
	if(ClientSN.length()==40)
	{
		streamSN=ClientSN.substr(8,32);
	}
	else
		return -1;

	CShm* pshm=GetShm(streamSN);
	int clientHandle=pshm->LibShm_ClientDetach(GetClientHandle(ClientSN));

	if(clientHandle==0)
	{
		EnterCriticalSection(&m_critSec);
		m_mapClients.erase(ClientSN);
		LeaveCriticalSection(&m_critSec);
	}
	return clientHandle;
}

int ShmMgr::GetClientHandle(string ClientSN)
{
	map <std::string, int>::iterator it = m_mapClients.find(ClientSN);
	if (it != m_mapClients.end())
		return it->second;

	return -1;
}
char* ShmMgr::GetNextFrame(string ClientSN)
{
	string streamSN="";
	string sessionID="";

	if(ClientSN.length()==40)
	{
		sessionID=ClientSN.substr(0,8);
		streamSN=ClientSN.substr(8,32);
	}
	else
		return NULL;

	int clientHandle=GetClientHandle(ClientSN);
	CShm *psmh=GetShm(streamSN);

	if(psmh==NULL)
		return NULL;

	return psmh->LibShm_GetNextFrame(clientHandle);

}
void ShmMgr::ClearAll()
{
/*
	EnterCriticalSection(&m_critSec);
	for (MapShm::iterator it = m_mapShms.begin(); it != m_mapShms.end(); ++it)
	{
		it->second->Clear();
	}
	LeaveCriticalSection(&m_critSec);*/
}

void ShmMgr::Delete(string StreamSN)
{
	
	EnterCriticalSection(&m_critSec);
	MapShm::iterator it = m_mapShms.find(StreamSN);

	if (it != m_mapShms.end())
	{
			it->second->LibShm_Destory();
			delete it->second;
			it->second=NULL;
			m_mapShms.erase(StreamSN);	
	}

	LeaveCriticalSection(&m_critSec);
}

void ShmMgr::DeleteALL()
{
/*
	EnterCriticalSection(&m_critSec);

	for (MapQueue::iterator it = m_mapShmss.begin(); it != m_mapShmss.end(); ++it)
	{
		delete it->second;
		it->second=NULL;
	}
	m_mapShmss.clear();
	LeaveCriticalSection(&m_critSec);*/
}

