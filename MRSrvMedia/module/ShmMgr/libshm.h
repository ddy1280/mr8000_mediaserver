#ifndef __LIB_SHM_H__
#define __LIB_SHM_H__

#include <string>
#include <map>
#include <Windows.h>


#ifndef __LINUX__
#define  __LINUX__
#endif

#ifdef __LINUX__
#define mysleep(int)  Sleep(int)
#else
#define mysleep(int)  Sleep(int)
#endif

#define LIBSHM_SUPPORT_SUBSTREAM

//#define  LIBSHM_KEY       0x80100000

#define  LIBSHM_KEY       0xFFFFFFFF

#ifndef __LINUX__
#define  STREAM_SHM_SIZE_MAIN  0x100000    // 1MB for each channel (For testing)
#define  STREAM_SHM_SIZE_SUB   0x80000
#define  LIBSHM_CHANNELS       4
#else
#define  STREAM_SHM_SIZE_MAIN  0x300000    // 2MB for each channel (Real system)
#define  STREAM_SHM_SIZE_SUB   0x80000
#define	 STREAM_SHM_SIZE_JPG   0x50000
#define  LIBSHM_CHANNELS  1//4
#endif

#define  LIBSHM_MANAGE_SIZE  0x10000  // 64KB
//#define LIBSHM_MANAGE_SIZE 0x1000 // 4KB
#define  MAX_SHM_CLIENT   8
//#define  MAX_KEY_FRAME_NUM   0x1000
#define MAX_KEY_FRAME_NUM	0x400

#define SHM_FRAME_IDENTIFY   0xFF55AAFF

#define SHM_RELEASE_MAX_COUNT 0x100

#define SHM_MAX_FRAME_SIZE    0x40000

// Set share create mode
typedef enum
{
  SHM_MODE_SLAVE = 0,
  SHM_MODE_MASTER
} E_LIBSHM_MODE;

typedef enum
{
  SHM_USER_CLIENT = 0,
  SHM_USER_MASTER
} E_SHM_USER_MODE;

typedef struct
{
  int  iClientHandle;   // Claculate by (Handle << 4) + reg client no)
  int  mode;      // user mode (not used)
  int  Rd_ptr;    // put address offset from channel shm start address
  int  iReadKeySeq;
} S_SHM_USER;

typedef enum
{
  IS_NORMAL_FRAME = 0,
  IS_KEY_FRAME,
  IS_EOF_FRAME
} E_MEDIA_FRAME_MODE;

typedef struct
{
  unsigned int  Identify;
  char          Frame_Type;   // Reserved for different codec
  char          Frame_mode;
  unsigned short iFrameSeq;
  int           iLen;
} S_SHM_FRAME_HEADER;

typedef struct
{
  int  Source;
  int  iShmSize;
//  char *Shm_Ptr;
  int  Shm_Ptr;
//  char *Shm_Ptr_Buttom;
  int  Shm_Ptr_Buttom;
  int  Wr_ptr;    // put address offset from channel shm start address
  int  iKeyFrameOffset[MAX_KEY_FRAME_NUM];
  int  iKeyFrameCnt;
  int  iKeyPtrS;  // key frame index start cell
  int  iKeyPtrE;  // key frame index end cell
  int  iOvweWrite;
  int  iKeyChkE;
  S_SHM_USER  stShmClient[MAX_SHM_CLIENT];
  int  iFreeSize;
  int  Space_Status;
  int  ShmLock;
  short  Shm_Release;
  short  Shm_ReleaseCnt;
} S_SHM_HANDLER;

/*#ifdef __LINUX__

int libShm_CreateChannel(int Channel);
int libShm_PutFrame(int Handle, int Frame_Type, char *pData, int iLen);
int libShm_ClientRegister(int Channel);
void libShm_ReleaseChannel(int Handle);

int libShm_GetLastKeyFrame(int Handle, char *pData);
int libShm_GetOldestKeyFrame(int Handle, char *pData);

#endif*/

class CShm
{
	//friend class ReceiverQueue;

public:

	CShm(void);
	~CShm(void);

	int Init(int iMode,const char *Name);
	int LibShm_CreateChannel(int source=0);
	int LibShm_ReleaseChannel(int Handle=0);
	int LibShm_UpdateState(void);
	int LibShm_ClientAttach(int source=0);//新增一筆client
	int LibShm_ClientDetach(int Handle);//刪除一筆client
	int LibShm_Destory(void);

	int LibShm_IndexKeyFrame(int Handle);
	int LibShm_PushFrame(int Handle, S_SHM_FRAME_HEADER *pstShmFrameHeader, char *pFrame, int IsKeyFrame);
	int LibShm_WriteFrame(char *Frame, char Frame_type, char Frame_mode, int iLen);
	char* LibShm_GetLastKeyFrame(int Handle, int *Key_Idx);
	char* LibShm_GetNewestKeyFrame(int Handle, int *Key_Idx);
	int LibShm_CheckNewestKeyIdx(int Handle);
	char* LibShm_GetNextFrame(int Handle);
	int LibShm_GetFreeSpace(int Handle);
	int LibShm_GetValidSize(int Handle);
	int LibShm_GetClientCount(int Handle=0);

private:

	HANDLE m_LibshmId;
	char* m_pLibShm;
	S_SHM_HANDLER  *m_pstShmHandler;
  CRITICAL_SECTION m_critSec;


	int LibShm_IsLock(int Handle);
	int LibShm_Lock(int Handle);
	int LibShm_Unlock(int Handle);
	int LibShm_ClientLock(int Handle);
	int LibShm_ClientUnlock(int Handle);
	int LibShm_ReadGop(char *FrameBuf, int iBlkSize);

};




#endif
