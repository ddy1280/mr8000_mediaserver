// ShmMgr.cpp : 定義主控台應用程式的進入點。
//

#include "stdafx.h"
//#include "libshm.h"
#include "ShmMgr.h"
#include <iomanip>      // std::setfill, std::setw
#include <iostream>
#include <sstream>
#include <process.h>

typedef struct
{
	HANDLE hThread;
	HANDLE hMsg;
	unsigned int aThreadID;
	void *Parent;
	bool exitflag;
	int toggleCount;
} ThreadInfo;
unsigned char tmpdata[10];

ShmMgr *pShmMgr=NULL;

std::string GetStreamHashIndex(int SessionID)
{
	std::stringstream hashSrc;
	hashSrc << "000f3a026333" << "LIVE" << 1 << 1388538061; 

	std::string hashIdx = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	std::stringstream ret;
	ret << std::setfill ('0') << std::setw(8) << std::uppercase  << std::hex << SessionID;
	hashIdx = ret.str()+hashIdx;
	return hashIdx;	
}
std::string GetStreamHashIndex2(int SessionID)
{
	std::stringstream hashSrc;
	hashSrc << "000f3a026333" << "LIVE" << 1 << 1388538061; 

	std::string hashIdx = "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb";
	std::stringstream ret;
	ret << std::setfill ('0') << std::setw(8) << std::uppercase  << std::hex << SessionID;
	hashIdx = ret.str()+hashIdx;
	return hashIdx;	
}

static UINT WINAPI Thread1(LPVOID pvParam)
{
	int i=0,j=0;
	pShmMgr->InitShm(GetStreamHashIndex(1));
	pShmMgr->InitShm(GetStreamHashIndex(2));

	for(i=0;i<100;i++)
	{

			char frame[1024];
			for(j=0;j<1024;j++)
				frame[j]='a'+i;

			char tmpBuffer[SHM_MAX_FRAME_SIZE];
			int iTotalSize=0;
			DVR_SM_HEADER			sSmHeader;

			memset(&tmpBuffer[0], 0, SHM_MAX_FRAME_SIZE);

			int itOffset = 0; 
			memcpy( &tmpBuffer[0], &sSmHeader, sizeof(sSmHeader));
			iTotalSize += sizeof(sSmHeader);
			itOffset += sizeof(sSmHeader);
			memcpy( &tmpBuffer[itOffset], &frame, sizeof(frame));
			iTotalSize += sizeof(frame);

			CShm *psmh=pShmMgr->GetShm(GetStreamHashIndex(1));
			if(psmh!=NULL)
			{
				if(psmh->LibShm_WriteFrame( &tmpBuffer[0], MEDIA_VIDEO, VIDEO_P_FRAME, iTotalSize) < 0 )
					printf("Can not put stream o memory");
			}
	}


	return 0;
}
static UINT WINAPI Thread2(LPVOID pvParam)
{
	while(1)
	{	
		if(pShmMgr!=NULL)
		{
			int clientHandle=pShmMgr->GetClientHandle(GetStreamHashIndex(1));

			char *tmpBuffer;
			CShm *psmh=pShmMgr->GetShm(GetStreamHashIndex(1));
			if(psmh!=NULL)
			{
				tmpBuffer=psmh->LibShm_GetNextFrame(clientHandle);
				char * pcBuffer;

				if( tmpBuffer )
				{
					DVR_SM_HEADER		*pSmHeader;
					pSmHeader = (DVR_SM_HEADER *)&tmpBuffer[0];
					pcBuffer = tmpBuffer+sizeof(DVR_SM_HEADER);
				}	
			}
	
		}

		Sleep(10);
	}

	return 0;
}

static UINT WINAPI Thread3(LPVOID pvParam)
{

	while(1)
	{	
		if(pShmMgr!=NULL)
		{
			int clientHandle=pShmMgr->GetClientHandle(GetStreamHashIndex(2));

			char *tmpBuffer;
			CShm *psmh=pShmMgr->GetShm(GetStreamHashIndex(2));
			if(psmh!=NULL)
			{
				tmpBuffer=psmh->LibShm_GetNextFrame(clientHandle);
				char * pcBuffer;

				if( tmpBuffer )
				{
					DVR_SM_HEADER		*pSmHeader;
					pSmHeader = (DVR_SM_HEADER *)&tmpBuffer[0];
					pcBuffer = tmpBuffer+sizeof(DVR_SM_HEADER);
				}	
			}
		}

		Sleep(10);
	}

	return 0;
}
static UINT WINAPI ThreadSrc2(LPVOID pvParam)
{
	int i=0,j=0;
	pShmMgr->InitShm(GetStreamHashIndex2(1));


	for(i=0;i<100;i++)
	{

		char frame[1024];
		for(j=0;j<1024;j++)
			frame[j]='0'+i;

		char tmpBuffer[SHM_MAX_FRAME_SIZE];
		int iTotalSize=0;
		DVR_SM_HEADER			sSmHeader;

		memset(&tmpBuffer[0], 0, SHM_MAX_FRAME_SIZE);

		int itOffset = 0; 
		memcpy( &tmpBuffer[0], &sSmHeader, sizeof(sSmHeader));
		iTotalSize += sizeof(sSmHeader);
		itOffset += sizeof(sSmHeader);
		memcpy( &tmpBuffer[itOffset], &frame, sizeof(frame));
		iTotalSize += sizeof(frame);

		CShm *psmh=pShmMgr->GetShm(GetStreamHashIndex2(1));
		if(psmh!=NULL)
		{
			if(psmh->LibShm_WriteFrame( &tmpBuffer[0], MEDIA_VIDEO, VIDEO_P_FRAME, iTotalSize) < 0 )
				printf("Can not put stream o memory");
		}
	}

	return 0;
}
static UINT WINAPI ThreadTarget2(LPVOID pvParam)
{

	while(1)
	{	
		if(pShmMgr!=NULL)
		{
			int clientHandle=pShmMgr->GetClientHandle(GetStreamHashIndex2(1));

			char *tmpBuffer;
			CShm *psmh=pShmMgr->GetShm(GetStreamHashIndex2(1));
			if(psmh!=NULL)
			{
				tmpBuffer=psmh->LibShm_GetNextFrame(clientHandle);
				char * pcBuffer;

				if( tmpBuffer )
				{
					DVR_SM_HEADER		*pSmHeader;
					pSmHeader = (DVR_SM_HEADER *)&tmpBuffer[0];
					pcBuffer = tmpBuffer+sizeof(DVR_SM_HEADER);
				}	
			}
		}

		Sleep(10);
	}

	return 0;
}
int _tmain(int argc, _TCHAR* argv[])
{
	pShmMgr=new ShmMgr();

	//hashIdx=hashGen.Generate("00:0f:3a:02:63:33","1");



	ThreadInfo *threadInfo=new ThreadInfo;
	threadInfo->exitflag=false;
	threadInfo->toggleCount = 0;

	threadInfo->hThread= (HANDLE) _beginthreadex(0,0,Thread1,(LPVOID)threadInfo,0,  (unsigned *) &threadInfo->aThreadID);
	Sleep(100);
	ThreadInfo *threadInfo2=new ThreadInfo;
	threadInfo2->exitflag=false;
	threadInfo2->toggleCount = 0;

	threadInfo2->hThread= (HANDLE) _beginthreadex(0,0,Thread2,(LPVOID)threadInfo2,0,  (unsigned *) &threadInfo2->aThreadID);


	ThreadInfo *threadInfo3=new ThreadInfo;
	threadInfo3->exitflag=false;
	threadInfo3->toggleCount = 0;

	threadInfo3->hThread= (HANDLE) _beginthreadex(0,0,Thread3,(LPVOID)threadInfo3,0,  (unsigned *) &threadInfo3->aThreadID);

	////////////
	///////////
	ThreadInfo *threadInfo4=new ThreadInfo;
	threadInfo4->exitflag=false;
	threadInfo4->toggleCount = 0;

	threadInfo4->hThread= (HANDLE) _beginthreadex(0,0,ThreadSrc2,(LPVOID)threadInfo4,0,  (unsigned *) &threadInfo4->aThreadID);


	ThreadInfo *threadInfo5=new ThreadInfo;
	threadInfo5->exitflag=false;
	threadInfo5->toggleCount = 0;

	threadInfo5->hThread= (HANDLE) _beginthreadex(0,0,ThreadTarget2,(LPVOID)threadInfo5,0,  (unsigned *) &threadInfo5->aThreadID);


	//使程式不會結束
	while(true);
	return 0;
}

