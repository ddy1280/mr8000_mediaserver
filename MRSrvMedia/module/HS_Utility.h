#ifndef __HS_UTILITY_H__
#define __HS_UTILITY_H__

#include <iostream>


#include <windows.h>
#include <stdio.h>
#include <iomanip>
#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <time.h>
#include <sys/stat.h>
#include "StringUtil.h"

using namespace std;

string TimestampToString(unsigned long timeValue,bool isGMT);
unsigned long StringtoTime(char *str,bool isGMT);
unsigned int UTC2Local(long timeValue);
unsigned int Local2UTC(long timeValue);
bool DirIsExist(std::wstring Path);


#endif