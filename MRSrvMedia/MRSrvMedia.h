
#ifdef MEDIASERVER_EXPORTS
#define MEDIASERVER_API __declspec(dllexport)
#else
#define MEDIASERVER_API __declspec(dllimport)
#endif

extern "C"
{
typedef const char* (*EXT_PROCESS_HANDLER)(const char *Command,const char *Param);
typedef enum MR_MEDIA_STATUS
{
	MR_MEDIA_Success=0,
	MR_MEDIA_ServerNotStart=-1,
	MR_MEDIA_ServerAlreadyRun=-2,

};

MEDIASERVER_API MR_MEDIA_STATUS MR_MediaSvr_Start(const char *ServerIP, const char *port,int thread_num);
MEDIASERVER_API MR_MEDIA_STATUS MR_MediaSvr_Stop(void);
MEDIASERVER_API MR_MEDIA_STATUS MR_MediaSvr2_Start(const char *ServerIP, const char *port,int thread_num);
MEDIASERVER_API MR_MEDIA_STATUS MR_MediaSvr2_Stop(void);
MEDIASERVER_API MR_MEDIA_STATUS MR_MediaSvr_SetParentServerIP(const char *ServerIP, const char *port);
MEDIASERVER_API MR_MEDIA_STATUS MR_MediaSvr_SetServerInfo(const char *serverName, const char* version);
MEDIASERVER_API MR_MEDIA_STATUS MR_MediaSvr_SetStreamSource(int Mode);
MEDIASERVER_API MR_MEDIA_STATUS MR_MediaSvr_SetJpegPath(wchar_t *Path);

};