// syslogserver.cpp : 定義 DLL 應用程式的進入點。
//

#include "MRSrvMedia.h"
#include "Protocol_Media.h"
#include "Global.h"
#include "String/StringUtility.h"
//#include "HTTPServer/HttpServer.h"
#include "HTTPServerEX/HttpServerEx.h"
#include <fstream>
#include "ShmMgr.h"
#include "LibRTSPServer.h"
#include "CStreamer.h"
#include "CRtspSession.h"
#include <direct.h>
#include <io.h>
#include <fcntl.h>
#include <sstream>

#pragma comment (lib,"String.lib")
#pragma comment (lib,"HTTPServerEx.lib")
//#pragma comment (lib,"HTTPServer.lib")
#pragma comment (lib,"LibRTSPServer.lib")


#ifdef _MANAGED
#pragma managed(push, off)
#endif

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
    return TRUE;
}

#ifdef _MANAGED
#pragma managed(pop)
#endif


using namespace HS::Net;
using namespace HS::String;


HttpServer httpd;
CRTSPServer rtspd;
ShmMgr *qMgr;
CMediaProtocol mProtocol;

#define SERVER_TYPE "MEDIA_SERVER"

void HttpServerHandler(const request& req, reply& rep,void* param);
void HttpUpdateHandler();
//int HttpStatusHandler(SOCKET s);
int RtspSessionHandler(const RTSP_SESSION_HANDLE rtspHandle,const RTSP_STREAMER_HANDLE ssHandle);


#ifdef _CLIENT_SERVICE
HttpServer httpd2;
void HttpUpdateHandler2();
void HttpServerHandler2(const request& req, reply& rep,void* param);
#endif

MEDIASERVER_API MR_MEDIA_STATUS MR_MediaSvr_Start(const char *ServerIP, const char *port,int thread_num)//, EXT_PROCESS_HANDLER extproc)//,MR_EXT_HANDLER_MODE extHandleMode)
{
	//mProtocol.SetExternalHandler((MR_IN_HANDLER_MODE) extHandleMode,(IN_PROCESS_HANDLER) extproc);

	httpd.SetHttpRequestHandler(HttpServerHandler,NULL); //Protocol 實作介面
	httpd.SetHttpUpdateHandler(HttpUpdateHandler,NULL);  //Server loop callback
	//ETHAN:暫時先mark
//	httpd.SetHttpStatusHandler(HttpStatusHandler,NULL);


	httpd.Listen(ToInt(port),thread_num,NONBLOCKING);
	httpd.SetServerInformation(SERVER_TYPE);
	httpd.Start();


	rtspd.SetRtspSessionHandler(RtspSessionHandler);
	rtspd.Start();

	mProtocol.SetServerPort(ToInt(port));

	qMgr=new ShmMgr;
	mProtocol.InitialShmMgr(qMgr);

	if (httpd.GetStatus() /*&& httpd2.GetStatus()*/)
		return MR_MEDIA_Success;
	else
		return MR_MEDIA_ServerAlreadyRun;

}
#ifdef _CLIENT_SERVICE
MEDIASERVER_API MR_MEDIA_STATUS MR_MediaSvr2_Start(const char *ServerIP, const char *port,int thread_num)//, EXT_PROCESS_HANDLER extproc)//,MR_EXT_HANDLER_MODE extHandleMode)
{
	//mProtocol.SetExternalHandler((MR_IN_HANDLER_MODE) extHandleMode,(IN_PROCESS_HANDLER) extproc);
	httpd2.SetHttpRequestHandler(HttpServerHandler2,NULL); //Protocol 實作介面
	httpd2.SetHttpUpdateHandler(HttpUpdateHandler2,NULL);  //Server loop callback
	httpd2.Listen(ToInt(port),thread_num,NONBLOCKING);
	httpd2.SetServerInformation(SERVER_TYPE);
	httpd2.Start();

	if (httpd2.GetStatus())
		return MR_MEDIA_Success;
	else
		return MR_MEDIA_ServerAlreadyRun;
}
#endif

MEDIASERVER_API MR_MEDIA_STATUS MR_MediaSvr_SetServerInfo(const char *serverName, const char* version)
{
	std::string Name(serverName);
	if (Name=="") Name = "Media Server";
	mProtocol.SetServerInformation(SERVER_TYPE,Name,version);
	return MR_MEDIA_Success;
}
MEDIASERVER_API MR_MEDIA_STATUS MR_MediaSvr_SetStreamSource(int Mode)
{
	mProtocol.SetStreamSource(Mode);
	return MR_MEDIA_Success;
}
MEDIASERVER_API MR_MEDIA_STATUS MR_MediaSvr_SetJpegPath(wchar_t *Path)
{
	mProtocol.SetJpegPath(Path);
	return MR_MEDIA_Success;
}

MEDIASERVER_API MR_MEDIA_STATUS MR_MediaSvr_Stop(void)
{
  if (httpd.GetStatus())
	{

		httpd.DeleteHttpRequestHandler();
		httpd.evWaitcallback->WaitFor(10000);
		httpd.Stop();
		rtspd.Stop();	
		Sleep(1000);
		delete qMgr;
		qMgr=NULL;
		return MR_MEDIA_Success;
	}
	else
		return MR_MEDIA_ServerNotStart;
}			

#ifdef _CLIENT_SERVICE
MEDIASERVER_API MR_MEDIA_STATUS MR_MediaSvr2_Stop(void)
{
	if (httpd2.GetStatus())
	{
		httpd2.Stop();
		return MR_MEDIA_Success;
	}
	else
		return MR_MEDIA_ServerNotStart;
}	
#endif

MEDIASERVER_API MR_MEDIA_STATUS MR_MediaSvr_SetParentServerIP(const char *ServerIP, const char *port)
{
	mProtocol.SetParentServerIP(ServerIP,port);
	return MR_MEDIA_Success;
}

void HttpServerHandler(const request& req, reply& rep,void* param)
{
	httpd.evWaitcallback->ResetEvent();
	std::string repStr; 
	std::string reqRaw;

	//std::string ReqParam;
	//if (ToUpper(req.method) =="GET")
	//{
	//	ReqParam = ReplaceString(req.post_data,"&","\r\n");

	//}
	//else
	//{
	//    ReqParam = req.post_data;
	//}

	//Entry point
	repStr = mProtocol.ProtocolImplement(req,rep);
	if (mProtocol.ExternalHandler().ExtProc)
	{
		//處理後的資料餵給上級處理
		repStr = mProtocol.ExternalHandler().ExtProc(req.cmd.c_str(),repStr.c_str());
		//else
		//	repStr = mProtocol.ExternalHandler().ExtProc(req.cmd.c_str(),ReqParam.c_str());
	}


	if ((repStr == reply::replyString(reply::ok)) || (repStr ==""))
		rep = reply::stock_reply(reply::ok);
	else if (repStr == reply::replyString(reply::no_content))
		rep = reply::stock_reply(reply::no_content);
	else if (repStr == reply::replyString(reply::not_implemented))
		rep = reply::stock_reply(reply::not_implemented);
	else
		reply::stock_reply(rep,repStr);	
	httpd.evWaitcallback->SetEvent();

}

void HttpUpdateHandler()
{
    mProtocol.Update();
}
//int HttpStatusHandler(SOCKET s)
//{
//	return mProtocol.GetConnectStatus(s);
//}


#ifdef _CLIENT_SERVICE
void HttpServerHandler2(const request& req, reply& rep,void* param)
{
	std::string repStr; 
	std::string reqRaw;

	//Entry point
	repStr = mProtocol.ProtocolImplement2(req,rep);
	if (mProtocol.ExternalHandler().ExtProc)
	{
		//處理後的資料餵給上級處理
		repStr = mProtocol.ExternalHandler().ExtProc(req.cmd.c_str(),repStr.c_str());
	}

	if(repStr.size()>250)
		reply::stock_reply(rep,repStr,"image/jpeg");	

	if ((repStr == reply::replyString(reply::ok)) || (repStr ==""))
		rep = reply::stock_reply(reply::ok);
	else if (repStr == reply::replyString(reply::no_content))
		rep = reply::stock_reply(reply::no_content);
	else if (repStr == reply::replyString(reply::not_implemented))
		rep = reply::stock_reply(reply::not_implemented);
	else
		reply::stock_reply(rep,repStr);	
}
void HttpUpdateHandler2()
{
	mProtocol.Update();
}

#endif


int RtspSessionHandler(const RTSP_SESSION_HANDLE rtspHandle,const RTSP_STREAMER_HANDLE ssHandle)
{
	CRtspSession *rtspSession = (CRtspSession*)	rtspHandle;
	CStreamer *streamer = (CStreamer*) ssHandle;
	int ret=0;
					  

	ConnectStatus connStatus =(ConnectStatus) mProtocol.AddRequest(rtspSession->GetRID(),
									rtspSession->GetStreamType(),
									(int)rtspSession->GetStreamID(),
									rtspSession->GetChannelID(),
									rtspSession->GetPlayTimeUTC(),
									rtspSession->GetSessionID());
	while (connStatus==CS_SUCCESS)						
	{
		Sleep(33);
		if (rtspSession->RtspState() == RTSP_PLAY)
		{
			std::string clientSN = mProtocol.GetClientSN(rtspSession->GetRID(),
				rtspSession->GetStreamType(),
				(int)rtspSession->GetStreamID(),
				rtspSession->GetChannelID(),
				rtspSession->GetPlayTimeUTC(),
				rtspSession->GetSessionID());

			

			if (!qMgr) continue;


			char * tmpBuffer=qMgr->GetNextFrame(clientSN);
			if(!tmpBuffer ) 
			{
				printf("######(1)[%d] NO DATA\n",rtspSession->GetSessionID());
				continue;
			}

			DVR_SM_HEADER *pSmHeader;
			char *pcBuffer;
			pSmHeader = (DVR_SM_HEADER *)&tmpBuffer[0];
			pcBuffer = tmpBuffer+sizeof(DVR_SM_HEADER);

			if (pSmHeader && pcBuffer)
			{
				//printf("######[%d] Already GetFrame\n",rtspSession->GetSessionID());
				int rtpChannel = rtspSession->GetInterleavePorts((STREAM_ID_TYPE)pSmHeader->iStreamType).interleave_rtp_port;

				//for test
				if (pSmHeader->iStreamType==SIT_AUDIO) continue;   //聲音先不傳送，日後再確認

				ret = streamer->SendRTPPacket(pSmHeader->iStreamType,(char*) pcBuffer,pSmHeader->iDataSize,rtpChannel);
				//printf("######[%d] Send Finished\n",rtspSession->GetSessionID());

				if (ret == -2)		
				{
					mProtocol.ReleaseRequest(rtspSession->GetRID(),
						rtspSession->GetStreamType(),
						(int)rtspSession->GetStreamID(),
						rtspSession->GetChannelID(),
						rtspSession->GetPlayTimeUTC(),
						rtspSession->GetSessionID());
					return -1;
				}
			}
			else
			{
				//printf("(2)######[%d] NO DATA\n",rtspSession->GetSessionID());
			}
		}
		else
		{
			//printf("######[%d] Not Play\n",rtspSession->GetSessionID());
		}
		
	}
    return 0;
}
