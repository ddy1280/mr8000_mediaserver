#ifndef CLIENT_SERVICE_H
#define CLIENT_SERVICE_H

#include <Windows.h>
#include <iostream>
#include <queue>
#include "HttpClient/HttpClient.h"

using namespace std;


template <class T>
class CQueue
{
private:
	std::queue <T> q;
	bool isPointer;
public:
	CQueue()
	{
		if (strstr(typeid(q).name(),"*")>0)  //判斷是否為指標型態
			isPointer=true;
		else
			isPointer=false;

	}
	~CQueue()
	{
	}
	void Put (T value)
	{
		q.push(value);
	};
	T Get()
	{
		if (q.empty()) return NULL;
		T ret= q.front();
		q.pop();
		return ret;
	};
	bool Empty()
	{
		return q.empty();
	}
	size_t Size()
	{
		return q.size();
	};
	virtual void Clear()
	{
		while(!q.empty())
		{
			T tmp= Get();
			if (isPointer)
			{
				if (FreeFunction)
					FreeFunction(tmp);
				delete (void*)tmp;
			}
		}
		Empty();
	};
	void (*FreeFunction)(T Node);
} ;



class CClientService
{
private:
	CQueue <char*> m_cmd_queue;
	CRITICAL_SECTION lock; 
	int m_clientInterval; //向server 主動回覆訊息的間隔時間，預設為60s
	string m_autoUpdateCommand; //自動回覆訊息的命令
	//Thread
	HANDLE hThread_client;
	unsigned int ThreadID_client;
	bool m_bExitClientThread;
	static UINT __stdcall ClientServiceThreadFunc(LPVOID pvParam);
public:
	CClientService();
	~CClientService();
	std::string Response;
	void PostCommand(string url_command);
	string SendCommand(string url_command);

	void SetAutoUpdateCommand(string url_command,int interval);


};


#endif
