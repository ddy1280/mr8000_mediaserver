
#ifndef __CCONSOLEDEBUG_H
#define __CCONSOLEDEBUG_H
#include   <windows.h>
#include   <stdio.h>
enum TOutputMode {OUTPUT_CONSOLE=0,OUTPUT_FILE=1} ;
class CConsoleDebug
{
    private:
        HANDLE m_StdOut;
        FILE *fp;

        void ConsoleBufSize(short int cols, short int rows)
        {
            COORD cordinates = {cols, rows};
            HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
            SetConsoleScreenBufferSize(handle, cordinates);
        }

    public:

        CConsoleDebug()
        {
            m_StdOut = NULL;
        }

        ~CConsoleDebug()
        {
            DestoryConsole();
        }

        void InitConsole(int cols=80,int rows=24,int maxrows=0,TOutputMode OutputMode=OUTPUT_CONSOLE)
        {
            HWND hwndFound;
            wchar_t pszNewWindowTitle[]=L"DEBUG CONSOLE";
            wchar_t pszTmpStr[32];

            if (m_StdOut) return;
            if (OutputMode == OUTPUT_CONSOLE)
            {
                AllocConsole();
                freopen("conin$", "r+t", stdin);
                freopen("conout$", "w+t", stdout);
                freopen("conout$", "w+t", stderr);
            }
            else if (OutputMode == OUTPUT_FILE)
            {
                freopen("DEBUG_CONSOLE_STDOUT.TXT", "w+t", stdout);
                freopen("DEBUG_CONSOLE_STDERR.TXT", "w+t", stderr);
            }

            m_StdOut = GetStdHandle(STD_OUTPUT_HANDLE);

            // fetch current window title
            wsprintf(pszTmpStr,L"%d-%d", ::GetTickCount(), GetCurrentProcessId());    // change current window title
            SetConsoleTitle(pszTmpStr);    // ensure window title has been updated
            ::Sleep(40);    // look for NewWindowTitle
            hwndFound = FindWindow(NULL, pszTmpStr);

            SetConsoleTitle(pszNewWindowTitle);

            RemoveMenu(GetSystemMenu(hwndFound, FALSE), SC_CLOSE, MF_BYCOMMAND);
            ConsoleBufSize(cols,maxrows);

            COORD size = {cols, rows};
            SetConsoleScreenBufferSize(m_StdOut,size);

            SMALL_RECT rc = {0,0, cols-1, rows-1};
            SetConsoleWindowInfo(m_StdOut,true ,&rc);
        }

        void DestoryConsole(void)
        {
            m_StdOut = NULL;
            fclose(stderr);
            fclose(stdout);
            fclose(stdin);
            FreeConsole();
        }
};

#endif //__CCONSOLEDEBUG_H
