#include <process.h>
#include <boost/timer/timer.hpp>  
#include "ClientService.h"
#include "HttpClient/HttpClient.h"

#pragma comment (lib, "HttpClient.lib")
					   

using namespace HS::Net;
HttpClient httpc;
CClientService::CClientService()
{
	InitializeCriticalSection(&lock);
	m_bExitClientThread = false;
	m_autoUpdateCommand="";
	m_clientInterval = 60;
	hThread_client = (HANDLE) _beginthreadex(NULL, 0, CClientService::ClientServiceThreadFunc, (LPVOID) this, 0, &ThreadID_client);
}

CClientService::~CClientService()
{
	DeleteCriticalSection(&lock);
	m_bExitClientThread=true;
	if (hThread_client)
	{
		::CloseHandle(hThread_client);
		hThread_client = NULL;
	}	
}

//此Thread 負責定時向Register Server 註冊，並可依命令傳送相關資料
UINT __stdcall CClientService::ClientServiceThreadFunc(LPVOID pvParam)
{
	//std::cerr << "ClientServiceThreadFunc thread start \r\n";
	CClientService *obj = (CClientService*)pvParam;
	try
	{
		__int64 elapsedInterval;
		boost::timer::cpu_timer timer;  
		timer.start();
		elapsedInterval = timer.elapsed().wall;
		
		while (!obj->m_bExitClientThread)
		{
			if (!obj->m_cmd_queue.Empty())
			{
				char *cmd = (char*)obj->m_cmd_queue.Get();

				HttpClient::Reply_S reply;
				reply = httpc.SendRequest(HttpClient::HTTP_POST, cmd);

			}


			if (timer.elapsed().wall - elapsedInterval > obj->m_clientInterval*1000000000.0L)	//second
			{
				if (obj->m_autoUpdateCommand.length()>0)
				{
					obj->Response = obj->SendCommand(obj->m_autoUpdateCommand);
				}
				
				elapsedInterval = timer.elapsed().wall;
			}
			Sleep(100);

		}

		timer.stop();
	}
	catch (std::exception& e)
	{
		std::cerr << "exception: " << e.what() << "\n";
		return -1;
	}
	//std::cerr << "ClientServiceThreadFunc thread end \r\n";
	return 0;
}

void CClientService::PostCommand(string url_command)
{
	try
	{
		char *cmdbuf = new char[url_command.length()+1];
		memset(cmdbuf,0,url_command.length()+1);
		strncpy(cmdbuf,url_command.c_str(),url_command.length());
		m_cmd_queue.Put(cmdbuf);
	}
	catch (std::exception& e)
	{
	}
}

string CClientService::SendCommand(string url_command)
{
	//EnterCriticalSection(&lock);
	std::string ret;
	try
	{		
		HttpClient::Reply_S reply;
		
		reply = httpc.SendRequest(HttpClient::HTTP_POST, url_command);
		if (reply.content_length ==0 && reply.status_code == 200)
			ret = "OK";
		else
			ret = reply.response_text;
		
	}
	catch (std::exception& e)
	{
		ret = "FAIL";
	}
	//LeaveCriticalSection(&lock);
	return ret;

}


void CClientService::SetAutoUpdateCommand(string url_command,int interval)
{
	m_clientInterval = interval;
	m_autoUpdateCommand = url_command;
}


