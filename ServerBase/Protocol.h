#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <boost/timer/timer.hpp>
#include "HTTPServer/request.h"
#include "HTTPServer/reply.h"
#include "HTTPServer/CLock.h"
#include "ClientService.h"



#include <map>
#include <string>

using namespace HS::Net;

//因為InnerDeviceProtocol()的query 可輸出給網頁查詢及函式輸出，兩者對於編碼處理機制不同，必須分別處理 
//網頁部份會由瀏覽器解碼，所以輸出為編碼後的UTF8字串即可，但程式部份因為BSTR的轉換造成編碼錯亂，需以BASE64保護資料
//故此以UNICODE_MODE_ENUM區分是由何者呼叫 (只有在無外部callback 函式時才處理)
typedef enum UNICODE_MODE_ENUM 
{
	UNICODE_DATA_NORMAL = 0,
	UNICODE_DATA_ENCODE,
}UNICODE_MODE_ENUM;

#define FIELD_SPLITTER	","
#define	END_CRLF		"\r\n"


typedef const char* (*IN_PROCESS_HANDLER)(const char *Command,const char *Param);
typedef enum MR_IN_HANDLER_MODE
{
	SERVER_ENHANCE=0,    //Enhance server function 
	SERVER_REPLACE,      //Replace server function
};

struct ExtProcessMgr
{
	MR_IN_HANDLER_MODE ExtHandlerMode;
	IN_PROCESS_HANDLER ExtProc;
};

class CProtocol
{
private:

	__int64 elapsedInterval;
	bool m_bEncryptionMode;
	std::string m_ServerType;
	std::string m_ServerName;
	std::string m_Version;
	int m_ServerPort;

	ExtProcessMgr mHandleMgr; 
	CRITICAL_SECTION lock; 
protected:
	boost::timer::cpu_timer timer;  

	int mServerTimeout;

	CClientService *ReporterClient;
	std::string mParentServerIP;
	std::string mParentServerPort;

	virtual void ServerJoin(std::string myServerIP,int myServerPort);
	virtual void ServerLeave(void);

public:
	CProtocol();
	~CProtocol();
	
	void SetServerInformation(std::string ServerType,std::string ServerName,std::string Version);
	bool SetParentServerIP(std::string parentServer,std::string port);
	void SetExternalHandler(MR_IN_HANDLER_MODE mode,IN_PROCESS_HANDLER handler){mHandleMgr.ExtHandlerMode = mode;mHandleMgr.ExtProc = handler;};
	virtual std::string ProtocolImplement(const request& req, reply& rep) = 0;   //abstract class member function
	virtual std::string ProtocolPostProcessing(std::string command,std::string param)= 0;   //abstract class member function
	virtual void Update(bool ImmediatelyFlag=false);
	ExtProcessMgr ExternalHandler(){return mHandleMgr;};
	void SetServerPort(int value){m_ServerPort = value;}; 
    int GetServerPort(){return m_ServerPort;}; 

};


#endif
