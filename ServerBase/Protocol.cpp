#include "Protocol.h"
#include "Global.h"
#include "String/StringUtility.h"
#include "NetUtility/NetUtility.h"

#pragma comment (lib,"String.lib")
#pragma comment (lib,"NetUtility.lib")

using namespace HS::String;

//===============================================================================
//Protocol Name: Register Protocol
//
//===============================================================================

CProtocol::CProtocol()
{
	ReporterClient = new CClientService();
	mServerTimeout = 60;
	InitializeCriticalSection(&lock);

	timer.start();
	elapsedInterval = timer.elapsed().wall;

}

CProtocol::~CProtocol()
{
	ServerLeave();
	timer.stop();
	DeleteCriticalSection(&lock);
	delete ReporterClient;
}


bool CProtocol::SetParentServerIP(std::string parentServer,std::string port)
{
	mParentServerIP = parentServer;
	mParentServerPort = port;

	//check Register alive?
	if (mParentServerIP.length()>0 && mParentServerPort.length()>0)
	{
		//?? 尚未加入檢查Parent server 是否為Register Server ?? 只有 Register 才能註冊
	    Update(true);
		return true;

	}
	return false;
}


void CProtocol::SetServerInformation(std::string ServerType,std::string ServerName,std::string Version)
{
	m_ServerType = ServerType;
	m_ServerName = ServerName;
	m_Version = Version;
}
			
void CProtocol::ServerJoin(std::string myServerIP,int myServerPort)
{
	if (m_ServerType.length()==0) return;

	//在SeverJoin 時讀取Server timeout 時間，這樣子若Regiater 註冊時間有變更，能夠隨時得知
	std::string url = "http://"+ mParentServerIP + ":" + mParentServerPort +"/READ_CONFIG";
	std::string repStr = ReporterClient->SendCommand(url);
	if (repStr.find("REG_TIMEOUT")!=std::string::npos)
	{
		std::string x = GetValueByKeyFromKeyPair(repStr,"REG_TIMEOUT");
		mServerTimeout = ToInt(x);
	}
	else
		mServerTimeout = 60;
	
	url = "http://"+ mParentServerIP + ":" + mParentServerPort +"/SERVER_JOIN?SERVER_TYPE="+m_ServerType+"&IP_ADDRESS="+myServerIP+"&PORT="+ToString(myServerPort)+"&VERSION="+m_Version;
	ReporterClient->SendCommand(url);
}

void CProtocol::ServerLeave(void)
{
	if (m_ServerType.length()==0) return;

	std::string url = "http://"+ mParentServerIP + ":" + mParentServerPort +"/SERVER_LEAVE?SERVER_TYPE="+m_ServerType;
	ReporterClient->SendCommand(url);
}

void CProtocol::Update(bool ImmediatelyFlag)
{
	//EnterCriticalSection(&lock);
	if (ImmediatelyFlag)
	{
		if (!HS::Net::CheckPort(mParentServerIP.c_str(), mParentServerPort.c_str())) return;

		//Alive
		std::string url = "http://"+ mParentServerIP + ":" + mParentServerPort +"/SERVER_ALIVE?SERVER_TYPE="+m_ServerType;
		std::string repstr = ReporterClient->SendCommand(url);
			if (repstr != reply::replyString(reply::ok))
		{//若失敗則重新Join
			ServerJoin(HS::Net::GetLocalIPAddress(),m_ServerPort);
		}
		elapsedInterval = timer.elapsed().wall;
	}
	else
	{
		int timeout = mServerTimeout>5?mServerTimeout-5:1; //要比Server 檢查時還早註冊
		if (timer.elapsed().wall - elapsedInterval > (__int64)timeout*1000000000L)	//second
		{
 			if (!HS::Net::CheckPort(mParentServerIP.c_str(), mParentServerPort.c_str())) return;

			//Alive
			std::string url = "http://"+ mParentServerIP + ":" + mParentServerPort +"/SERVER_ALIVE?SERVER_TYPE="+m_ServerType;
			std::string repstr = ReporterClient->SendCommand(url);
			if (repstr != "OK")
			{//若失敗則重新Join
				ServerJoin(HS::Net::GetLocalIPAddress(),m_ServerPort);
			}


			elapsedInterval = timer.elapsed().wall;
		}


	}
	//LeaveCriticalSection(&lock);
}

