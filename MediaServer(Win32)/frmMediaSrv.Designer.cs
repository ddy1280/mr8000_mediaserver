﻿namespace MediaServer
{
    partial class frmMediaSrv
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該公開 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
        ///
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMediaSrv));
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mmuStart = new System.Windows.Forms.ToolStripMenuItem();
            this.mmuStop = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ssMsg = new System.Windows.Forms.ToolStripStatusLabel();
            this.ssModuleVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.grpSrvSetting = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.edPath = new System.Windows.Forms.TextBox();
            this.btnTest = new System.Windows.Forms.Button();
            this.edParentServerPort = new System.Windows.Forms.TextBox();
            this.lblParentServerPort = new System.Windows.Forms.Label();
            this.edParentServerIP = new System.Windows.Forms.TextBox();
            this.lblParentServerIP = new System.Windows.Forms.Label();
            this.lblPort = new System.Windows.Forms.Label();
            this.chkAuto = new System.Windows.Forms.CheckBox();
            this.edPort = new System.Windows.Forms.TextBox();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.checkJPEG = new System.Windows.Forms.CheckBox();
            this.contextMenuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.grpSrvSetting.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mmuStart,
            this.mmuStop});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(104, 48);
            // 
            // mmuStart
            // 
            this.mmuStart.Name = "mmuStart";
            this.mmuStart.Size = new System.Drawing.Size(103, 22);
            this.mmuStart.Text = "Start";
            // 
            // mmuStop
            // 
            this.mmuStop.Name = "mmuStop";
            this.mmuStop.Size = new System.Drawing.Size(103, 22);
            this.mmuStop.Text = "Stop";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ssMsg,
            this.ssModuleVersion});
            this.statusStrip1.Location = new System.Drawing.Point(0, 283);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(575, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.Stretch = false;
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // ssMsg
            // 
            this.ssMsg.Name = "ssMsg";
            this.ssMsg.Size = new System.Drawing.Size(465, 17);
            this.ssMsg.Spring = true;
            this.ssMsg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ssModuleVersion
            // 
            this.ssModuleVersion.Name = "ssModuleVersion";
            this.ssModuleVersion.Size = new System.Drawing.Size(95, 17);
            this.ssModuleVersion.Text = "ModuleVersion";
            // 
            // grpSrvSetting
            // 
            this.grpSrvSetting.Controls.Add(this.groupBox1);
            this.grpSrvSetting.Controls.Add(this.btnTest);
            this.grpSrvSetting.Controls.Add(this.edParentServerPort);
            this.grpSrvSetting.Controls.Add(this.lblParentServerPort);
            this.grpSrvSetting.Controls.Add(this.edParentServerIP);
            this.grpSrvSetting.Controls.Add(this.lblParentServerIP);
            this.grpSrvSetting.Controls.Add(this.lblPort);
            this.grpSrvSetting.Controls.Add(this.chkAuto);
            this.grpSrvSetting.Controls.Add(this.edPort);
            this.grpSrvSetting.Location = new System.Drawing.Point(12, 12);
            this.grpSrvSetting.Name = "grpSrvSetting";
            this.grpSrvSetting.Size = new System.Drawing.Size(470, 244);
            this.grpSrvSetting.TabIndex = 21;
            this.grpSrvSetting.TabStop = false;
            this.grpSrvSetting.Text = "Server Setting";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkJPEG);
            this.groupBox1.Controls.Add(this.btnBrowse);
            this.groupBox1.Controls.Add(this.edPath);
            this.groupBox1.Location = new System.Drawing.Point(141, 37);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(305, 108);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Stream Source";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(230, 70);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(51, 22);
            this.btnBrowse.TabIndex = 20;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // edPath
            // 
            this.edPath.Location = new System.Drawing.Point(17, 70);
            this.edPath.Name = "edPath";
            this.edPath.Size = new System.Drawing.Size(207, 22);
            this.edPath.TabIndex = 19;
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(225, 154);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(75, 22);
            this.btnTest.TabIndex = 14;
            this.btnTest.Text = "Test";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // edParentServerPort
            // 
            this.edParentServerPort.Location = new System.Drawing.Point(169, 154);
            this.edParentServerPort.Name = "edParentServerPort";
            this.edParentServerPort.Size = new System.Drawing.Size(50, 22);
            this.edParentServerPort.TabIndex = 13;
            // 
            // lblParentServerPort
            // 
            this.lblParentServerPort.AutoSize = true;
            this.lblParentServerPort.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblParentServerPort.Location = new System.Drawing.Point(160, 132);
            this.lblParentServerPort.Name = "lblParentServerPort";
            this.lblParentServerPort.Size = new System.Drawing.Size(24, 12);
            this.lblParentServerPort.TabIndex = 12;
            this.lblParentServerPort.Text = "Port";
            // 
            // edParentServerIP
            // 
            this.edParentServerIP.Location = new System.Drawing.Point(25, 154);
            this.edParentServerIP.Name = "edParentServerIP";
            this.edParentServerIP.Size = new System.Drawing.Size(138, 22);
            this.edParentServerIP.TabIndex = 11;
            // 
            // lblParentServerIP
            // 
            this.lblParentServerIP.AutoSize = true;
            this.lblParentServerIP.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblParentServerIP.Location = new System.Drawing.Point(16, 133);
            this.lblParentServerIP.Name = "lblParentServerIP";
            this.lblParentServerIP.Size = new System.Drawing.Size(80, 12);
            this.lblParentServerIP.TabIndex = 10;
            this.lblParentServerIP.Text = "Parent Server IP";
            // 
            // lblPort
            // 
            this.lblPort.AutoSize = true;
            this.lblPort.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblPort.Location = new System.Drawing.Point(16, 37);
            this.lblPort.Name = "lblPort";
            this.lblPort.Size = new System.Drawing.Size(61, 12);
            this.lblPort.TabIndex = 0;
            this.lblPort.Text = "Service Port";
            // 
            // chkAuto
            // 
            this.chkAuto.AutoSize = true;
            this.chkAuto.Checked = true;
            this.chkAuto.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAuto.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.chkAuto.Location = new System.Drawing.Point(25, 192);
            this.chkAuto.Name = "chkAuto";
            this.chkAuto.Size = new System.Drawing.Size(137, 16);
            this.chkAuto.TabIndex = 4;
            this.chkAuto.Text = "Auto run on system start";
            this.chkAuto.UseVisualStyleBackColor = true;
            this.chkAuto.CheckedChanged += new System.EventHandler(this.chkAuto_CheckedChanged);
            // 
            // edPort
            // 
            this.edPort.Location = new System.Drawing.Point(18, 52);
            this.edPort.Name = "edPort";
            this.edPort.Size = new System.Drawing.Size(100, 22);
            this.edPort.TabIndex = 1;
            this.edPort.Text = "520";
            // 
            // btnStop
            // 
            this.btnStop.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnStop.Location = new System.Drawing.Point(488, 44);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 23;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnStart
            // 
            this.btnStart.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnStart.Location = new System.Drawing.Point(488, 15);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 22;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // checkJPEG
            // 
            this.checkJPEG.AutoSize = true;
            this.checkJPEG.Location = new System.Drawing.Point(17, 36);
            this.checkJPEG.Name = "checkJPEG";
            this.checkJPEG.Size = new System.Drawing.Size(78, 16);
            this.checkJPEG.TabIndex = 21;
            this.checkJPEG.Text = "Write JPEG";
            this.checkJPEG.UseVisualStyleBackColor = true;
            // 
            // frmMediaSrv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(575, 305);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.grpSrvSetting);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmMediaSrv";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Media Server";
            this.Load += new System.EventHandler(this.frmLogSrv_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmLogSrv_FormClosed);
            this.contextMenuStrip1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.grpSrvSetting.ResumeLayout(false);
            this.grpSrvSetting.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mmuStart;
        private System.Windows.Forms.ToolStripMenuItem mmuStop;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel ssMsg;
        private System.Windows.Forms.GroupBox grpSrvSetting;
        private System.Windows.Forms.TextBox edParentServerPort;
        private System.Windows.Forms.Label lblParentServerPort;
        private System.Windows.Forms.TextBox edParentServerIP;
        private System.Windows.Forms.Label lblParentServerIP;
        private System.Windows.Forms.Label lblPort;
        private System.Windows.Forms.CheckBox chkAuto;
        private System.Windows.Forms.TextBox edPort;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.ToolStripStatusLabel ssModuleVersion;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox edPath;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.CheckBox checkJPEG;
    }
}

