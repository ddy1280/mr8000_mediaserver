﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;  //for StructLayout

namespace MediaServerSDK
{
    public enum MR_MEDIA_STATUS
    {
        MR_LOG_Success = 0,
        MR_LOG_ServerNotStart = -1,
        MR_LOG_ServerAlreadyRun = -2,
    };
                                                 

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    unsafe public delegate string EXT_PROCESS_HANDLER(string Command, string Param);

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    unsafe public delegate string EXT_AUTH_HANDLER(string AuthData);

    public class MRMediaSrvClass
    {

        [DllImport("MRSrvMedia.dll")]
        public static extern MR_MEDIA_STATUS MR_MediaSvr_Start(string ServerIP, string Port, int thread_num);
        [DllImport("MRSrvMedia.dll")]
        public static extern MR_MEDIA_STATUS MR_MediaSvr_Stop();
        [DllImport("MRSrvMedia.dll")]
        public static extern MR_MEDIA_STATUS MR_MediaSvr2_Start(string ServerIP, string Port, int thread_num);
        [DllImport("MRSrvMedia.dll")]
        public static extern MR_MEDIA_STATUS MR_MediaSvr2_Stop();
        [DllImport("MRSrvMedia.dll")]
        public static extern MR_MEDIA_STATUS MR_MediaSvr_SetParentServerIP(string ServerIP, string port);
        [DllImport("MRSrvMedia.dll")]
        public static extern MR_MEDIA_STATUS MR_MediaSvr_SetServerInfo(string serverName, string version);
        [DllImport("MRSrvMedia.dll")]
        public static extern MR_MEDIA_STATUS MR_MediaSvr_SetStreamSource(int Mode);

        [DllImport("MRSrvMedia.dll", CharSet = CharSet.Unicode)]
        public static extern MR_MEDIA_STATUS MR_MediaSvr_SetJpegPath(string Path);

        
        //[DllImport("MRSrvMedia.dll")]
        //public static extern MR_MEDIA_STATUS MR_MediaSvr_SetLogPath([MarshalAs(UnmanagedType.LPWStr)] string logpath);
    }

};