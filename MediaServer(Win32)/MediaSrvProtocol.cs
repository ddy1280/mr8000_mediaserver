﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using HSCSLIB;
using System.Data.SqlClient;
using MediaServerSDK;
using BrightIdeasSoftware;
using System.Text.RegularExpressions;
using Gettext;

namespace MediaServer
{
    class MediaSrvProtocol
    {

        public static string LogProc_Callback(string Command, string Param)
        {
            if (Param.IndexOf("No Content") > 0) 
                return Param;

            #region LOG / PANIC Command
            if ((Command.ToUpper() == "/LOG") || (Command.ToUpper() == "/PANIC"))
            {

                CMessageData rdd = new CMessageData(Param.Replace("\r\n", ","));

                DateTime devTime = (new DateTime(1970, 1, 1, 0, 0, 0)).AddSeconds(Int32.Parse(rdd.UTC));
                StringBuilder sb = new StringBuilder();
                sb.Append("INSERT INTO LogMessage(RID,CARID,DeviceTime,Type,SubType,Severity,Description,FileList) ");
                sb.AppendFormat("VALUES ('{0}','{1}','{2:yyyy/MM/dd HH:mm:ss}',{3},{4},{5},'{6}','{7}')", rdd.RID, rdd.CARID, devTime, rdd.TYPE, rdd.SUBTYPE, rdd.SEVERITY, rdd.DESC, rdd.FILEINFO);

                try
                {
                    using (SqlConnection connExec = HSCSLIB.DB.ConnectDatabase())
                    {
                        SqlCommand cmd = new SqlCommand(sb.ToString(), connExec);
                        int row = cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }
                }
                catch (System.Exception ex)
                {
                    frmMediaSrv.thisForm.UpdateStatus(TT.T("Database Error: Unable to connect to the database! Please re-confirm. ") + DEBUG.__LINE__());
                }
            }
            #endregion

            #region PUSHDATA Command
            else if (Command.ToUpper() == "/PUSHDATA")
            {
                StringBuilder sb = new StringBuilder();
                CPushData rdd = new CPushData(Param.Replace("\r\n", ","));

                DateTime devTime = (new DateTime(1970, 1, 1, 0, 0, 0)).AddSeconds(Int32.Parse(rdd.UTC));


                sb.Append("INSERT INTO LogPushData(RID,CARID,DeviceTime,Sig,Fix,PDOP,HDOP,VDOP,");
                sb.Append("Longitude,Latitude,Altitude,Speed,Heading,");
                sb.Append("Declination,INVIEW,Satellites,INUSE,GDATA,TPMS_PSI,TPMS_TEMP,TPMS_PWR) ");

                sb.AppendFormat("VALUES ('{0}','{1}','{2:yyyy/MM/dd HH:mm:ss}',{3},{4},{5},{6},{7},", rdd.RID, rdd.CARID, devTime, rdd.SIG, rdd.FIX, rdd.PDOP, rdd.HDOP, rdd.VDOP);
                sb.AppendFormat("{0},{1},{2},{3},{4},", rdd.LONGITUDE, rdd.LATITUDE, rdd.ALTITUDE, rdd.SPEED, rdd.HEADING);
                sb.AppendFormat("{0},{1},'{2}',{3},'{4}',{5},{6},{7})", rdd.DECLINATION, rdd.INVIEW, rdd.SATELLITES, rdd.INUSE, rdd.GDATA, rdd.TPMS_PSI, rdd.TPMS_TEMP, rdd.TPMS_PWR);

                try
                {
                    using (SqlConnection connExec = HSCSLIB.DB.ConnectDatabase())
                    {
                        SqlCommand cmd = new SqlCommand(sb.ToString(), connExec);
                        if (cmd.Connection != null)
                        {
                            int row = cmd.ExecuteNonQuery();
                        }
                        cmd.Dispose();
                    }
                }
                catch (System.Exception ex)
                {
                    frmMediaSrv.thisForm.UpdateStatus(TT.T("Database Error: Unable to connect to the database! Please re-confirm. ") + DEBUG.__LINE__());
                }

            }
            #endregion

            #region READ_CONFIG Command
            else if (Command.ToUpper() == "/READ_CONFIG")
            {
                Dictionary<string, string> PostKeyPair = HSCSLIB.Strings.KeyPairDataToDictionary(Param, "\r\n");


                StringBuilder sb = new StringBuilder();
                StringBuilder sbRet = new StringBuilder();
                sb.Append("SELECT RID,PushType,JPG_CNT_MOTION,JPG_CNT_SENSOR,JPG_CNT_GSENSOR,JPG_CNT_PANIC FROM RegisterInfo WHERE 1=1 ");
                if (PostKeyPair.ContainsKey("RID"))
                {
                    sb.AppendFormat("AND RID='{0}'", PostKeyPair["RID"]);
                }

                try
                {
                    using (SqlConnection connExec = HSCSLIB.DB.ConnectDatabase())
                    {

                        SqlCommand cmd = new SqlCommand(sb.ToString(), connExec);
                        SqlDataReader dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            for (int fIdx = 0; fIdx < dr.FieldCount; fIdx++)
                            {
                                sbRet.AppendFormat("{0}={1}", dr.GetName(fIdx).Trim().ToUpper(), dr[dr.GetName(fIdx)].ToString().Trim());
                                if (fIdx < dr.FieldCount - 1) sbRet.Append(",");
                            }
                            sbRet.Append("\r\n");
                        }
                        dr.Close();
                        dr.Dispose();
                        cmd.Dispose();
                    }

                    //Update LogConfigChanged-------------------------------------- 
                    sb.Length = 0;
                    sb.Append("UPDATE RegisterInfo SET LogConfigChanged=0 WHERE 1=1");
                    if (PostKeyPair.ContainsKey("RID"))
                    {
                        sb.AppendFormat("AND RID='{0}'", PostKeyPair["RID"]);
                    }

                    try
                    {
                        using (SqlConnection connExec = HSCSLIB.DB.ConnectDatabase())
                        {
                            SqlCommand cmd = new SqlCommand(sb.ToString(), connExec);
                            int row = cmd.ExecuteNonQuery();
                            cmd.Dispose();
                        }
                    }
                    catch (System.Exception ex)
                    {
                        frmMediaSrv.thisForm.UpdateStatus(TT.T("Database Error: Unable to connect to the database! Please re-confirm. ") + DEBUG.__LINE__());
                    }
                    //--------------------------------------------------------------------------------


                    return sbRet.ToString();
                }
                catch (System.Exception ex)
                {



                    frmMediaSrv.thisForm.UpdateStatus(TT.T("Database Error: Unable to connect to the database! Please re-confirm. ") + DEBUG.__LINE__());
                }

            }
            #endregion

            #region WRITE_CONFIG Command
            else if (Command.ToUpper() == "/WRITE_CONFIG")
            {
                Dictionary<string, string> PostKeyPair = HSCSLIB.Strings.KeyPairDataToDictionary(Param, "\r\n");

                CMediaServerConfig rdd = new CMediaServerConfig(Param.Replace("\r\n", ","));

                StringBuilder sb = new StringBuilder();

                sb.AppendFormat("IF EXISTS (SELECT * FROM RegisterInfo WHERE 1=1 ");
                if (PostKeyPair.ContainsKey("RID"))
                {
                    sb.AppendFormat("AND RID='{0}'", rdd.RID);
                }
                sb.Append(") BEGIN ");
                sb.Append("UPDATE RegisterInfo SET ");
                if (PostKeyPair.ContainsKey("PUSHTYPE"))
                    sb.AppendFormat("PushType='{0}', ", rdd.PushType);
                if (PostKeyPair.ContainsKey("JPG_CNT_MOTION"))
                    sb.AppendFormat("JPG_CNT_MOTION={0}, ", rdd.JPG_CNT_MOTION);
                if (PostKeyPair.ContainsKey("JPG_CNT_SENSOR"))
                    sb.AppendFormat("JPG_CNT_SENSOR={0}, ", rdd.JPG_CNT_SENSOR);
                if (PostKeyPair.ContainsKey("JPG_CNT_GSENSOR"))
                    sb.AppendFormat("JPG_CNT_GSENSOR={0}, ", rdd.JPG_CNT_GSENSOR);
                if (PostKeyPair.ContainsKey("JPG_CNT_PANIC"))
                    sb.AppendFormat("JPG_CNT_PANIC={0}, ", rdd.JPG_CNT_PANIC);

                sb.Append("LogConfigChanged=1 WHERE 1=1 ");
                if (PostKeyPair.ContainsKey("RID"))
                {
                    sb.AppendFormat("AND RID='{0}'", rdd.RID);
                }
                sb.Append(" END");




                try
                {
                    using (SqlConnection connExec = HSCSLIB.DB.ConnectDatabase())
                    {
                        SqlCommand cmd = new SqlCommand(sb.ToString(), connExec);
                        int row = cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }
                }
                catch (System.Exception ex)
                {
                    frmMediaSrv.thisForm.UpdateStatus(TT.T("Database Error: Unable to connect to the database! Please re-confirm. ") + DEBUG.__LINE__());
                }
            }
            #endregion

            #region QUERY / COUNT Command
            else if ((Command.ToUpper() == "/QUERY") || (Command.ToUpper() == "/COUNT"))
            {
                ///參數：
                /// MODE: 顯示資料模式 LOG | PUSHDATA 
                /// RID: 車輛識別序號 
                /// CARID: 車牌號碼
                /// RANGE: 分頁範圍
                /// BEGIN: 起始時間
                /// END: 結束時間
                /// TYPE: 事件模式 //System:0, Event:1, Error:2, Operate:3, Message:4
                /// SEVERITY: 嚴重度

                Dictionary<string, string> PostKeyPair = HSCSLIB.Strings.KeyPairDataToDictionary(Param, "\r\n");

                if (!PostKeyPair.ContainsKey("MODE"))
                {
                    return "NO_DATA";
                }

                #region LOG Command
                if (PostKeyPair["MODE"].ToUpper() == "LOG")
                {
                    StringBuilder sb = new StringBuilder();
                    StringBuilder sbRet = new StringBuilder();

                    if (Command.ToUpper() == "/COUNT")
                        sb.Append("SELECT COUNT(*) FROM ( ");
                    else
                        sb.Append("SELECT * FROM ( ");
                    sb.Append("SELECT  ROW_NUMBER() OVER (ORDER BY PID DESC) AS RowNum, RID,CARID,DeviceTime as UTC,Type,SubType,Severity,Description,FileList as FileInfo FROM LogMessage WHERE 1=1 ");
                    if (PostKeyPair.ContainsKey("RID"))
                        sb.AppendFormat("AND RID='{0}'", PostKeyPair["RID"]);
                    if (PostKeyPair.ContainsKey("CARID"))
                        sb.AppendFormat("AND CARID='{0}'", PostKeyPair["CARID"]);
                    if (PostKeyPair.ContainsKey("BEGIN"))
                        sb.AppendFormat("and DeviceTime >= '{0}' ", PostKeyPair["BEGIN"].Replace("_", " "));
                    if (PostKeyPair.ContainsKey("END"))
                        sb.AppendFormat("and DeviceTime <= '{0}' ", PostKeyPair["END"].Replace("_", " "));

                    if (PostKeyPair.ContainsKey("TYPE"))
                        sb.AppendFormat("and Type = {0} ", PostKeyPair["TYPE"]);
                    if (PostKeyPair.ContainsKey("SEVERITY"))
                        sb.AppendFormat("and severity <= {0} ", PostKeyPair["SEVERITY"]);
                    sb.Append(") AS NewTable ");

                    //只有/Query 指令支援RANGE，/Count 不支援
                    if ((Command.ToUpper() == "/QUERY") && (PostKeyPair.ContainsKey("RANGE")))
                    {
                        bool IsMatchFlag = Regex.IsMatch(PostKeyPair["RANGE"], @"(^(\d+)-(\d+)$)");
                        if (IsMatchFlag)
                        {
                            int start = Convert.ToInt32(HSCSLIB.Strings.GetToken(PostKeyPair["RANGE"], '-', 0));
                            int end = Convert.ToInt32(HSCSLIB.Strings.GetToken(PostKeyPair["RANGE"], '-', 1));
                            if (end >= start) sb.AppendFormat("WHERE RowNum BETWEEN {0} AND {1}", start, end);
                        }

                    }

                    try
                    {
                        using (SqlConnection connExec = HSCSLIB.DB.ConnectDatabase())
                        {

                            SqlCommand cmd = new SqlCommand(sb.ToString(), connExec);
                            SqlDataReader dr = cmd.ExecuteReader();


                            if (Command.ToUpper() == "/COUNT")
                            {
                                dr.Read();
                                sbRet.AppendFormat("{0}\r\n", dr[dr.GetName(0)]);
                            }
                            else
                                while (dr.Read())
                                {
                                    for (int fIdx = 1; fIdx < dr.FieldCount; fIdx++) //從1 開始是因為不要把 RowNum 顯示出來
                                    {

                                        if (dr.GetName(fIdx).Trim().ToUpper() == "UTC")
                                            sbRet.AppendFormat("{0}={1}", dr.GetName(fIdx).Trim().ToUpper(), String.Format("{0:yyyy/MM/dd HH:mm:ss}", ((DateTime)dr[dr.GetName(fIdx)])));
                                        else
                                            sbRet.AppendFormat("{0}={1}", dr.GetName(fIdx).Trim().ToUpper(), dr[dr.GetName(fIdx)].ToString().Trim());

                                        if (fIdx < dr.FieldCount - 1) sbRet.Append(",");
                                    }
                                    sbRet.Append("\r\n");
                                }
                            dr.Close();
                            dr.Dispose();
                            cmd.Dispose();
                        }

                        return sbRet.ToString();
                    }
                    catch (System.Exception ex)
                    {
                        frmMediaSrv.thisForm.UpdateStatus(TT.T("Database Error: Unable to connect to the database! Please re-confirm. ") + DEBUG.__LINE__());
                    }
                }
                #endregion

                #region PUSHDATA Command
                else if (PostKeyPair["MODE"].ToUpper() == "PUSHDATA")
                {
                    StringBuilder sb = new StringBuilder();
                    StringBuilder sbRet = new StringBuilder();

                    if (Command.ToUpper() == "/COUNT")
                        sb.Append("SELECT COUNT(*) FROM ( ");
                    else
                        sb.Append("SELECT * FROM ( ");
                    sb.Append("SELECT  ROW_NUMBER() OVER (ORDER BY PID DESC) AS RowNum, *  FROM LogPushData WHERE 1=1 ");
                    if (PostKeyPair.ContainsKey("RID"))
                        sb.AppendFormat("AND RID='{0}'", PostKeyPair["RID"]);
                    if (PostKeyPair.ContainsKey("CARID"))
                        sb.AppendFormat("AND CARID='{0}'", PostKeyPair["CARID"]);
                    if (PostKeyPair.ContainsKey("BEGIN"))
                        sb.AppendFormat("and DeviceTime >= '{0}' ", PostKeyPair["BEGIN"].Replace("_", " "));
                    if (PostKeyPair.ContainsKey("END"))
                        sb.AppendFormat("and DeviceTime <= '{0}' ", PostKeyPair["END"].Replace("_", " "));

                    sb.Append(") AS NewTable ");

                    //只有/Query 指令支援RANGE，/Count 不支援
                    if ((Command.ToUpper() == "/QUERY") && (PostKeyPair.ContainsKey("RANGE")))
                    {
                        bool IsMatchFlag = Regex.IsMatch(PostKeyPair["RANGE"], @"(^(\d+)-(\d+)$)");
                        if (IsMatchFlag)
                        {
                            int start = Convert.ToInt32(HSCSLIB.Strings.GetToken(PostKeyPair["RANGE"], '-', 0));
                            int end = Convert.ToInt32(HSCSLIB.Strings.GetToken(PostKeyPair["RANGE"], '-', 1));
                            if (end >= start) sb.AppendFormat("WHERE RowNum BETWEEN {0} AND {1}", start, end);
                        }

                    }

                    try
                    {
                        using (SqlConnection connExec = HSCSLIB.DB.ConnectDatabase())
                        {

                            SqlCommand cmd = new SqlCommand(sb.ToString(), connExec);
                            SqlDataReader dr = cmd.ExecuteReader();


                            if (Command.ToUpper() == "/COUNT")
                            {
                                dr.Read();
                                sbRet.AppendFormat("{0}\r\n", dr[dr.GetName(0)]);
                            }
                            else
                                while (dr.Read())
                                {
                                    for (int fIdx = 1; fIdx < dr.FieldCount; fIdx++) //從1 開始是因為不要把 RowNum 顯示出來
                                    {

                                        if (dr.GetName(fIdx).Trim().ToUpper() == "UTC")
                                            sbRet.AppendFormat("{0}={1}", dr.GetName(fIdx).Trim().ToUpper(), String.Format("{0:yyyy/MM/dd HH:mm:ss}", ((DateTime)dr[dr.GetName(fIdx)])));
                                        else
                                            sbRet.AppendFormat("{0}={1}", dr.GetName(fIdx).Trim().ToUpper(), dr[dr.GetName(fIdx)].ToString().Trim());

                                        if (fIdx < dr.FieldCount - 1) sbRet.Append(",");
                                    }
                                    sbRet.Append("\r\n");
                                }
                            dr.Close();
                            dr.Dispose();
                            cmd.Dispose();
                        }

                        return sbRet.ToString();
                    }
                    catch (System.Exception ex)
                    {
                        frmMediaSrv.thisForm.UpdateStatus(TT.T("Database Error: Unable to connect to the database! Please re-confirm. ") + DEBUG.__LINE__());
                    }


                }
                #endregion
            }
            #endregion
            return "";

        }


    }
}
