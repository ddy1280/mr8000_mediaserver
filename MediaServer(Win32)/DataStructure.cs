﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MediaServer
{
    #region 資料欄位
    //Device 主動Push 的資料, 包含GPS Info, TPMS, GSensor Data...
    public class CPushData
    {
        public string RID;          //以裝置之LAN MAC Address碼，做為車輛裝置唯一識別
        public string CARID;        //車牌號碼
        public string UTC;          //此筆資料之基準時間
        // 以下為GPS資料格式
        public string SIG;          //GPS quality indicator(0=Invalid; 1=Fix; 2=Differential; 3=Sensitive)
        public string FIX;          //Operating mode, used for navigation(1=Fix not available; 2=2D; 3=3D)
        public string PDOP;         //Position Dilution of Precision(double)
        public string HDOP;         //Horizontal Dilution of Precision(double)
        public string VDOP;         //Vertical Dilution of Precision(double)
        public string LATITUDE;     //Latitude in NDEG +/- [degree].[min][sec]
        public string LONGITUDE;    //Longitude in NDEG +/- [degree].[min][sec]
        public string ALTITUDE;     //Antenna altitude above/below mean sea level (geoid) in meters
        public string SPEED;        //Speed over the ground in kilometers/hour
        public string HEADING;      //Track angle in degrees True
        public string DECLINATION;  //Magnetic variation degrees(Easterly var. subtracts from course)
        public string INUSE;        //Number of satellites in use (not those in view)
        public string INVIEW;       //Total number of satellites in view
        public string SATELLITES;   //Satellites information, use “|”to separate, max information is 12
                                    //format is <id, in_use, elv, azimum, sig>|<id, in_use, elv, azimum, sig>|…
                                    //  id;           //Satellite PRN number
                                    //  in_use;       //Used in position fix
                                    //  elv;          //Elevation in degrees, 90 maximum
                                    //  azimuth;      //Azimuth, degrees from true north, 000 to 359
                                    //  sig;          //Signal, 00-99 dB
        // 以下為G-Sensor資料格式
        public string GDATA;        //G-Sensor 資料，格式為：<TIME (ms)>:X_Y_Z，可一次傳送多筆資料，以符號 "|" 為間隔。
                                    //<TIME (ms)> 為 UTC 之時間差，以ms 為單位
        // 以下為TPMS資料格式，可一次傳送多筆資料，以符號 "|" 為間隔。
        public string TPMS_PSI;     //TPMS 數值
        public string TPMS_TEMP;    //TPMS 溫度
        public string TPMS_PWR;     //TPMS 電源狀態 (0:OFF, 1:ON)

        public CPushData()
        {
        }
        public CPushData(string logData)
        {
            List<string> Fields = HSCSLIB.Strings.StringToStringList(logData, ",");
            foreach (string field in Fields)
            {
                if ((field.Length == 0) || (field.IndexOf("=") < 0)) continue;
                string fName = field.Substring(0, field.IndexOf("="));
                string fValue = field.Substring(field.IndexOf("=") + 1, field.Length - (field.IndexOf("=") + 1));

                if (fName == "RID") this.RID = fValue;
                else if (fName == "CARID") this.CARID = fValue;
                else if (fName == "UTC") this.UTC = fValue;
                else if (fName == "SIG") this.SIG = fValue;
                else if (fName == "FIX") this.FIX = fValue;
                else if (fName == "PDOP") this.PDOP = fValue;
                else if (fName == "HDOP") this.HDOP = fValue;
                else if (fName == "VDOP") this.VDOP = fValue;
                else if (fName == "LONGITUDE") this.LONGITUDE = fValue;
                else if (fName == "LATITUDE") this.LATITUDE = fValue;
                else if (fName == "ALTITUDE") this.ALTITUDE = fValue;
                else if (fName == "SPEED") this.SPEED = fValue;
                else if (fName == "HEADING") this.HEADING = fValue;
                else if (fName == "DECLINATION") this.DECLINATION = fValue;
                else if (fName == "INUSE") this.INUSE = fValue;
                else if (fName == "INVIEW") this.INVIEW = fValue;
                else if (fName == "SATELLITES") this.SATELLITES = fValue;
                else if (fName == "GDATA") this.GDATA = fValue;
                else if (fName == "TPMS_PSI") this.TPMS_PSI = fValue;
                else if (fName == "TPMS_TEMP") this.TPMS_TEMP = fValue;
                else if (fName == "TPMS_PWR") this.TPMS_PWR = fValue;

            }
        }
    }
    //這個結構要修改
    public class CGSensorQueryData
    {
        public string RID;
        public string CARID;
        public string UTC;
        public string X;
        public string Y;
        public string Z;

        public CGSensorQueryData()
        {

        }
        public CGSensorQueryData(string gData)
        {
            List<string> Fields = HSCSLIB.Strings.StringToStringList(gData, ",");
            foreach (string field in Fields)
            {
                string fName = field.Substring(0, field.IndexOf("="));
                string fValue = field.Substring(field.IndexOf("=") + 1, field.Length - (field.IndexOf("=") + 1));

                if (fName == "RID") this.RID = fValue;
                else if (fName == "CARID") this.CARID = fValue;
                else if (fName == "UTC") this.UTC = fValue;
                else if (fName == "X") this.X = fValue;
                else if (fName == "Y") this.Y = fValue;
                else if (fName == "Z") this.Z = fValue;
            }
        }
    }
    //這個結構要修改
    public class CGSensorData
    {
        public struct GItem
        {
            public Int32 stime;
            public UInt16 gx;
            public UInt16 gy;
            public UInt16 gz;
        };

        public string RID;
        public string CARID;
        public UInt32 UTC;
        public List<GItem> GDATA;


        public CGSensorData()
        {
        }
        public CGSensorData(string logData)
        {
            List<string> Fields = HSCSLIB.Strings.StringToStringList(logData, ",");
            foreach (string field in Fields)
            {
                string fName = field.Substring(0, field.IndexOf("="));
                string fValue = field.Substring(field.IndexOf("=") + 1, field.Length - (field.IndexOf("=") + 1));

                if (fName == "RID") this.RID = fValue;
                else if (fName == "CARID") this.CARID = fValue;
                else if (fName == "UTC") this.UTC = UInt32.Parse(fValue);
                else if (fName == "GDATA")
                {
                    GDATA = new List<GItem>();

                    List<string> sTmps = HSCSLIB.Strings.StringToStringList(fValue, "|");
                    foreach (string ss in sTmps)
                    {
                        // 100:221,222,333
                        GItem gn;
                        gn.stime = Int32.Parse(ss.Substring(0, ss.IndexOf(":")));
                        string tmp = ss.Substring(ss.IndexOf(":") + 1);
                        List<string> gv = HSCSLIB.Strings.StringToStringList(tmp, "_");
                        gn.gx = UInt16.Parse(gv[0]);
                        gn.gy = UInt16.Parse(gv[1]);
                        gn.gz = UInt16.Parse(gv[2]);
                        GDATA.Add(gn);
                    }

                }

            }
        }






        static public List<string> StringToStringList(string str, string split)
        {
            string[] splits = new string[] { split };
            List<string> strList = new List<string>(
                           str.Split(splits,
                           StringSplitOptions.None));
            strList.Remove(""); //remove empty string
            return strList;
        }




    }

    //Push Event Log Message
    public class CMessageData
    {
        public string RID;
        public string CARID;
        public string UTC;
        public int TYPE;
        public int SUBTYPE;
        public int SEVERITY;
        public string DESC;
        public string FILEINFO;
        public CMessageData()
        {
        }
        public CMessageData(string logData)
        {
            List<string> Fields = HSCSLIB.Strings.StringToStringList(logData, ",");
            foreach (string field in Fields)
            {
                if ((field.Length == 0) || (field.IndexOf("=") < 0)) continue;
                string fName = field.Substring(0, field.IndexOf("="));
                string fValue = field.Substring(field.IndexOf("=") + 1, field.Length - (field.IndexOf("=") + 1));

                if (fName == "RID") this.RID = fValue;
                else if (fName == "CARID") this.CARID = fValue;
                else if (fName == "UTC") this.UTC = fValue;
                else if (fName == "TYPE") this.TYPE = Int32.Parse(fValue);
                else if (fName == "SUBTYPE") this.SUBTYPE = Int32.Parse(fValue);
                else if (fName == "SEVERITY") this.SEVERITY = Int32.Parse(fValue);
                else if (fName == "DESCRIPTION") this.DESC = fValue;
                else if (fName == "FILEINFO") this.FILEINFO = fValue;
            }
        }
    }

    //Log Server設定值結構
    public class CMediaServerConfig
    {
        public string RID;
        public string PushType;
        public int JPG_CNT_MOTION;
        public int JPG_CNT_SENSOR;
        public int JPG_CNT_GSENSOR;
        public int JPG_CNT_PANIC;
        public CMediaServerConfig()
        {
        }
        public CMediaServerConfig(string logData)
        {
            List<string> Fields = HSCSLIB.Strings.StringToStringList(logData, ",");
            foreach (string field in Fields)
            {
                if ((field.Length == 0) || (field.IndexOf("=") < 0)) continue;
                string fName = field.Substring(0, field.IndexOf("="));
                string fValue = field.Substring(field.IndexOf("=") + 1, field.Length - (field.IndexOf("=") + 1));

                if (fName == "RID") this.RID = fValue;
                else if (fName == "PUSHTYPE") this.PushType = fValue;
                else if (fName == "JPG_CNT_MOTION") this.JPG_CNT_MOTION = Int32.Parse(fValue);
                else if (fName == "JPG_CNT_SENSOR") this.JPG_CNT_SENSOR = Int32.Parse(fValue);
                else if (fName == "JPG_CNT_GSENSOR") this.JPG_CNT_GSENSOR = Int32.Parse(fValue);
                else if (fName == "JPG_CNT_PANIC") this.JPG_CNT_PANIC = Int32.Parse(fValue);
            }
        }
    }
    #endregion

}


