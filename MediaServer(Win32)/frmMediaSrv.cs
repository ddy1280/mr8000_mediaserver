﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using HSCSLIB;
using System.Data.SqlClient;
using MediaServerSDK;
using BrightIdeasSoftware;
using System.Text.RegularExpressions;
using Gettext;
using System.IO;
using System.Threading;
          
namespace MediaServer
{
    public partial class frmMediaSrv : Form
    {
        private delegate void MessageCallBackinUI(string myStr);
        private String connectionString;
        private int EffectiveCount = 0;
        private int Pagesize = 100;

        private EXT_PROCESS_HANDLER cb_Log = MediaSrvProtocol.LogProc_Callback;
        static public frmMediaSrv thisForm;
        
        
        public frmMediaSrv()
        {
            InitializeComponent();
        }
           
        private void frmLogSrv_Load(object sender, EventArgs e)
        {
            frmMediaSrv.thisForm = this;
            notifyIcon1.Text = this.Text;
            this.Text += " (" + HSCSLIB.WIN32.GetAssemblyVersion() + ")";
            ssModuleVersion.Text = "Module VER: " + HSCSLIB.WIN32.GetVersion(Path.Combine(Directory.GetCurrentDirectory(), "MRSrvMedia.DLL")).Replace(',', '.');

              
            //Load configure
            MediaServer.Default.Reload();
            edParentServerIP.Text = MediaServer.Default.ParentServerIP;
            edParentServerPort.Text = MediaServer.Default.ParentServerPort;
            edPort.Text = MediaServer.Default.Port;
            chkAuto.Checked = MediaServer.Default.AutoRun;

            checkJPEG.Checked = MediaServer.Default.JpegMode; 
           //radioHTTP.Checked= MediaServer.Default.HttpMode;

            if (MediaServer.Default.Path.Length == 0)
                edPath.Text = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            else
                edPath.Text = MediaServer.Default.Path;


            if (CheckRegisterServerAvaliable())
                btnStart_Click(sender, e);


        }

        private void frmLogSrv_FormClosed(object sender, FormClosedEventArgs e)
        {
            MediaServer.Default.Port = edPort.Text;

            MediaServer.Default.AutoRun = chkAuto.Checked;
            MediaServer.Default.ParentServerIP = edParentServerIP.Text;
            MediaServer.Default.ParentServerPort = edParentServerPort.Text;
            MediaServer.Default.Path = edPath.Text;
            MediaServer.Default.ImagePath = "";
            MediaServer.Default.JpegMode = checkJPEG.Checked;
            //MediaServer.Default.HttpMode=radioHTTP.Checked;

            MediaServer.Default.Save();
        }

        public void UpdateStatus(string messageStr)
        {
            if (this.InvokeRequired)
            {
                MessageCallBackinUI cbUI = new MessageCallBackinUI(UpdateStatus);
                this.Invoke(cbUI, messageStr);
            }
            else
            {
                ssMsg.Text = messageStr;
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (true/*ApplyServerSetting()*/)
            {
                /*
                using (SqlConnection connExec = HSCSLIB.DB.ConnectDatabase())
                {
                    if (connExec == null)
                    {
                        ssMsg.Text = TT.T("Database connect fail.");
                        return;
                    }
                }
                */
                MR_MEDIA_STATUS ret = MRMediaSrvClass.MR_MediaSvr_Start("0.0.0.0", edPort.Text, 4);
                if(checkJPEG.Checked)
                    MRMediaSrvClass.MR_MediaSvr_SetStreamSource(1);
                else
                    MRMediaSrvClass.MR_MediaSvr_SetStreamSource(0);
                //if (radioHTTP.Checked)
                //    MRMediaSrvClass.MR_MediaSvr_SetStreamSource(0);
                //else
                //    MRMediaSrvClass.MR_MediaSvr_SetStreamSource(1);

                MRMediaSrvClass.MR_MediaSvr_SetJpegPath(edPath.Text);
                
                if (ret == MR_MEDIA_STATUS.MR_LOG_Success)
                {
                   // MRMediaSrvClass.MR_MediaSvr2_Start("0.0.0.0", edClientPort.Text, 4);

                    //Thread.Sleep(1000);
                    HSCSLIB.CSHARP.SetEnabled(false, btnStart, mmuStart);
                    HSCSLIB.CSHARP.SetEnabled(true, btnStop, mmuStop);

                    HSCSLIB.CSHARP.SetEnabled(false, edPath, btnBrowse, checkJPEG, edPort, edParentServerIP, edParentServerPort, btnTest);

                    notifyIcon1.Text = TT.T("Log Server is running.");
                    ssMsg.Text = TT.T("Log Server is running.");


                }
                MRMediaSrvClass.MR_MediaSvr_SetServerInfo("Log Server", HSCSLIB.WIN32.GetVersion());


                if (edParentServerIP.Text.Length > 0 && edParentServerPort.Text.Length > 0 && UInt16.Parse(edParentServerPort.Text) > 0)
                {
                        MRMediaSrvClass.MR_MediaSvr_SetParentServerIP(edParentServerIP.Text, edParentServerPort.Text);
                }

            }


        }
        private void btnStop_Click(object sender, EventArgs e)
        {
            MR_MEDIA_STATUS ret = MRMediaSrvClass.MR_MediaSvr_Stop();
            if (ret == MR_MEDIA_STATUS.MR_LOG_Success)
            {
                //MRMediaSrvClass.MR_MediaSvr2_Stop();
                HSCSLIB.CSHARP.SetEnabled(true, btnStart, mmuStart);
                HSCSLIB.CSHARP.SetEnabled(false, btnStop, mmuStop);

                HSCSLIB.CSHARP.SetEnabled(true, edPath, btnBrowse, checkJPEG, edPort, edParentServerIP, edParentServerPort, btnTest);
                
                notifyIcon1.Text = TT.T("Log Server has been stopped.");
                ssMsg.Text = TT.T("Log Server has been stopped.");

            }
        }

        private void btnTestDB_Click(object sender, EventArgs e)
        {

        }

        private void chkAuto_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkrun = sender as CheckBox;
            HSCSLIB.WIN32.WriteAutoRunRegistry("Log Server", chkrun.Checked);
        }

        private void btnPath_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog dialog = new FolderBrowserDialog())
            {                     
                dialog.Description = "Select a folder to save snapshot images";
                dialog.ShowNewFolderButton = true;
                dialog.RootFolder = Environment.SpecialFolder.MyDocuments;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
//                    txtPath.Text = dialog.SelectedPath;
//                    MRMediaSrvClass.MR_MediaSvr_SetLogPath(txtPath.Text);
                }
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {


        }

        /// <summary>
        /// 測試Parent Server 是否能連到，服務是否存在
        /// </summary>
        private void btnTest_Click(object sender, EventArgs e)
        {
            CheckRegisterServerAvaliable();
        }

        private bool CheckRegisterServerAvaliable()
        {
            //initial
            edParentServerIP.BackColor = Color.FromArgb(255, 224, 192);
            edParentServerPort.BackColor = Color.FromArgb(255, 224, 192);

            //check ip & port
            if (edParentServerIP.Text.Length>0 && edParentServerPort.Text.Length >0)
            {
                if (HSCSLIB.NET.CheckPort(edParentServerIP.Text, Int32.Parse(edParentServerPort.Text)))
                {
                    edParentServerIP.BackColor = Color.White;
                    edParentServerPort.BackColor = Color.White;
                    ssMsg.Text = TT.T("The parent server {0}:{1} is avaliable.", edParentServerIP.Text, edParentServerPort.Text);
                    return true;
                }
                else
                {
                    ssMsg.Text = TT.T("The parent server {0}:{1} connect fail.", edParentServerIP.Text, edParentServerPort.Text);
                    return false;
                }
            }
            else
            {
                edParentServerIP.Focus();
                ssMsg.Text = TT.T("Please specify the parent server address and port.");
                return false;
            }

        }

        private bool ApplyServerSetting()
        {
            ssMsg.Text = TT.T("Check parent server...");
            if (CheckRegisterServerAvaliable())
            {
                ssMsg.Text = TT.T("Get database connection information...");
                string connStr = GetDatabaseInfoFromRegisterServer();
                if (connStr != "")
                {
                    HSCSLIB.DB.SetDBConnectionString(connStr);
                    ssMsg.Text = TT.T("Connect to database server...");
                    if (HSCSLIB.DB.TestDB())
                    {
                        //grpDBSetting.BackColor = Color.Transparent;
                        ssMsg.Text = TT.T("Database is available.");

                        if (btnStart.Enabled == false && btnStop.Enabled == false)
                        {
                            HSCSLIB.CSHARP.SetEnabled(true, btnStart, mmuStart);
                            HSCSLIB.CSHARP.SetEnabled(false, btnStop, mmuStop);
                        }
                        edParentServerIP.BackColor = Color.White;
                        edParentServerPort.BackColor = Color.White;

                        MediaServer.Default.Save();
                        return true;

                    }
                    else
                    {
                        ssMsg.Text = TT.T("Database can't connect.");
                    }
                }
                else
                {
                    edParentServerIP.BackColor = Color.FromArgb(255, 224, 192);
                    edParentServerPort.BackColor = Color.FromArgb(255, 224, 192);
                    edParentServerIP.Focus();
                    ssMsg.Text = TT.T("Please specify the Parent Server address and port. ");
                }

            }
            return false;

        }


        /// <summary>
        /// 向Register Server 查詢資料庫連線資訊
        /// </summary>
        /// <returns>返回資料庫連線字串</returns>
        private string GetDatabaseInfoFromRegisterServer()
        {
            if (edParentServerIP.Text.Length == 0) return "";
            if (edParentServerPort.Text.Length == 0) return "";

            string url = string.Format("http://{0}:{1}/{2}", edParentServerIP.Text, edParentServerPort.Text, "GET_DB_INFO");
            string retStr = HSCSLIB.NET.HttpPost(url);
            if (retStr.Length > 0)
            {
                string dbinfo = HSCSLIB.CRYPTO.AES_DecryptBase64(retStr, "HS3128");
                string connStr = HSCSLIB.Strings.GetToken(dbinfo, ':', 0);
                return connStr;
            }

            return "";
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {

            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                //
                // The user selected a folder and pressed the OK button.
                // We print the number of files found.
                //
                edPath.Text = folderBrowserDialog1.SelectedPath;
            }
        }





    }

}