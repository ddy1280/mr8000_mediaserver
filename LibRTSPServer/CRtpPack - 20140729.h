// --------------------------------------------------------
// CRtpPack
// - Streaming data packetizer

#ifndef CRTPPACK_H
#define CRTPPACK_H

#define SPS	(1)
#define PPS	(2)
#define RTP_VERSION_OFFSET	(0)
#define RTP_MARK_OFFSET		(1)
#define RTP_SEQ_OFFSET		(2)
#define RTP_TIMETAMP_OFFSET	(4)
#define RTP_SSRC_OFFSET		(8)
#define MAX_PACK_LEN  1400  
#define RTP_HEAD_LEN  12

#define VIDEO_PAYLOAD_TYPE_H264			(0x60)  //96
#define AUDIO_PAYLOAD_TYPE_PCM_ALaw		(0x8)   //8
#define TEXT_PAYLOAD_TYPE_APPLICATION	(0x61)  //97


#define _htons(A)  ( (((unsigned short)(A) & 0xff00) >> 8) | (((unsigned short)(A) & 0x00ff) << 8) )
#define _htonl(A)  ((((unsigned int)(A) & 0xff000000) >> 24) | \
	                        (((unsigned int)(A) & 0x00ff0000) >> 8) | \
							(((unsigned int)(A) & 0x0000ff00) << 8) | \
							(((unsigned int)(A) & 0x000000ff) << 24))


//rtp頭結構
class CRTPHeader
{
private:
	typedef struct rtp_pack_head	 /*12 bytes*/
	{
		/* byte 0 */  
		unsigned  char  csrc_len:4;  /* CC expect 0 */  
		unsigned  char  extension:1; /* X expect 1, see RTP_OP below */  
		unsigned  char  padding:1;   /* P expect 0 */  
		unsigned  char  version:2;   /* V expect 2 */  
		/* byte 1 */  
		unsigned  char  payload:7;  /* PT RTP_PAYLOAD_RTSP */  
		unsigned  char  marker:1;   /* M expect 1 */  
		/* byte 2,3 */  
		unsigned  short  seq_no;    /*sequence number*/  
		/* byte 4-7 */  
		unsigned   int  timestamp;  
		/* byte 8-11 */  
		unsigned  int  ssrc;  /* stream number is used here. */ 
	}rtp_pack_head;
	rtp_pack_head m_Header;
public:
	CRTPHeader(){};
	~CRTPHeader(){};
	void SetSSRC(unsigned  int value){m_Header.ssrc = _htonl(value);};
	void SetTimestamp(unsigned int value){m_Header.timestamp = _htonl(value);};
	void SetSequence(unsigned short value){m_Header.seq_no = _htons(value);};
	friend class CRtpPacker;

}; 

//rtp數據結構
typedef struct rtp_data_s
{
	void *buff;
	char ftp_fu_indicator;
	char ftp_fu_header;
	unsigned int offset;
	unsigned int datalen;
	unsigned int bufrelen;
	unsigned int rtpdatakcount;
} rtp_data_s;
//rtp 包結構
typedef struct rtp_pack_s
{
	void *databuff;
	unsigned int packlen;
	unsigned int packtype;
} rtp_pack_s;




#define MAX_PACKET_LEN	1518
class CRtpPacker
{
private:

	char *m_packetBuf;
	bool IsTCPTransport;
public:
	CRtpPacker();
	~CRtpPacker();

	CRTPHeader RtpHeader;

	void Clear();


	char checkend(unsigned char *p);
	//創建rtp頭
	void *creat_rtp_pack(rtp_data_s *data);
	//rtp封包
	rtp_pack_s *rtp_pack(int streamType,rtp_data_s *pdata);
	//獲取264數據
	void *getdata(char *FrameBuf,int size);

};



#endif