#include "stdafx.h"

#define WIN32_LEAN_AND_MEAN	 //防止 Winsock.h 與 Winsock2.h 衝突

#include <process.h>
#include <Winsock2.h>
#include <windows.h>  

#include "CStreamer.h"
#include "CRtspSession.h"
#include "LibRTSPServer.h"

#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "String.lib")

class CEvent
{
private:
	HANDLE mEvent;
public:
	CEvent(bool bManualReset,bool bInitialState,std::string EventObjName)
	{
	    mEvent = ::CreateEventA(NULL,bManualReset,bInitialState,EventObjName.c_str());
	}
	~CEvent()
	{
		if (mEvent) CloseHandle(mEvent);
	}
	void SetEvent()
	{
		if (mEvent) ::SetEvent(mEvent);
	}
	void ResetEvent()
	{
		if (mEvent) ::ResetEvent(mEvent);
	}
	void WaitFor(DWORD timeout=INFINITE)
	{
		if (mEvent) ::WaitForSingleObject(mEvent,timeout);
	}
};


CRTSPServer::CRTSPServer(int port)
{
	mRtspSessionProc = NULL;
	bThreadExitFlag=false;
	evWaitThread = new CEvent(TRUE, FALSE, "");
}

CRTSPServer::CRTSPServer()
{
	mRtspSessionProc = NULL;
	bThreadExitFlag=false;
	evWaitThread = new CEvent(TRUE, FALSE, "");
}

CRTSPServer::~CRTSPServer()
{
    delete evWaitThread;
}


void CRTSPServer::Start()
{
	//init winsock
	WSADATA     WsaData; 
	int ret = WSAStartup(0x101,&WsaData); 
	if (ret != 0) return;  

	if (hRtspListenThreadHandle==NULL)
	{
		bThreadExitFlag = false;
		hRtspListenThreadHandle = (HANDLE) _beginthreadex(NULL, 0, CRTSPServer::RtspListenThreadFunc,(LPVOID) this,0,&hRtspListenThreadID);
	}
}

void CRTSPServer::Stop()
{
	if (hRtspListenThreadHandle)																				
	{
		bThreadExitFlag = true;

  
		((CEvent*)evWaitThread)->WaitFor(); //等待 Thread 結束
		CloseHandle(hRtspListenThreadHandle);
		hRtspListenThreadHandle= NULL;

	}
	WSACleanup();  
}


//---------------------------------------------------------------------------------------------------

unsigned int __stdcall CRTSPServer::RtspListenThreadFunc(void* param)
{
	//std::cerr << "RtspListenThreadFunc thread start \r\n";
	//perror("RtspListenThreadFunc thread start \r\n");
	CRTSPServer *_this = (CRTSPServer*)param;

	unsigned int hRtspSessionThreadID;
	SOCKET      MasterSocket;                                 // our masterSocket(socket that listens for RTSP client connections)  

	sockaddr_in ServerAddr;                                   // server address parameters
	sockaddr_in ClientAddr;                                   // address parameters of a new RTSP client
	int         ClientAddrLen = sizeof(ClientAddr);   
 

	ClientThreadInfo_S clientThreadInfo;
	clientThreadInfo.object = param;  		   



	ServerAddr.sin_family      = AF_INET;   
	ServerAddr.sin_addr.s_addr = INADDR_ANY;   
	ServerAddr.sin_port        = htons(8554);                 // listen on RTSP port 8554
	MasterSocket               = socket(AF_INET,SOCK_STREAM,0);   

	// bind our master socket to the RTSP port and listen for a client connection
	if (bind(MasterSocket,(sockaddr*)&ServerAddr,sizeof(ServerAddr)) != 0) return 0;  
	if (listen(MasterSocket,64) != 0) return 0;

	u_long iMode = 1;	//Non-blocking mode
	int iResult = ioctlsocket(MasterSocket, FIONBIO, &iMode);

	while (!_this->bThreadExitFlag) 
	{   // loop forever to accept client connections
		clientThreadInfo.ClientSocket = accept(MasterSocket,(struct sockaddr*)&ClientAddr,&ClientAddrLen);

		if(clientThreadInfo.ClientSocket == SOCKET_ERROR)
		{
			if (WSAGetLastError() == WSAEWOULDBLOCK)
			{				
				Sleep(1000);
				continue;
			}
			else
			{
				perror("call accept error");
				break;
			}
		}
		else
		{
		    
			HANDLE hRtspSessionThreadHandle = (HANDLE) _beginthreadex(NULL, 0, CRTSPServer::SessionThreadHandler,(LPVOID) &clientThreadInfo,0,&hRtspSessionThreadID);
			CloseHandle(hRtspSessionThreadHandle);
			printf("Client connected. Client address: %s\r\n",inet_ntoa(ClientAddr.sin_addr));  
		}

		Sleep(500);

	}  

	closesocket(MasterSocket);   
 
    ((CEvent*)_this->evWaitThread)->SetEvent();
	//std::cerr << "RtspListenThreadFunc thread end \r\n";
	//perror("RtspListenThreadFunc thread end \r\n");
}

unsigned int __stdcall CRTSPServer::SessionThreadHandler(void* param)
{
	ClientThreadInfo_S *clientInfo = (ClientThreadInfo_S*)param;
	CRTSPServer *_this = (CRTSPServer*)clientInfo->object;
	SOCKET Client = clientInfo->ClientSocket;

	char         RecvBuf[10000];                    // receiver buffer
	int          res;  
	CStreamer    Streamer(Client);                  // our streamer for UDP/TCP based RTP transport
	CRtspSession RtspSession(Client,&Streamer);     // our threads RTSP session and state
	int          StreamID = 0;                      // the ID of the 2 JPEG samples streams which we support

	WSAEVENT RtspReadEvent = WSACreateEvent();      // create READ wait event for our RTSP client socket
	WSAEventSelect(Client,RtspReadEvent,FD_READ);   // select socket read event



	while (!_this->bThreadExitFlag)
	{
		WaitForSingleObject(RtspReadEvent,INFINITE);
		WSAResetEvent(RtspReadEvent);

		memset(RecvBuf,0x00,sizeof(RecvBuf));
		res = recv(Client,RecvBuf,sizeof(RecvBuf),0);

		// we filter away everything which seems not to be an RTSP command: O-ption, D-escribe, S-etup, P-lay, T-eardown
		if ((RecvBuf[0] == 'O') || (RecvBuf[0] == 'D') || (RecvBuf[0] == 'S') || (RecvBuf[0] == 'P') || (RecvBuf[0] == 'T'))
		{
			RTSP_CMD_TYPES C = RtspSession.Handle_RtspRequest(RecvBuf,res);
			printf("RTSP State:%s\r\n",RtspSession.RtspStateString(C).c_str());
			if (C == RTSP_PLAY) 
			{
				if (_this->mRtspSessionProc!=NULL) 
				{
					//回呼至上層，由HTTP SERVER 讀取資料至 Streamer.ReadStreaming
					int ret = _this->mRtspSessionProc((RTSP_SESSION_HANDLE)&RtspSession,(RTSP_STREAMER_HANDLE)&Streamer);
					if (ret <0) 
					{
						break;
					}					
				}
			}
			else if (C == RTSP_TEARDOWN) 
			{
				break;
			}

		}



	}

	closesocket(Client);
	return 0;

//-------------------------------------------------------------------------------------------------------------------------------------------------
	//HANDLE       WaitEvents[2];                     // the waitable kernel objects of our session

	//HANDLE HTimer = ::CreateWaitableTimerA(NULL,false, NULL);

	//WSAEVENT RtspReadEvent = WSACreateEvent();      // create READ wait event for our RTSP client socket
	//WSAEventSelect(Client,RtspReadEvent,FD_READ);   // select socket read event
	//WaitEvents[0] = RtspReadEvent;
	//WaitEvents[1] = HTimer;

	//// set frame rate timer
	//double T = 33.3333333;                                       // frame rate 30fps
	//int iT   = T;
	//const __int64 DueTime = - static_cast<const __int64>(iT) * 10 * 1000;
	//::SetWaitableTimer(HTimer,reinterpret_cast<const LARGE_INTEGER*>(&DueTime),iT,NULL,NULL,false);


	//bool StreamingStarted = false;
	//while (!_this->bThreadExitFlag)
	//{
	//	DWORD objState = WaitForMultipleObjects(2,WaitEvents,false,INFINITE);

	//	if (objState==WAIT_OBJECT_0 + 0)
	//	{   // read client socket
	//		WSAResetEvent(WaitEvents[0]);

	//		memset(RecvBuf,0x00,sizeof(RecvBuf));
	//		res = recv(Client,RecvBuf,sizeof(RecvBuf),0);

	//		// we filter away everything which seems not to be an RTSP command: O-ption, D-escribe, S-etup, P-lay, T-eardown
	//		if ((RecvBuf[0] == 'O') || (RecvBuf[0] == 'D') || (RecvBuf[0] == 'S') || (RecvBuf[0] == 'P') || (RecvBuf[0] == 'T'))
	//		{
	//			RTSP_CMD_TYPES C = RtspSession.Handle_RtspRequest(RecvBuf,res);
	//			printf("RTSP State:%s\r\n",RtspSession.RtspStateString(C).c_str());
	//			if (C == RTSP_PLAY)     
	//				StreamingStarted = true; 
	//			else if (C == RTSP_TEARDOWN) 
	//			{
	//				break;
	//			}
	//				
	//		}
	//	}
	//	else if (objState==WAIT_OBJECT_0 + 1)
	//	{
	//		if (StreamingStarted && _this->mRtspSessionProc!=NULL) 
	//		{
	//			//回呼至上層，由HTTP SERVER 讀取資料至 Streamer.ReadStreaming
	//			int ret = _this->mRtspSessionProc((RTSP_SESSION_HANDLE)&RtspSession,(RTSP_STREAMER_HANDLE)&Streamer);
	//			if (ret <0) 
	//			{
	//				break;
	//			}					
	//		}
	//	}

	//}



}
