
#include "CRtpPack.h"
#include <string.h>
#include <stdio.h>


#include "CRtpPack.h"


#pragma comment(lib, "Ws2_32.lib")


CRtpPacker::CRtpPacker()
{
	IsTCPTransport = true;
	m_packetBuf = new char[MAX_PACKET_LEN];
}

CRtpPacker::~CRtpPacker()
{
	delete []m_packetBuf;
	m_packetBuf = NULL;
}

void CRtpPacker::Clear()
{
	memset(m_packetBuf,0,MAX_PACKET_LEN);
}
//創建rtp包
void *CRtpPacker::creat_rtp_pack(rtp_data_s *data)
{
	unsigned char *buf;
	//數據長度檢測，數據長度大於最大包長，則分包。
	if(data->datalen>MAX_PACK_LEN&&data->datalen>(data->bufrelen+data->rtpdatakcount))
	{
		//分包
		unsigned int templen=(data->datalen-data->bufrelen-1);
		data->rtpdatakcount+=1;
		if(templen>MAX_PACK_LEN-1)
		{
			buf= new unsigned char[MAX_PACK_LEN];
			memset(buf,0,MAX_PACK_LEN);
			if(data->bufrelen==0)
			{
				//第一個分包
				memcpy(buf+RTP_HEAD_LEN+1,(unsigned char*)data->buff+data->offset,MAX_PACK_LEN-RTP_HEAD_LEN-1);

				data->ftp_fu_indicator=((*(buf+RTP_HEAD_LEN+1)&0xE0));
				data->ftp_fu_header=(*(buf+RTP_HEAD_LEN+1)&0x1f);
				*(buf+RTP_HEAD_LEN+1)=(data->ftp_fu_header|(0x80));
				data->bufrelen+=MAX_PACK_LEN;
				data->offset+=MAX_PACK_LEN-RTP_HEAD_LEN-1;
			}
			else
			{
				//中間分包
				memcpy((buf+RTP_HEAD_LEN+2),(unsigned char*)data->buff+data->offset,MAX_PACK_LEN-RTP_HEAD_LEN-2);
				*(buf+RTP_HEAD_LEN+1)=(data->ftp_fu_header|(0x00));
				data->bufrelen+=MAX_PACK_LEN;
				data->offset+=MAX_PACK_LEN-RTP_HEAD_LEN-2;
			}

			*(buf+RTP_HEAD_LEN)=(data->ftp_fu_indicator|(0x1c));

		}
		else
		{
			//最後一個分包
			templen=data->datalen-data->offset;
			buf=new unsigned char [templen+RTP_HEAD_LEN+2];
			memset(buf,0,templen+RTP_HEAD_LEN+2);
			memcpy((buf+RTP_HEAD_LEN+2),(unsigned char*)data->buff+data->offset,templen);
			*(buf+RTP_HEAD_LEN)=(0x1c|data->ftp_fu_indicator);
			*(buf+RTP_HEAD_LEN+1)=(data->ftp_fu_header|(0x40));
			data->bufrelen+=templen+RTP_HEAD_LEN+2;
			data->offset+=templen-1;
		}
	}
	else if(data->datalen>data->bufrelen)
	{
		//數據長度小於包長，則不分包
		buf= new unsigned char [data->datalen+RTP_HEAD_LEN];
		memset(buf,0,data->datalen+RTP_HEAD_LEN);

		memcpy((buf+RTP_HEAD_LEN),data->buff,data->datalen);
		data->bufrelen+=data->datalen+RTP_HEAD_LEN;
	}
	else
		return NULL;

	return buf;
}

//typedef enum STREAM_TYPE
//{
//	ST_UNKNOWN = -1,
//	ST_VIDEO =0,
//	ST_AUDIO,
//	ST_TEXT,
//}STREAM_TYPE;
rtp_pack_s *CRtpPacker::rtp_pack(int streamType,rtp_data_s *pdata)
{
	rtp_pack_s *pack = new rtp_pack_s;
	char *rtp_buff;
	int len=pdata->bufrelen;

	//獲取封包後的rtp數據包
	rtp_buff=(char*)creat_rtp_pack(pdata);

	if(rtp_buff!=NULL)
	{
		//固定頭部填充
		RtpHeader.m_Header.version = 2;
		RtpHeader.m_Header.padding=0;
		RtpHeader.m_Header.extension=0;//1; // {Add RTP Header Extension}
		RtpHeader.m_Header.csrc_len=0;
		RtpHeader.m_Header.marker=0;

		if (streamType==0)
		{
			RtpHeader.m_Header.payload = VIDEO_PAYLOAD_TYPE_H264;
		}
		else if (streamType==1)
		{
			RtpHeader.m_Header.payload = AUDIO_PAYLOAD_TYPE_PCM_ALaw;
		}
		else if (streamType==2)
		{
			RtpHeader.m_Header.payload = TEXT_PAYLOAD_TYPE_APPLICATION;
		}
		//創建rtp_pack_s結構
		
		memcpy(rtp_buff,&RtpHeader.m_Header,sizeof(CRTPHeader::rtp_pack_head));
		pack->databuff=rtp_buff;
		pack->packlen=pdata->bufrelen-len;
		return pack;
	}
	else
	{
		delete [] pdata->buff;
		pdata->buff=NULL;

		delete pack;
		return NULL;

	}

}

//H264 開始碼檢測
char CRtpPacker::checkend(unsigned char *p)
{
	if((*(p+0)==0x00)&&(*(p+1)==0x00)&&(*(p+2)==0x00)&&(*(p+3)==0x01))
		return 1;
	else
		return 0;
}


void *CRtpPacker::getdata(char *FrameBuf,int size)
{
	//rtp_data_s結構創建，填充
	rtp_data_s *rtp_data=new rtp_data_s ;
	memset(rtp_data,0,sizeof(rtp_data_s));

	char *fbuf = new char[size];
	memcpy(fbuf,FrameBuf,size); 

	rtp_data->buff=fbuf;    
	rtp_data->datalen=size;
	rtp_data->bufrelen=0;
	return rtp_data;



}

