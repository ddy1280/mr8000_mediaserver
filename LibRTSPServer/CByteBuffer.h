#ifndef CBYTEBUFFER_H
#define CBYTEBUFFER_H
#include <stdarg.h>


class CByteBuffer
{
private:
	unsigned char *m_buf;
	unsigned char *m_buf_end;
	int m_size;
	
public:
	CByteBuffer();
	CByteBuffer(int size);
	~CByteBuffer();

	void PutByte(unsigned char value);
	void Put2Byte(unsigned short value);
	void Put4Byte(unsigned int value);
	void PutBuffer(const char *pcData, int iSize);
	void PutString(const char *fmt, ...);

	void Reset();
	void Reset(int size);

	inline const char* Buffer(){return (const char*) m_buf;};
	inline int Length(){return m_buf_end - m_buf;};
	//CByteBuffer& operator+(const CByteBuffer& rhs);
 
};


#endif