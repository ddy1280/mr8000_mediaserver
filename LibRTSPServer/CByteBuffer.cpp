#include <iostream>

#include "CByteBuffer.h"

#define DEFAULT_BUFFER_SIZE (4096)


CByteBuffer::CByteBuffer(int size)
{
    m_buf = NULL;
	Reset(size);
}

CByteBuffer::CByteBuffer()
{
	m_buf = NULL;
	Reset(DEFAULT_BUFFER_SIZE);
}
CByteBuffer::~CByteBuffer()
{
	delete [] m_buf;
	m_buf = NULL;
}
void CByteBuffer::PutByte(unsigned char value)
{
	*(m_buf_end)++ = value;
}
void CByteBuffer::Put2Byte(unsigned short value)
{
	PutByte( value >> 8);
	PutByte( value);
}
void CByteBuffer::Put4Byte(unsigned int value)
{
	PutByte( value >> 24);
	PutByte( value >> 16);
	PutByte( value >> 8);
	PutByte( value);
}
void CByteBuffer::PutBuffer(const char *pcData, int iSize)
{
	if ((m_size - Length() )<iSize)
	{
		throw "Buffer will overflow!!";
	}
	memcpy(m_buf_end,pcData,iSize);
	m_buf_end+=iSize;
}


void CByteBuffer::PutString(const char *fmt, ...)
{
	va_list ap;
	char buf[1024];
	int ret = 0;

	va_start(ap, fmt);
	ret = vsnprintf(buf, sizeof(buf), fmt, ap);
	va_end(ap);

	PutBuffer(buf, ret);
}


void CByteBuffer::Reset()
{
	if (m_buf)
	{
		memset(m_buf,0,m_size);
		m_buf_end = m_buf;
	}
}

void CByteBuffer::Reset(int size)
{
    if (m_buf)
	{
		delete [] m_buf;
		m_buf = NULL;
	}
	m_size = size;
	m_buf = new unsigned char [size];
    m_buf_end = m_buf;
}

