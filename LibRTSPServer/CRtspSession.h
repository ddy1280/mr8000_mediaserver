// MediaLAN 02/2013
// CRtspSession
// - parsing of RTSP requests and generation of RTSP responses


#ifndef _CRtsp_Session_H
#define _CRtsp_Session_H

#include <Winsock.h>
#include <windows.h>  
#include <string>
#include <map>
#include "CStreamer.h"
#include "CByteBuffer.h"

// supported command types
enum RTSP_CMD_TYPES
{
    RTSP_OPTIONS=0,
    RTSP_DESCRIBE,
    RTSP_SETUP,
    RTSP_PLAY,
    RTSP_TEARDOWN,
	RTSP_SET_PARAMETER,	 //�|�ݹ�{
    RTSP_UNKNOWN
};
/*** RTSP handling */
enum RTSPStatusCode 
{
	RTSP_STATUS_OK              =200, /***< OK */
	RTSP_STATUS_METHOD          =405, /***< Method Not Allowed */
	RTSP_STATUS_BANDWIDTH       =453, /***< Not Enough Bandwidth */
	RTSP_STATUS_SESSION         =454, /***< Session Not Found */
	RTSP_STATUS_STATE           =455, /***< Method Not Valid in This State */
	RTSP_STATUS_AGGREGATE       =459, /***< Aggregate operation not allowed */
	RTSP_STATUS_ONLY_AGGREGATE  =460, /***< Only aggregate operation allowed */
	RTSP_STATUS_TRANSPORT       =461, /***< Unsupported transport */
	RTSP_STATUS_INTERNAL        =500, /***< Internal Server Error */
	RTSP_STATUS_SERVICE         =503, /***< Service Unavailable */
	RTSP_STATUS_VERSION         =505, /***< RTSP Version not supported */
};

typedef enum StreamModeEnum
{
	SS_MAIN_STREAM=0,
	SS_SUB_STREAM,
	SS_JPEG_STREAM,
};

#define RTSP_BUFFER_SIZE       10000    // for incoming requests, and outgoing responses
#define RTSP_PARAM_STRING_MAX  200  


//To discriminate interleave port on TCP connection 
typedef struct INTERLEAVE_PORT_DEF
{
	int interleave_rtp_port;
	int interleave_rtcp_port;
}INTERLEAVE_PORT_DEF;

typedef enum STREAM_ID_TYPE
{
	SIT_VIDEO=0,
	SIT_AUDIO,
	SIT_TEXT,
}STREAM_ID_TYPE;

class CRtspSession
{
public:
    CRtspSession(SOCKET aRtspClient, CStreamer * aStreamer);
    ~CRtspSession();

    RTSP_CMD_TYPES Handle_RtspRequest(char const * aRequest, unsigned aRequestSize);
	StreamModeEnum GetStreamID(){return m_StreamID;};

	int GetChannelID(){return m_ChannelID;};
	std::string GetDeviceID(){return m_RID;};
	std::string GetRID();
	int GetPlayTimeUTC(){return m_UTC;};
	std::string GetStreamType(){return m_StreamType;};
	unsigned int GetSessionID();
	std::string GetStreamHashIndex();  
	std::string RtspStateString(RTSP_CMD_TYPES type);
	RTSP_CMD_TYPES RtspState(){return m_RtspCmdType;};
	INTERLEAVE_PORT_DEF GetInterleavePorts(STREAM_ID_TYPE streamid){return m_stream_id_types[streamid];};
private:
	CByteBuffer rtspBuf;


    void Init();
    bool ParseRtspRequest(char const * aRequest, unsigned aRequestSize);

    // RTSP request command handlers
    void Handle_RtspOPTION();
    void Handle_RtspDESCRIBE();
    void Handle_RtspSETUP(); 
    void Handle_RtspPLAY();
	void Handle_RtspSETPARAMETER();

    void RtspReplyHeader(RTSPStatusCode status);
	void RtspReplyError(RTSPStatusCode errorStatus);

    // global session state parameters
    unsigned int   m_RtspSessionID;							  // RTSP Session ID
    SOCKET         m_RtspClient;                              // RTSP socket of that session
    StreamModeEnum m_StreamID;                                // number of simulated stream of that session	(CH: Channel)
    u_short        m_ClientRTPPort;                           // client port for UDP based RTP transport
    u_short        m_ClientRTCPPort;                          // client port for UDP based RTCP transport  
    bool           m_TcpTransport;                            // if Tcp based streaming was activated
	std::string	   m_Interleaved;
    CStreamer    * m_Streamer;                                // the UDP or TCP streamer of that session

	int			   m_ChannelID;
	std::string m_RID;
	std::string m_StreamType;								  // Command, like...live or pb
	time_t m_UTC;
    // parameters of the last received RTSP request
	 
	std::map<STREAM_ID_TYPE,INTERLEAVE_PORT_DEF> m_stream_id_types;


    RTSP_CMD_TYPES m_RtspCmdType;                             // command type (if any) of the current request
    char           m_URLPreSuffix[RTSP_PARAM_STRING_MAX];     // stream name pre suffix 
    char           m_URLSuffix[RTSP_PARAM_STRING_MAX];        // stream name suffix
    char           m_CSeq[RTSP_PARAM_STRING_MAX];             // RTSP command sequence number
    char           m_URLHostPort[RTSP_BUFFER_SIZE];           // host:port part of the URL
    unsigned       m_ContentLength;                           // SDP string size


};

#endif