// MediaLAN 02/2013
// CRtspSession
// - JPEG packetizer and UDP/TCP based streaming

#ifndef _CStreamer_H
#define _CStreamer_H


#include <Windows.h>
#include <string>
#include <stdio.h>
#include <Winsock.h>
#include "CRtpPack.h"

#define MTU_SIZE	(1500 - 20 - 24 - 12)  ///MTU - IP Header - TCP Header - RTP Header

typedef struct RTP_HEAD
{
	unsigned int SSRC;
	unsigned short  SequenceNumber;	  //16 bit
	DWORD   Timestamp;
};

typedef unsigned int SOCKET; 
class CStreamer
{
public:
    CStreamer(SOCKET aClient);
    ~CStreamer();

    void    InitTransport(unsigned short  aRtpPort, unsigned short aRtcpPort, bool TCP);
    unsigned short  GetRtpServerPort();
    unsigned short  GetRtcpServerPort();
	int    SendRTPPacket(int streamType,char *buffer, int size,int rtpChannel=0);

private:

	int RecvPacket();
	RTP_HEAD m_rtpInfo[3];	//for Video, audio, text stream

//	CH264_RTP_PACK *m_rtpPack;
    SOCKET  m_RtpSocket;          // RTP socket for streaming RTP packets to client
    SOCKET  m_RtcpSocket;         // RTCP socket for sending/receiving RTCP packages

    unsigned short  m_RtpClientPort;      // RTP receiver port on client 
    unsigned short  m_RtcpClientPort;     // RTCP receiver port on client
    unsigned short  m_RtpServerPort;      // RTP sender port on server 
    unsigned short  m_RtcpServerPort;     // RTCP sender port on server

    int     m_SendIdx;
    bool    m_TCPTransport;
    SOCKET  m_Client;
};

#endif