// MediaLAN 02/2013
// CRtspSession
// - JPEG packetizer and UDP/TCP based streaming

#include "CStreamer.h"

#include <time.h>
#include <stdio.h>


CStreamer::CStreamer(SOCKET aClient):m_Client(aClient)
{
    m_RtpServerPort  = 0;
    m_RtcpServerPort = 0;
    m_RtpClientPort  = 0;
    m_RtcpClientPort = 0;


    m_SendIdx        = 0;
    m_TCPTransport   = false;

	srand(time(NULL));
	for (int streamIndex=0;streamIndex<3;streamIndex++)
	{
		
		m_rtpInfo[streamIndex].SSRC = m_RtpSocket << 16 | rand()*0xFFFF+1;         // create a SSRC ID
		m_rtpInfo[streamIndex].SequenceNumber = 0;
		m_rtpInfo[streamIndex].Timestamp = 0;
	}


};

CStreamer::~CStreamer()
{
    closesocket(m_RtpSocket);
    closesocket(m_RtcpSocket);


};



void CStreamer::InitTransport(unsigned short  aRtpPort, unsigned short  aRtcpPort, bool TCP)
{
    sockaddr_in Server;  

    m_RtpClientPort  = aRtpPort;
    m_RtcpClientPort = aRtcpPort;
    m_TCPTransport   = TCP;



    if (!m_TCPTransport)
    {   // allocate port pairs for RTP/RTCP ports in UDP transport mode
        Server.sin_family      = AF_INET;   
        Server.sin_addr.s_addr = INADDR_ANY;   
        for (unsigned short  P = 6970; P < 0xFFFE ; P += 2)
        {
            m_RtpSocket     = socket(AF_INET, SOCK_DGRAM, 0);                     
            Server.sin_port = htons(P);
            if (bind(m_RtpSocket,(sockaddr*)&Server,sizeof(Server)) == 0)
            {   // Rtp socket was bound successfully. Lets try to bind the consecutive Rtsp socket
                m_RtcpSocket = socket(AF_INET, SOCK_DGRAM, 0);
                Server.sin_port = htons(P + 1);
                if (bind(m_RtcpSocket,(sockaddr*)&Server,sizeof(Server)) == 0) 
                {
                    m_RtpServerPort  = P;
                    m_RtcpServerPort = P+1;
                    break; 
                }
                else
                {
                    closesocket(m_RtpSocket);
                    closesocket(m_RtcpSocket);
                };
            }
            else closesocket(m_RtpSocket);
        };
    };
};

unsigned short  CStreamer::GetRtpServerPort()
{
    return m_RtpServerPort;
};

unsigned short  CStreamer::GetRtcpServerPort()
{
    return m_RtcpServerPort;
};

//typedef enum STREAM_TYPE
//{
//	ST_UNKNOWN = -1,
//	ST_VIDEO =0,
//	ST_AUDIO,
//	ST_TEXT,
//}STREAM_TYPE;
int CStreamer::SendRTPPacket(int streamType,char *buffer, int size,int rtpChannel)
{
	if (streamType == -1) return -1;

	sockaddr_in RecvAddr;
	int         RecvLen = sizeof(RecvAddr);


	// get client address for UDP transport
	getpeername(m_Client,(struct sockaddr*)&RecvAddr,&RecvLen);
	RecvAddr.sin_family = AF_INET;
	RecvAddr.sin_port   = htons(m_RtpClientPort);

	CRtpPacker rtppkg(streamType,buffer,size,m_TCPTransport);


	//每一個串流，要有各自的SSRC, Timestamp, Sequence

	rtppkg.SetRTPHeader_SSRC(m_rtpInfo[streamType].SSRC);
	rtppkg.SetRTPHeader_Timestamp(m_rtpInfo[streamType].Timestamp);
	rtppkg.SetRTPHeader_Sequence(m_rtpInfo[streamType].SequenceNumber);

	rtppkg.SetRTPChannel(rtpChannel);

	//if (streamType==0)
	//{
	//	FILE *fp;
	//	char filename[255];
	//	sprintf(filename,"bufdata_src_%d.bin",m_SendIdx);
	//	fp = fopen(filename,"wb");
	//	fwrite(buffer,size,1,fp);
	//	fclose(fp);

	//}

	while (!rtppkg.Eof())
	{
		rtppkg.RTP_Pack();

		//if (rtppkg.PacketSeq()<10)
		//{
		//	char filename[255];
		//	sprintf(filename,"bufdata%d.bin",rtppkg.PacketSeq());

		//	fp=fopen(filename,"wb");
		//	fwrite(rtppkg.PacketData(),rtppkg.PacketSize(),1,fp);
		//	fclose(fp);

		//}
		int num=0;
		//通過網絡將數據發送出去

		if (m_TCPTransport) 
			num = send(m_Client,(const char*)rtppkg.PacketData(),rtppkg.PacketSize(),0);
		else
		    sendto(m_RtpSocket,(const char*)rtppkg.PacketData(),rtppkg.PacketSize(),0,(SOCKADDR *) & RecvAddr,sizeof(RecvAddr));

		int recvNum = RecvPacket();
		if (recvNum<0) return recvNum;

	
		//序列號增加
		m_rtpInfo[streamType].SequenceNumber++;
		rtppkg.SetRTPHeader_Sequence(m_rtpInfo[streamType].SequenceNumber);
		rtppkg.MoveNext();
	}

	if (streamType == 0) 
		m_rtpInfo[streamType].Timestamp+=3600;
	else if (streamType == 1)// ST_AUDIO
		m_rtpInfo[streamType].Timestamp+=size;	// 8000/25
	else if (streamType == 2)// ST_TEXT
		m_rtpInfo[streamType].Timestamp+=3600;//暫訂

	m_SendIdx++;
}



int CStreamer::RecvPacket()
{
	char recvbuf[1000];
	int recvNum=0;
	if (m_TCPTransport) 
		recvNum = recv (m_Client,recvbuf,1000, 0);
	else
	{
		sockaddr_in RecvAddr;
		int         RecvLen = sizeof(RecvAddr);
		// get client address for UDP transport
		getpeername(m_Client,(struct sockaddr*)&RecvAddr,&RecvLen);
		RecvAddr.sin_family = AF_INET;
		RecvAddr.sin_port   = htons(m_RtpClientPort);

		recvNum = recvfrom(m_Client,recvbuf,1000, 0,(struct sockaddr *)&RecvAddr ,&RecvLen);
	}
	std::string recvStr(recvbuf);
	if (recvStr.find("TEARDOWN")!=std::string::npos)
	{
		return -2;
	}
}
 