#ifndef RTSPSERVER_H  
#define RTSPSERVER_H  


#define RTSP_SESSION_HANDLE void*
#define RTSP_STREAMER_HANDLE void*

typedef int (*EXT_RTSP_SESSION_HANDLER)(const RTSP_SESSION_HANDLE rtspHandle,const RTSP_STREAMER_HANDLE ssHandle);

class CRTSPServer
{
public:
	CRTSPServer(int port);
	CRTSPServer();
	~CRTSPServer();
	void SetRtspSessionHandler(EXT_RTSP_SESSION_HANDLER handler){mRtspSessionProc=handler;};
	void Start();
	void Stop();



protected:

private:
	//{ for Thread
	volatile bool bThreadExitFlag;
	void* hRtspListenThreadHandle;
	unsigned int hRtspListenThreadID;
	void *evWaitThread;
	//}

	struct ClientThreadInfo_S
	{
		SOCKET ClientSocket; 
		void * object;
	}; 

	EXT_RTSP_SESSION_HANDLER mRtspSessionProc;
	static unsigned int __stdcall RtspListenThreadFunc(void* param);
	static unsigned int __stdcall SessionThreadHandler(void* param);

};

#endif

