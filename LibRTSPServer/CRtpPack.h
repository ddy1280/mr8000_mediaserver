// --------------------------------------------------------
// CRtpPack
// - Streaming data packetizer

#include <stdio.h>

#ifndef CRTPPACK_H
#define CRTPPACK_H

//#define SPS	(1)
//#define PPS	(2)
//#define RTP_VERSION_OFFSET	(0)
//#define RTP_MARK_OFFSET		(1)
//#define RTP_SEQ_OFFSET		(2)
//#define RTP_TIMETAMP_OFFSET	(4)
//#define RTP_SSRC_OFFSET		(8)
 

#define VIDEO_PAYLOAD_TYPE_H264			(0x60)  //96
#define AUDIO_PAYLOAD_TYPE_PCM_ALaw		(0x8)   //8
#define TEXT_PAYLOAD_TYPE_APPLICATION	(0x61)  //97


#define _htons(A)  ( (((unsigned short)(A) & 0xff00) >> 8) | (((unsigned short)(A) & 0x00ff) << 8) )
#define _htonl(A)  ((((unsigned int)(A) & 0xff000000) >> 24) | \
	                        (((unsigned int)(A) & 0x00ff0000) >> 8) | \
							(((unsigned int)(A) & 0x0000ff00) << 8) | \
							(((unsigned int)(A) & 0x000000ff) << 24))

#define MAX_PACKET_LEN		1400//1460//1518
#define RTP_HEAD_LEN		12
#define RTP_HEAD_EXT_LEN	12
#define TCP_INTERLEAVE_FRAME_HEAD_LEN		4
#define FU_LEN	2
#define MAX_PACK_LEN  (MAX_PACKET_LEN-TCP_INTERLEAVE_FRAME_HEAD_LEN-RTP_HEAD_LEN) 


typedef enum 
{
	FU_PacketFirst=0,
	FU_PacketMiddle,
	FU_PacketLast,
	FU_PacketOnlyOne
}FUPacketSeq;



typedef struct RTP_HEADER	 /*12 bytes*/
{
	/* byte 0 */  
	unsigned  char  csrc_len:4;  /* CC expect 0 */  
	unsigned  char  extension:1; /* X expect 1, see RTP_OP below */  
	unsigned  char  padding:1;   /* P expect 0 */  
	unsigned  char  version:2;   /* V expect 2 */  
	/* byte 1 */  
	unsigned  char  payload:7;  /* PT RTP_PAYLOAD_RTSP */  
	unsigned  char  marker:1;   /* M expect 1 */  
	/* byte 2,3 */  
	unsigned  short  seq_no;    /*sequence number*/  
	/* byte 4-7 */  
	unsigned   int  timestamp;  
	/* byte 8-11 */  
	unsigned  int  ssrc;  /* stream number is used here. */ 
	unsigned  int  csrc[1];         /* optional CSRC list */
}RTP_HEADER;

typedef struct RTP_HEADER_EXTENSION
{
	/*byte 0-3*/
	unsigned short def_profile; /*defined by profile*/
	unsigned short ext_len;	    /*length*/
	/*byte 4-12*/
	unsigned int rec_sec; /* TimeStamp Sec*/	
	unsigned int rec_usec; /* TimeStamp uSuec */
} RTP_HEADER_EXTENSION; /* 12 bytes */

/****************************************************************** 
FU_INDICATOR	 FU_HEADER
+---------------+---------------+ 
|7|6|5|4|3|2|1|0|7|6|5|4|3|2|1|0|  
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ 
|F|NRI| Type    |S|E|R| Type    |  
+---------------+---------------+ 
|    1 byte     |    1 byte     |

******************************************************************/ 
typedef  struct  __fu_indicator__ {  
	//byte 0  
	unsigned  char  Type:5;  
	unsigned  char  NRI:2;   
	unsigned  char  FBit:1;           
} FU_INDICATOR;  /*1 byte */  
 
typedef  struct  __fu_header__ {  
	//byte 0  
	unsigned  char  NalUnitType:5;  //Type
	unsigned  char  ForbiddenBit:1; //R 
	unsigned  char  EndBit:1;		//E
	unsigned  char  StartBit:1;     //S 
} FU_HEADER;  /* 1 byte */


class CRtpPacker
{
private:

	char *m_srcBuffer;
	int m_srcBufSize;
	char *m_SrcBuffCurrPtr;					//RAW DATA要開始傳送的位址
	int m_SrcBuffCurrSize;					//RAW DATA剩餘未傳完的size
	//int m_ActualWrite
	FU_INDICATOR *fuIndictor;																		   
	bool mIsTCPTransport;
	bool m_eof;
	int m_streamType;
	char m_packetBuf[MAX_PACKET_LEN];
	int m_idx;
	int m_rtp_offset;
	int m_final_size;						//最後實際要寫入或傳送的buffer size
	int m_rtp_payload_size;
	int m_nal_type;	
	int m_rtp_channel;						//rtp channel form interleaved id
	//
	char *m_rtp_header_ptr;
	char *m_rtp_fu_indicator_ptr;
	char *m_rtp_payload_ptr; 

	FU_HEADER *fuHeader;
	void FU_Parser();
	void GetAddress();

	FILE *fpLog;
public:
	CRtpPacker(int streamType,char *srcbuf,int size,bool isTCP);
	~CRtpPacker();



	void Clear();


	bool CheckH264StartCode(char *p);
	bool FindNextStartCode(char *address,int size);

	void RTP_Pack();
	int PacketSize(){return m_final_size;};
	char *PacketData(){return m_packetBuf;};
	void MoveNext();
	bool Eof(){return m_eof;};
	int PacketSeq(){return m_idx;};
	RTP_HEADER* GetRTPHeader();
	RTP_HEADER_EXTENSION* GetRTPExtension();
	void WriteBuffer(FUPacketSeq type,int StreamType,char* packetBuf,int packetSize);

	void SetRTPHeader_SSRC(unsigned  int value){GetRTPHeader()->ssrc = _htonl(value);};
	void SetRTPHeader_Timestamp(unsigned int value){GetRTPHeader()->timestamp = _htonl(value);};
	void SetRTPHeader_Sequence(unsigned short value){GetRTPHeader()->seq_no = _htons(value);};
	void SetRTPChannel(int value){m_rtp_channel = value;};
};



#endif