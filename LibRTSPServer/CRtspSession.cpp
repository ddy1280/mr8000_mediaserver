// RTSP Protocol 
// CRtspSession
// - parsing of RTSP requests and generation of RTSP responses


#include "CRtspSession.h"

#include <stdio.h>
#include <sstream>
#include <string>
#include <time.h>
#include <map>
#include <iomanip>      // std::setfill, std::setw

#include <String/StringUtility.h>			
#include "Algorithm/md5.h"




#define NULLPTR NULL
using namespace HS;

#pragma comment(lib, "String.lib")
#pragma comment(lib, "Algorithm.lib")



CRtspSession::CRtspSession(SOCKET aRtspClient, CStreamer * aStreamer):m_RtspClient(aRtspClient),m_Streamer(aStreamer)
{
    Init();
	srand(time(NULL));
	m_RtspSessionID  = aRtspClient << 16;         // create a session ID
    m_RtspSessionID |= rand()*0xFFFF+1;
        
    m_StreamID       = SS_MAIN_STREAM;
	m_ChannelID      = -1;
    m_ClientRTPPort  =  0;
    m_ClientRTCPPort =  0;
    m_TcpTransport   =  false;								   

	rtspBuf.Reset(4096);
};

CRtspSession::~CRtspSession()
{
};

void CRtspSession::Init()
{
    m_RtspCmdType   = RTSP_UNKNOWN;
    memset(m_URLPreSuffix, 0x00, sizeof(m_URLPreSuffix));
    memset(m_URLSuffix,    0x00, sizeof(m_URLSuffix));
    memset(m_CSeq,         0x00, sizeof(m_CSeq));
    memset(m_URLHostPort,  0x00, sizeof(m_URLHostPort));
    m_ContentLength  =  0;
};


bool CRtspSession::ParseRtspRequest(char const * aRequest, unsigned aRequestSize)
{
    char     CmdName[RTSP_PARAM_STRING_MAX];
    char     CurRequest[RTSP_BUFFER_SIZE];
    unsigned CurRequestSize; 

    Init();
    CurRequestSize = aRequestSize;
	memset(CurRequest,0,RTSP_BUFFER_SIZE);
    memcpy(CurRequest,aRequest,aRequestSize);

    // check whether the request contains information about the RTP/RTCP UDP client ports (SETUP command)
    char * ClientPortPtr;
    char * TmpPtr;
    char   CP[1024];
    char * pCP;

    ClientPortPtr = strstr(CurRequest,"client_port");
    if (ClientPortPtr != NULLPTR)
    {
        TmpPtr = strstr(ClientPortPtr,"\r\n");
        if (TmpPtr != NULLPTR) 
        {
            TmpPtr[0] = 0x00;
            strcpy(CP,ClientPortPtr);
            pCP = strstr(CP,"=");
            if (pCP != NULLPTR)
            {
                pCP++;
                strcpy(CP,pCP);
                pCP = strstr(CP,"-");
                if (pCP != NULLPTR)
                {
                    pCP[0] = 0x00;
                    m_ClientRTPPort  = atoi(CP);
                    m_ClientRTCPPort = m_ClientRTPPort + 1;
                };
            };
        };
    };

    // Read everything up to the first space as the command name
    bool parseSucceeded = false;
    unsigned i;
    for (i = 0; i < sizeof(CmdName)-1 && i < CurRequestSize; ++i) 
    {
        char c = CurRequest[i];
        if (c == ' ' || c == '\t') 
        {
            parseSucceeded = true;
            break;
        }
        CmdName[i] = c;
    }
    CmdName[i] = '\0';
    if (!parseSucceeded) return false;

    // find out the command type
    if		(strstr(CmdName,"OPTIONS")		!= NULLPTR) m_RtspCmdType = RTSP_OPTIONS;		
    else if (strstr(CmdName,"DESCRIBE")		!= NULLPTR) m_RtspCmdType = RTSP_DESCRIBE;		
    else if (strstr(CmdName,"SETUP")		!= NULLPTR) m_RtspCmdType = RTSP_SETUP;			
    else if (strstr(CmdName,"PLAY")			!= NULLPTR) m_RtspCmdType = RTSP_PLAY;			
	else if (strstr(CmdName,"SET_PARAMETER")!= NULLPTR) m_RtspCmdType = RTSP_SET_PARAMETER;	
    else if (strstr(CmdName,"TEARDOWN")		!= NULLPTR) m_RtspCmdType = RTSP_TEARDOWN;

	

    // check whether the request contains transport information (UDP or TCP)
    if (m_RtspCmdType == RTSP_SETUP)
    {
		std::string TmpSetupReq = std::string(CurRequest);

		if (TmpSetupReq.find("RTP/AVP/TCP")!=std::string::npos)
		{
			int pos = String::ToLower(TmpSetupReq).find("interleaved");
			if (pos!=std::string::npos)
			{
				m_Interleaved = TmpSetupReq.substr(pos);
				if (m_Interleaved.find("\r\n")!=std::string::npos)
					m_Interleaved.erase(m_Interleaved.find("\r\n"),m_Interleaved.length());
				if (m_Interleaved.find(";")!=std::string::npos)
					m_Interleaved.erase(m_Interleaved.find(";"),m_Interleaved.length());
			}
			
			m_TcpTransport = true;
		}
		else
		{
			m_TcpTransport = false;
		}
        //TmpPtr = strstr(CurRequest,"RTP/AVP/TCP");
        //if (TmpPtr != NULLPTR) m_TcpTransport = true; else m_TcpTransport = false;
    };

    // Skip over the prefix of any "rtsp://" or "rtsp:/" URL that follows:
    unsigned j = i+1;
    while (j < CurRequestSize && (CurRequest[j] == ' ' || CurRequest[j] == '\t')) ++j; // skip over any additional white space
    for (; (int)j < (int)(CurRequestSize-8); ++j) 
    {
        if ((CurRequest[j]   == 'r' || CurRequest[j]   == 'R') && 
            (CurRequest[j+1] == 't' || CurRequest[j+1] == 'T') && 
            (CurRequest[j+2] == 's' || CurRequest[j+2] == 'S') && 
            (CurRequest[j+3] == 'p' || CurRequest[j+3] == 'P') && 
             CurRequest[j+4] == ':' && CurRequest[j+5] == '/') 
        {
            j += 6;
            if (CurRequest[j] == '/') 
            {   // This is a "rtsp://" URL; skip over the host:port part that follows:
                ++j;
                unsigned uidx = 0;
                while (j < CurRequestSize && CurRequest[j] != '/' && CurRequest[j] != ' ') 
                {   // extract the host:port part of the URL here
                    m_URLHostPort[uidx] = CurRequest[j];
                    uidx++;
                    ++j;
                };
            } 
            else --j;
            i = j;
            break;
        }
    }

    // Look for the URL suffix (before the following "RTSP/"):
    parseSucceeded = false;
    for (unsigned k = i+1; (int)k < (int)(CurRequestSize-5); ++k) 
    {
        if (CurRequest[k]   == 'R'   && CurRequest[k+1] == 'T'   && 
            CurRequest[k+2] == 'S'   && CurRequest[k+3] == 'P'   && 
            CurRequest[k+4] == '/') 
        {
            while (--k >= i && CurRequest[k] == ' ') {}
            unsigned k1 = k;
            while (k1 > i && CurRequest[k1] != '/') --k1;
            if (k - k1 + 1 > sizeof(m_URLSuffix)) return false;
            unsigned n = 0, k2 = k1+1;

            while (k2 <= k) m_URLSuffix[n++] = CurRequest[k2++];
            m_URLSuffix[n] = '\0';

            if (k1 - i > sizeof(m_URLPreSuffix)) return false;
            n = 0; k2 = i + 1;
            while (k2 <= k1 - 1) m_URLPreSuffix[n++] = CurRequest[k2++];
            m_URLPreSuffix[n] = '\0';
            i = k + 7; 
            parseSucceeded = true;
            break;
        }
    }
    if (!parseSucceeded) return false;

    // Look for "CSeq:", skip whitespace, then read everything up to the next \r or \n as 'CSeq':
    parseSucceeded = false;
    for (j = i; (int)j < (int)(CurRequestSize-5); ++j) 
    {
        if (CurRequest[j]   == 'C' && CurRequest[j+1] == 'S' && 
            CurRequest[j+2] == 'e' && CurRequest[j+3] == 'q' && 
            CurRequest[j+4] == ':') 
        {
            j += 5;
            while (j < CurRequestSize && (CurRequest[j] ==  ' ' || CurRequest[j] == '\t')) ++j;
            unsigned n;
            for (n = 0; n < sizeof(m_CSeq)-1 && j < CurRequestSize; ++n,++j) 
            {
                char c = CurRequest[j];
                if (c == '\r' || c == '\n') 
                {
                    parseSucceeded = true;
                    break;
                }
                m_CSeq[n] = c;
            }
            m_CSeq[n] = '\0';
            break;
        }
    }
    if (!parseSucceeded) return false;

    // Also: Look for "Content-Length:" (optional)
    for (j = i; (int)j < (int)(CurRequestSize-15); ++j) 
    {
        if (CurRequest[j]    == 'C'  && CurRequest[j+1]  == 'o'  && 
            CurRequest[j+2]  == 'n'  && CurRequest[j+3]  == 't'  && 
            CurRequest[j+4]  == 'e'  && CurRequest[j+5]  == 'n'  && 
            CurRequest[j+6]  == 't'  && CurRequest[j+7]  == '-'  && 
            (CurRequest[j+8] == 'L' || CurRequest[j+8]   == 'l') &&
            CurRequest[j+9]  == 'e'  && CurRequest[j+10] == 'n' && 
            CurRequest[j+11] == 'g' && CurRequest[j+12]  == 't' && 
            CurRequest[j+13] == 'h' && CurRequest[j+14] == ':') 
        {
            j += 15;
            while (j < CurRequestSize && (CurRequest[j] ==  ' ' || CurRequest[j] == '\t')) ++j;
            unsigned num;
            if (sscanf(&CurRequest[j], "%u", &num) == 1) m_ContentLength = num;
        }
    }
    return true;
};

RTSP_CMD_TYPES CRtspSession::Handle_RtspRequest(char const * aRequest, unsigned aRequestSize)
{
    if (ParseRtspRequest(aRequest,aRequestSize))
    {
		if		(m_RtspCmdType == RTSP_OPTIONS)			Handle_RtspOPTION();
		else if (m_RtspCmdType == RTSP_DESCRIBE)		Handle_RtspDESCRIBE();
		else if (m_RtspCmdType == RTSP_SETUP)			Handle_RtspSETUP();
		else if (m_RtspCmdType == RTSP_PLAY)			Handle_RtspPLAY();
		else if (m_RtspCmdType == RTSP_SET_PARAMETER)	Handle_RtspSETPARAMETER();
    };
    return m_RtspCmdType;
};

void CRtspSession::Handle_RtspOPTION()
{
	RtspReplyHeader(RTSP_STATUS_OK);
	rtspBuf.PutString("Public: OPTIONS, DESCRIBE, SETUP, TEARDOWN, PLAY, PAUSE\r\n");
	rtspBuf.PutString("Server: HiMediaServer\r\n");
	rtspBuf.PutString("\r\n");

	send(m_RtspClient,rtspBuf.Buffer(),rtspBuf.Length(),0);

}

void CRtspSession::Handle_RtspDESCRIBE()
{
    char   URLBuf[1024];
	std::string s_URLSuffix(m_URLSuffix);
    m_ChannelID = -1;        // invalid URL
	m_UTC = 0;
	// check whether we know a stream with the URL which is requested
	m_StreamType = s_URLSuffix.substr(0,s_URLSuffix.find("?"));
	std::string mStreamParam = s_URLSuffix.substr(s_URLSuffix.find("?")+1);
	std::map<std::string,std::string> mParamMap = String::KeyPairToMap(mStreamParam,"&");
	
	if (String::CheckKeyExist(mParamMap,"CH"))		m_ChannelID = String::ToInt(mParamMap["CH"]);
	if (String::ToUpper(m_StreamType) == "LIVE")   //It effects only in live mode
	{
		if (String::CheckKeyExist(mParamMap,"STREAM"))	m_StreamID = (StreamModeEnum) String::ToInt(mParamMap["STREAM"]);
	}
	if (String::CheckKeyExist(mParamMap,"RID"))		m_RID = mParamMap["RID"];
	if (String::CheckKeyExist(mParamMap,"UTC"))		m_UTC = String::ToInt(mParamMap["UTC"]);


    if (m_ChannelID == -1)
    {   // Stream not available
		RtspReplyError(RTSP_STATUS_SERVICE);
        send(m_RtspClient,rtspBuf.Buffer(),rtspBuf.Length(),0);   
        return;
    }

    // simulate DESCRIBE server response
	char OBuf[256];
	char * ColonPtr;
	strcpy(OBuf,m_URLHostPort);
	ColonPtr = strstr(OBuf,":");
	if (ColonPtr != NULLPTR) ColonPtr[0] = 0x00;

	std::stringstream ssSDP;
	ssSDP << "v=0\r\n";
	ssSDP << "o=- "<<m_RtspSessionID<<" 0 IN IP4 "<< OBuf <<"\r\n";			  // o=  (owner/creator and session identifier).
	ssSDP << "s=Hello\r\n";
	ssSDP << "c=IN IP4 0.0.0.0\r\n";
	ssSDP << "t=0 0\r\n";

	ssSDP << "m=video 0 RTP/AVP 96\r\n";
	ssSDP << "b=AS:200\r\n";
	ssSDP << "a=rtpmap:96 H264/90000\r\n";
	ssSDP << "a=fmtp:96 packetization-mode=1\r\n";
	ssSDP << "a=StreamCount:3\r\n";
	ssSDP << "a=control:streamid=0\r\n";

#ifdef AUDIO_FLAG
	ssSDP << "m=audio 0 RTP/AVP 8\r\n";  //adpcm:5, pcm: 8
	ssSDP << "b=AS:0\r\n";
	ssSDP << "a=control:streamid=1\r\n";
#endif
	ssSDP << "m=application 0 RTP/AVP 97\r\n";
	ssSDP << "b=AS:64\r\n";
	ssSDP << "a=control:streamid=2\r\n";	
	ssSDP << "a=rtpmap:97 T140/90000\r\n"; 


    _snprintf(URLBuf,sizeof(URLBuf),
        "rtsp://%s/%s",
        m_URLHostPort,
        s_URLSuffix.c_str());


	RtspReplyHeader(RTSP_STATUS_OK);
	rtspBuf.PutString("Content-Base: %s/\r\n",URLBuf);
	rtspBuf.PutString("Content-Type: application/sdp\r\n");
	rtspBuf.PutString("Content-Length: %d\r\n",ssSDP.str().length());
	rtspBuf.PutString("\r\n");
	rtspBuf.PutString(ssSDP.str().c_str());

    send(m_RtspClient,rtspBuf.Buffer(),rtspBuf.Length(),0);   
}

void CRtspSession::Handle_RtspSETUP()
{
    char Transport[255];

	char   URLBuf[1024];
	std::string s_streamid(m_URLSuffix);

	STREAM_ID_TYPE iStreamID = (STREAM_ID_TYPE) String::ToInt(String::GetToken(s_streamid,1,"="));


	

	INTERLEAVE_PORT_DEF irp;

	irp.interleave_rtp_port=String::ToInt(String::GetToken(String::GetToken(m_Interleaved,1,"="),0,"-"));
	irp.interleave_rtcp_port=String::ToInt(String::GetToken(String::GetToken(m_Interleaved,1,"="),1,"-"));
	
	m_stream_id_types[iStreamID] = irp ;


    // init RTP streamer transport type (UDP or TCP) and ports for UDP transport
    m_Streamer->InitTransport(m_ClientRTPPort,m_ClientRTCPPort,m_TcpTransport);


    if (m_TcpTransport)
        _snprintf(Transport,sizeof(Transport),"RTP/AVP/TCP;unicast");
    else
        _snprintf(Transport,sizeof(Transport),
            "RTP/AVP;unicast;destination=127.0.0.1;source=127.0.0.1;client_port=%i-%i;server_port=%i-%i",
            m_ClientRTPPort,
            m_ClientRTCPPort,
            m_Streamer->GetRtpServerPort(),
            m_Streamer->GetRtcpServerPort());

	RtspReplyHeader(RTSP_STATUS_OK);
	rtspBuf.PutString("Session: %u\r\n",m_RtspSessionID);
	if (m_Interleaved.length()>0)
		rtspBuf.PutString("Transport: %s;%s\r\n",Transport,m_Interleaved.c_str());
	else
		rtspBuf.PutString("Transport: %s\r\n",Transport);

	rtspBuf.PutString("\r\n");

    send(m_RtspClient,rtspBuf.Buffer(),rtspBuf.Length(),0);
}

void CRtspSession::Handle_RtspPLAY()
{

	RtspReplyHeader(RTSP_STATUS_OK);
	rtspBuf.PutString("Session: %u\r\n",m_RtspSessionID);
	rtspBuf.PutString("Range: npt=0.000-\r\n");
	rtspBuf.PutString("\r\n");

    send(m_RtspClient,rtspBuf.Buffer(),rtspBuf.Length(),0);
}

void CRtspSession::Handle_RtspSETPARAMETER()
{

	RtspReplyHeader(RTSP_STATUS_OK);
	rtspBuf.PutString("Session: %u\r\n",m_RtspSessionID);
	rtspBuf.PutString("\r\n");

	send(m_RtspClient,rtspBuf.Buffer(),rtspBuf.Length(),0);
}



std::string CRtspSession::RtspStateString(RTSP_CMD_TYPES type)
{
	std::string RTSPStates[]={"OPTION","DESCRIBE","SETUP","PLAY","TEARDOWN","SET_PARAMETER","UNKNOWN"};
	return RTSPStates[type];
}

void CRtspSession::RtspReplyHeader(RTSPStatusCode status)
{
	const char *str;

	if (status == RTSP_STATUS_OK)  str = "OK";
	else if (status == RTSP_STATUS_METHOD)  str = "Method Not Allowed";
	else if (status == RTSP_STATUS_BANDWIDTH)  str = "Not Enough Bandwidth";
	else if (status == RTSP_STATUS_SESSION)  str = "Session Not Found";
	else if (status == RTSP_STATUS_STATE)  str = "Method Not Valid in This State";
	else if (status == RTSP_STATUS_AGGREGATE)  str = "Aggregate operation not allowed";
	else if (status == RTSP_STATUS_ONLY_AGGREGATE)  str = "Only aggregate operation allowed";
	else if (status == RTSP_STATUS_TRANSPORT)  str = "Unsupported transport";
	else if (status == RTSP_STATUS_INTERNAL)  str = "Internal Server Error";
	else if (status == RTSP_STATUS_SERVICE)  str = "Service Unavailable";
	else if (status == RTSP_STATUS_VERSION)  str = "RTSP Version not supported";
	else str = "Unknown Error";

	rtspBuf.Reset();
	rtspBuf.PutString("RTSP/1.0 %d %s\r\n", status, str);
	rtspBuf.PutString("CSeq: %s\r\n", m_CSeq);

	if (m_RtspCmdType!=RTSP_OPTIONS)
	{
		/* output GMT time */
		time_t ti;
		struct tm *tm;
		char buf2[64];

		ti = time(NULL);
		tm = gmtime(&ti);
		strftime(buf2, sizeof(buf2), "%a, %b %d %Y %H:%M:%S", tm);
		rtspBuf.PutString("Date: %s GMT\r\n", buf2);
	}
}

void CRtspSession::RtspReplyError(RTSPStatusCode errorStatus)
{
	RtspReplyHeader(errorStatus);
	rtspBuf.PutString("\r\n");
}

std::string CRtspSession::GetStreamHashIndex()
{
	std::stringstream hashSrc;
	hashSrc << m_RID << m_StreamID << m_StreamType << m_ChannelID << m_UTC; 

	std::string hashIdx = Algorithm::md5(hashSrc.str());
	std::stringstream ret;
	ret << std::setfill ('0') << std::setw(8) << std::uppercase  << std::hex << m_RtspSessionID;
	hashIdx = ret.str()+hashIdx;
	return hashIdx;	
}

unsigned int CRtspSession::GetSessionID()
{
	return m_RtspSessionID;	
}

std::string CRtspSession::GetRID()
{
	return m_RID;
}