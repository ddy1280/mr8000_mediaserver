
#include "CRtpPack.h"
#include <string.h>



#include "CRtpPack.h"
#define RTP_DEBUG


#pragma comment(lib, "Ws2_32.lib")


/*
**TCP 單包**
			(m_packetBuf)
			|		 (m_rtp_header_ptr)
			|		   |	    (m_rtp_payload_ptr)
			|		   |		  |		
			|		   |		  |		  
			V		   V		  V	                H264 Data	         		|
			+-------------------------------------------------------------------+
			|TCP Header|RTP Header|    											|
			+----------+----------+---------------------------------------------+
			| 4 bytes  | 12 bytes |												|
			+-------------------------------------------------------------------+ 
			|																	|


**TCP 分包**
          (m_packetBuf)
			|		 (m_rtp_header_ptr)
			|		   |	    (m_rtp_fu_indicator_ptr)
			|		   |		  |		 
			|		   |		  |		  		m_rtp_payload_ptr)
			V		   V		  V		         |        H264 Data	     		|
			+-------------------------------------------------------------------+
			|TCP Header|RTP Header| FU IND|FU HDR|	without nal_type			|
			+----------+----------+-------+------+------------------------------+
			| 4 bytes  | 12 bytes |	1 byte|1 byte|								|
			+-------------------------------------------------------------------+ 
			|																	|

**UDP 單包**
			(m_rtp_header_ptr)
			|		 (m_rtp_payload_ptr)
			|		   |		  		  
			V		   V		                  H264 Data	         			|
			+-------------------------------------------------------------------+
			|RTP Header|		   								                |
			+----------+--------------------------------------------------------+
			| 12 bytes |														|
			+-------------------------------------------------------------------+ 
			|																	|


**UDP 分包**
			(m_rtp_header_ptr)
			|		 (m_rtp_fu_indicator_ptr)
			|		   |		   (m_rtp_payload_ptr)
			|		   |	  		 |	
			V		   V			 V				H264 Data	     			|
			+-------------------------------------------------------------------+
			|RTP Header|FU IND|FU HDR|			without nal_type									|
			+----------+-------------+------------------------------------------+
			| 12 bytes |1 byte|1 byte|											|
			+-------------------------------------------------------------------+ 
			|																	|
*/

CRtpPacker::CRtpPacker(int streamType,char *srcbuf,int size,bool isTCP)
{
#ifdef RTP_DEBUG
	fpLog = fopen("rtp_log.txt","a+");
	fprintf(fpLog,"----------------------------------------------------------------------------\r\n");
	fprintf(fpLog,"Raw Buffer size:%d\r\n",size);
#endif

	Clear();
	mIsTCPTransport = isTCP;
	m_streamType = streamType;
	if (m_streamType ==0) //CheckH264StartCode(srcbuf))
	{
		if (FindNextStartCode(srcbuf,size))
		{
			m_srcBuffer = srcbuf+5;	//jump 00 00 00 01 67 at first ,only for H264 video stream
			m_srcBufSize= size-5;
			m_nal_type = *(m_srcBuffer-1);	//保留 nal_unit_type 之後填給 FU Header

		}
#ifdef RTP_DEBUG		
		fprintf(fpLog,"Raw Buffer offset nal, size:%d\r\n",m_srcBufSize);
#endif
	}
	else
	{
		m_srcBuffer = srcbuf;
		m_srcBufSize= size;
	}
	

	m_SrcBuffCurrPtr = m_srcBuffer;			//要開始傳送的位址
	m_SrcBuffCurrSize = m_srcBufSize;		//目前剩餘未傳完的size
	m_eof = false;
	m_idx=0;
	m_final_size = 0;

	m_rtp_header_ptr = NULL;
	m_rtp_fu_indicator_ptr = NULL;
	m_rtp_payload_ptr = NULL; 
	fuHeader = NULL;

	GetAddress();
}

CRtpPacker::~CRtpPacker()
{
#ifdef RTP_DEBUG
	fclose(fpLog);
#endif
}


void CRtpPacker::Clear()
{
	memset(m_packetBuf,0,MAX_PACKET_LEN);
}

RTP_HEADER* CRtpPacker::GetRTPHeader()
{
	return (RTP_HEADER *)(m_rtp_header_ptr);
} 

RTP_HEADER_EXTENSION* CRtpPacker::GetRTPExtension()
{
	return (RTP_HEADER_EXTENSION *)(m_rtp_header_ptr + RTP_HEAD_LEN);
} 
	
void CRtpPacker::GetAddress()
{
	if (mIsTCPTransport)
	{
		m_rtp_header_ptr = m_packetBuf+TCP_INTERLEAVE_FRAME_HEAD_LEN; 

		if (m_srcBufSize>MAX_PACK_LEN)//會分包
		{
			m_rtp_fu_indicator_ptr	= m_packetBuf +	TCP_INTERLEAVE_FRAME_HEAD_LEN+RTP_HEAD_LEN+RTP_HEAD_EXT_LEN;
			m_rtp_offset            =				TCP_INTERLEAVE_FRAME_HEAD_LEN+RTP_HEAD_LEN+RTP_HEAD_EXT_LEN+FU_LEN;
		}
		else
		{
			m_rtp_offset			=				TCP_INTERLEAVE_FRAME_HEAD_LEN+RTP_HEAD_LEN+RTP_HEAD_EXT_LEN;
		}
	}
	else	//UDP
	{
		m_rtp_header_ptr = m_packetBuf; 

		if (m_srcBufSize>MAX_PACK_LEN)//會分包
		{
			m_rtp_fu_indicator_ptr	= m_packetBuf +	RTP_HEAD_LEN+RTP_HEAD_EXT_LEN;
			m_rtp_offset			=				RTP_HEAD_LEN+RTP_HEAD_EXT_LEN+FU_LEN;
		}
		else
		{
			m_rtp_offset			=				RTP_HEAD_LEN+RTP_HEAD_EXT_LEN;
		}
	}
	m_rtp_payload_ptr		= m_packetBuf+m_rtp_offset;
	m_rtp_payload_size		= MAX_PACK_LEN-m_rtp_offset;

	FU_Parser();

}

//H264 開始碼檢測
bool CRtpPacker::CheckH264StartCode(char *p)
{

	if((*(p+0)==0x00)&&(*(p+1)==0x00)&&(*(p+2)==0x00)&&(*(p+3)==0x01))
		return true;
	else
		return false;
}


bool CRtpPacker::FindNextStartCode(char *address,int size)
{
	char *p = address;

	while (1)
	{	
		if (( p-address)>size-4)  return false;
		if ( *(p+0)==0x00 && *(p+1)==0x00 && *(p+2)==0x00 && *(p+3)==0x01 )
		{
			m_srcBuffer = p;
			m_srcBufSize= p-address;
			return true;
		}
		else
			p++;
	}
}



void CRtpPacker::RTP_Pack()
{
	RTP_HEADER *rtpHeader = GetRTPHeader();

	if (m_SrcBuffCurrSize>m_rtp_payload_size && m_idx==0) //第一包
	{
#ifdef RTP_DEBUG
		fprintf(fpLog,"Raw Buffer split...First, size=%d\r\n",m_rtp_payload_size);			
#endif
		WriteBuffer(FU_PacketFirst, m_streamType,m_SrcBuffCurrPtr,m_rtp_payload_size);
	}
	else
	{
		if (m_SrcBuffCurrSize<=m_rtp_payload_size && m_idx==0) //只有一包
		{
#ifdef RTP_DEBUG
			fprintf(fpLog,"Raw Buffer split...OnlyOne, size=%d\r\n",m_SrcBuffCurrSize);			
#endif
			WriteBuffer(FU_PacketOnlyOne,m_streamType,m_SrcBuffCurrPtr,m_SrcBuffCurrSize);
		}
		else if (m_SrcBuffCurrSize>m_rtp_payload_size) //中間包
		{
#ifdef RTP_DEBUG
			fprintf(fpLog,"Raw Buffer split...Middle, size=%d\r\n",m_rtp_payload_size);			
#endif
			WriteBuffer(FU_PacketMiddle,m_streamType,m_SrcBuffCurrPtr,m_rtp_payload_size);
		}
		else //最末包
		{
#ifdef RTP_DEBUG
			fprintf(fpLog,"Raw Buffer split...Last, size=%d\r\n",m_SrcBuffCurrSize);
#endif
			WriteBuffer(FU_PacketLast,m_streamType,m_SrcBuffCurrPtr,m_SrcBuffCurrSize);
		}
	}
}

void CRtpPacker::MoveNext()
{
	//Move Pointer
	if (m_SrcBuffCurrSize>=m_rtp_payload_size)
	{
		m_SrcBuffCurrSize	-=	m_rtp_payload_size;
		m_SrcBuffCurrPtr	+=	m_rtp_payload_size;
#ifdef RTP_DEBUG
		fprintf(fpLog,"Raw Buffer already send..%d, remain..%d\r\n",(m_SrcBuffCurrPtr-m_srcBuffer),m_SrcBuffCurrSize);
#endif
	}
	else
	{
		m_eof = true;
#ifdef RTP_DEBUG
		fprintf(fpLog,"Raw Buffer already completed\r\n");
#endif
	}

	m_idx++;

}

void CRtpPacker::FU_Parser()
{
/****************************************************************** 
 FU_Indicator, FU_Header 填值說明
 ******************************************************************
	 
 分包前的NALU:
	 NALU Format
	 +---------------+
	 |0|1|2|3|4|5|6|7|
	 +-+-+-+-+-+-+-+-+
	 |F|NRI|   Type  |   <--nal_unit_type
	 +---------------+

	 RFC3984: H.264 NALU TYPE(nal_unit_type)
	 1~23 : Single NAL unit packet.
	 24 : STAP-A
	 25 : STAP-B
	 26 : MTAP16
	 27 : MTAP32
	 28 : FU-A
	 29 : FU-B

分包後:
	FU_INDICATOR	 FU_HEADER
	+---------------+---------------+ 
	|7|6|5|4|3|2|1|0|7|6|5|4|3|2|1|0|  
	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ 
	|F|NRI|Type,FU-A|S|E|R|   Type  |  
	+---------------+---------------+ 
	|    1 byte     |    1 byte     |
	

******************************************************************/  
	if (m_rtp_fu_indicator_ptr!=NULL) 
	{
		*(m_rtp_fu_indicator_ptr) = *(m_rtp_payload_ptr)&0xE0 ; //把NALU 的F,NRI 設給 FU_INDICATOR 的F,NRI
		fuIndictor = (FU_INDICATOR*)(m_rtp_fu_indicator_ptr);
		fuIndictor->Type = 0x1C; //把分包的NALU Type 改為 FU-A (value:28)

		fuHeader= (FU_HEADER*)(m_rtp_fu_indicator_ptr+1);
		fuHeader->StartBit = 0;
		fuHeader->EndBit = 0;
		fuHeader->ForbiddenBit = 0;
		fuHeader->NalUnitType = m_nal_type;

	}
	else
	{
		printf("m_rtp_fu_indicator_ptr=NULL\r\n");
	}
	
}

void CRtpPacker::WriteBuffer(FUPacketSeq type,int StreamType,char* packetBuf,int packetSize)
{

	RTP_HEADER *rtpHeader = GetRTPHeader();
	RTP_HEADER_EXTENSION *rtpHeaderExt = GetRTPExtension();
	//m_rtp_payload_size 為除去所有Header 之後Send Buffer 剩餘的 size


	//for (int i=0;i<64;i++)
	//	fprintf(fpLog,"%02x ",(unsigned char)(*(packetBuf+i)));
	//fprintf(fpLog,"\n");	

	rtpHeaderExt->def_profile = 0;
	rtpHeaderExt->ext_len = _htons(0x02);
	rtpHeaderExt->rec_sec = _htonl(0);
	rtpHeaderExt->rec_usec = _htonl(0);

	m_final_size = m_rtp_offset+packetSize;   
	if (mIsTCPTransport)
	{
		char *tcp_interleave_header_ptr = m_packetBuf;
		int rtp_data_size = m_final_size - TCP_INTERLEAVE_FRAME_HEAD_LEN;

		tcp_interleave_header_ptr[0]  = '$';				// magic number
		tcp_interleave_header_ptr[1]  = m_rtp_channel;		// number of multiplexed subchannel on RTSP connection - here the RTP channel	 ==> 從RTSP 協議取得
		tcp_interleave_header_ptr[2]  = (rtp_data_size & 0x0000FF00) >> 8;
		tcp_interleave_header_ptr[3]  = (rtp_data_size & 0x000000FF);
	}

	memcpy(m_rtp_payload_ptr,packetBuf,packetSize);


	//RTP Header fix padding data
	rtpHeader->version = 2;
	rtpHeader->padding=0;
	rtpHeader->extension=1; // {Add RTP Header Extension}
	rtpHeader->csrc_len=0;
	rtpHeader->marker = 0;

	if (StreamType==0)
		rtpHeader->payload = VIDEO_PAYLOAD_TYPE_H264;
	else if (StreamType==1)
		rtpHeader->payload = AUDIO_PAYLOAD_TYPE_PCM_ALaw;
	else if (StreamType==2)
		rtpHeader->payload = TEXT_PAYLOAD_TYPE_APPLICATION;

	if (fuHeader)
	{
		if (type==FU_PacketFirst)
		{
			fuHeader->StartBit = 1;
			fuHeader->EndBit = 0;
			fuHeader->ForbiddenBit = 0;
		}
		else if (type==FU_PacketMiddle)
		{
			fuHeader->StartBit = 0;
			fuHeader->EndBit = 0;
			fuHeader->ForbiddenBit = 0;
			fuHeader->NalUnitType = m_nal_type;
		}
		else if (type==FU_PacketLast)
		{
			fuHeader->StartBit = 0;
			fuHeader->EndBit = 1;
			fuHeader->ForbiddenBit = 0;
			fuHeader->NalUnitType = m_nal_type;

			if (m_nal_type>=1 && m_nal_type<=5) //只有NALU 1~5 的包結尾才要要打mark
				rtpHeader->marker = 1;
			else
				rtpHeader->marker = 0;

		}
		else if (type==FU_PacketOnlyOne)
		{
			rtpHeader->marker = 0;
		}
	}

#ifdef RTP_DEBUG
	for (int i=0;i<64;i++)
		fprintf(fpLog,"%02x ",(unsigned char)(*(PacketData()+i)));
	fprintf(fpLog,"\n");	
	fprintf(fpLog,"|Interleave  |RTP Header                                        | RTP Ext Header                                   |       |\r\n");
#endif

	
}
