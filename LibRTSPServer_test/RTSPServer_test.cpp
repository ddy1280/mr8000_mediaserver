// MediaLAN 02/2013
// CRtspSession
// - Main and streamer master listener socket
// - Client session thread

#include "stdafx.h"
#include <Windows.h>
#include "LibRTSPServer.h"

#include <map>
#include <string>
#include <iostream>
#include <sstream>
#include <algorithm>

#include "CRtspSession.h"
#include "CStreamer.h"

#include <ios>
#include <boost/interprocess/file_mapping.hpp>
#include <boost/interprocess/mapped_region.hpp>

#include "H264Parser.h"

namespace bip = boost::interprocess;

#pragma comment(lib, "LibRTSPServer.lib")
#pragma comment(lib, "String.lib")




CRTSPServer rtspd;


int RtspSessionHandler(const RTSP_SESSION_HANDLE rtspHandle,const RTSP_STREAMER_HANDLE ssHandle)
{
	CRtspSession *rtspSession = (CRtspSession*)	rtspHandle;
	CStreamer *streamer = (CStreamer*) ssHandle;

//for test, open H264 file

	std::string filename = "R:\\TEST.264";
	bip::file_mapping mapping(filename.c_str(), bip::read_only);
	bip::mapped_region mapped_rgn(mapping, bip::read_only);
	char* const mmaped_data = static_cast<char*>(mapped_rgn.get_address());
	std::streamsize const mmap_size = mapped_rgn.get_size();

	CH264Parser h264parser;
	h264parser.SetBuffer(mmaped_data,mmap_size);

	while (1)
	{
		H264FRAME *pFrame = h264parser.GetFrame();
		if (pFrame==NULL) break;

		printf("%p,Type:%s,size=%X\n",pFrame->frameBuffer - mmaped_data,h264parser.GetFrameType(pFrame->frameType).c_str(),pFrame->frameSize);

		if (pFrame->frameType == FRAME_IFRAME || pFrame->frameType == FRAME_PFRAME)
			streamer->ReadStreamingBuffer(0,(char*) pFrame->frameBuffer,pFrame->frameSize);


		delete pFrame;
		pFrame = NULL;
	}













	//bool ret = true;
	//while (ret)						
	//{

	////	streamer->ReadStreamingBuffer(pkt->m_iStreamType,(char*) pkt->m_pData,pkt->m_iDataSize);

	//}
	return 0;
}

int main()  
{    



	rtspd.SetRtspSessionHandler(RtspSessionHandler);
	rtspd.Start();


	system("pause");

	rtspd.Stop();
										 
} 


