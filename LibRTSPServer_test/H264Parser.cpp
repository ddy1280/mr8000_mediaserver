#include <iostream>
#include <stdio.h>
#include <iomanip>      // std::setfill, std::setw
#include "H264Parser.h"


CH264Parser::CH264Parser()
{

}
CH264Parser::~CH264Parser()
{

}
void CH264Parser::SetBuffer(const char* buffer,int size)
{
	m_startPos =(char*) buffer;
    m_currPos = m_startPos;
	m_endPos = m_startPos+size; 
	m_maxsize = size;
}
H264FRAME* CH264Parser::GetFrame()
{

	m_currPos = FindNextStartCode(m_currPos);
//	if (m_currPos==NULL) break;

	H264FRAME *newframe = new H264FRAME();

	int key = m_currPos[4];
	if (key == 0x67) newframe->frameType = FRAME_SPS;
	else if (key == 0x68) newframe->frameType = FRAME_PPS;
	else if (key == 0x06) newframe->frameType = FRAME_SEI;
	else if (key == 0x65) newframe->frameType = FRAME_IFRAME;
	else if (key == 0x61 || key == 0x01) newframe->frameType = FRAME_PFRAME;

	newframe->frameBuffer = m_currPos;
	newframe->frameSize = FindNextStartCode(m_currPos+4)-m_currPos;
	
	//printf("%p, %02X %02X %02X %02X %02X,%s\n",m_currPos - m_startPos,m_currPos[0],m_currPos[1],m_currPos[2],m_currPos[3],m_currPos[4],GetFrameType(m_currPos[4]).c_str());

	m_currPos+=4;



	//while (m_currPos)
	//{
	//	m_currPos = FindNextStartCode(m_currPos);
	//	if (m_currPos==NULL) break;
	//	printf("%p, %02X %02X %02X %02X %02X,%s\n",m_currPos - m_startPos,m_currPos[0],m_currPos[1],m_currPos[2],m_currPos[3],m_currPos[4],GetFrameType(m_currPos[4]).c_str());
	//	m_currPos+=4;
	//}


	return newframe;
	//return frame;
}

char* CH264Parser::FindNextStartCode(char *address)
{
	char *p = address;
	while (1)
	{	
		if (p>m_endPos-4)  return NULL;
		if ( *(p+0)==0x00 && *(p+1)==0x00 && *(p+2)==0x00 && *(p+3)==0x01 )
		{
			return p;
		}
		else
			p++;
	}
}

std::string CH264Parser::GetFrameType(H264_FRAME_TYPE key)
{

	if (key == FRAME_SPS) return "SPS";
	else if (key == FRAME_PPS) return "PPS";
	else if (key == FRAME_SEI) return "SEI";
	else if (key == FRAME_IFRAME) return "I";
	else if (key == FRAME_PFRAME) return "P";
	else return "UNKNOWN";

}