#include <iostream>
typedef enum H264_FRAME_TYPE
{
	FRAME_UNKNOWN=-1,
	FRAME_IFRAME=0,
	FRAME_PFRAME=1,
	FRAME_SPS,
	FRAME_PPS,
	FRAME_SEI
};
typedef struct H264FRAME
{
	H264_FRAME_TYPE frameType;
	const char *frameBuffer;
	int frameSize;
};

class CH264Parser
{
private:
	char *m_startPos;
	char *m_endPos;
	char *m_currPos;
	size_t m_maxsize;
	char* FindNextStartCode(char *address);

public:
	CH264Parser();
	~CH264Parser();
	void SetBuffer(const char* buffer,int size);
	H264FRAME* GetFrame();
	std::string GetFrameType(H264_FRAME_TYPE key);

};



